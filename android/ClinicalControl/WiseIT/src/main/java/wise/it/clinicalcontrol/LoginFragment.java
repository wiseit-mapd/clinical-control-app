//WiseIT
package wise.it.clinicalcontrol;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import net.danlew.android.joda.JodaTimeAndroid;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.List;

import wise.it.clinicalcontrol.authentication.AuthenticationHolder;
import wise.it.clinicalcontrol.cache.Cache;
import wise.it.clinicalcontrol.model.Authenticable;
import wise.it.clinicalcontrol.model.CommonAuthenticable;
import wise.it.clinicalcontrol.model.Doctor;
import wise.it.clinicalcontrol.model.Identifiable;
import wise.it.clinicalcontrol.model.Nurse;
import wise.it.clinicalcontrol.model.Patient;
import wise.it.clinicalcontrol.model.User;
import wise.it.clinicalcontrol.model.measure.HealthMeasure;
import wise.it.clinicalcontrol.model.measure.HealthMeasureImpl;
import wise.it.clinicalcontrol.util.Utils;
import wise.it.clinicalcontrol.network.HttpMethod;
import wise.it.clinicalcontrol.network.RequestAsyncTask;
import wise.it.clinicalcontrol.network.RequestParams;


/**
 * Created by rider on 29.10.2016.
 */

public class LoginFragment extends Fragment {

    private class RetrieveUserPostProcessor implements RequestAsyncTask.PostProcessor {
        private final Class<? extends Identifiable> clazz;

        private RetrieveUserPostProcessor(final Class<? extends Identifiable> clazz) {
            this.clazz = clazz;
        }

        public void postProcess(final RequestAsyncTask.Response response) {
            if (response.getResult() == null) {
                Toast.makeText(getContext(), R.string.not_possible_login, Toast.LENGTH_SHORT).show();
                return;
            }

            try {
                final JSONObject jsonObject = new JSONObject(response.getResult());
                final JSONObject jsonIdentifiable = jsonObject.getJSONObject(clazz.getSimpleName().toLowerCase());
                final JSONObject jsonAuthenticable = jsonObject.getJSONObject("authenticable");

                final Authenticable authenticable = Utils.<Authenticable>convertToModel(jsonAuthenticable, CommonAuthenticable.class);
                Cache.getInstance().put(Authenticable.class, Collections.singleton(authenticable));
                final Identifiable identifiable = Utils.convertToModel(jsonIdentifiable, clazz);
                AuthenticationHolder.setUser(new User(identifiable));

                final Intent intent = new Intent(getContext(),MenuActivity.class);
                startActivity(intent);
            }  catch (final JSONException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }

    private final RequestAsyncTask.PostProcessor retrieveTokenPostProcessor = new RequestAsyncTask.PostProcessor() {
        @Override
        public void postProcess(RequestAsyncTask.Response response) {
            String message = null;

            switch(response.getCode()) {
                case 200: {
                    final String result= response.getResult().replace("\"","");
                    Token.sToken.setToken(result,getContext());
                    break;
                }
                case 401: {
                    Token.sToken.setToken(null,getContext());
                    message = getResources().getString(R.string.invalid_credentials);
                }
            }

            if (Token.sToken.getToken()!=null) {
                initAuthenticationHolder(Token.sToken);
            } else {
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            }
        }
    };





    private final String sharedPref = "wise.it.clinicalcontrol.loginCredentials";
    private final String tokenPref = "wise.it.clinicalcontrol.loginCredentials.token";

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    private EditText mUsername;
    private EditText mPassword;
    private Button mLoginButton;


    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        JodaTimeAndroid.init(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final Token token = Token.get(getContext());

        View v = inflater.inflate(R.layout.fragment_login, container, false);

        mUsername = (EditText)v.findViewById(R.id.username_edit_text);
        Drawable usernamedrawable = ContextCompat.getDrawable(getContext(),R.drawable.male_icon);
        usernamedrawable.setBounds(0,0, mUsername.getLineHeight(),mUsername.getLineHeight());
        mUsername.setCompoundDrawables(usernamedrawable, null, null, null);

        mPassword = (EditText)v.findViewById(R.id.password_edit_text);
        Drawable passworddrawable = ContextCompat.getDrawable(getContext(),R.drawable.key_icon);
        passworddrawable.setBounds(0,0, mPassword.getLineHeight(),mPassword.getLineHeight());
        mPassword.setCompoundDrawables(passworddrawable, null, null, null);

        mLoginButton = (Button)v.findViewById(R.id.login_btn);

        int hasInternetPermission = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE);
        if (hasInternetPermission != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_CODE_ASK_PERMISSIONS);
        }

        if (token.getToken() != null) {
            initAuthenticationHolder(token);
        }

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int hasInternetPermission = ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (hasInternetPermission != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_CODE_ASK_PERMISSIONS);
                }

                try {
                    final RequestParams params = createParams();
                    final RequestAsyncTask task = new RequestAsyncTask.Builder().setRequestParams(params).setPostProcessor(retrieveTokenPostProcessor).buildWithDialog(getActivity());
                    task.execute();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        return v;
    }

    private void initAuthenticationHolder(final Token token) {
        final RequestParams.Builder builder = new RequestParams.Builder();
        final String resource;
        final Class<? extends Identifiable> clazz;
        switch (token.getRole()) {
            case DOCTOR: {
                resource = String.format(Utils.DOCTORS_BY_AUTHENTICABLE_ID_RESOURCE_PATTERN, token.getId());
                clazz = Doctor.class;
                break;
            }
            case NURSE: {
                resource = String.format(Utils.NURSES_BY_AUTHENTICABLE_ID_RESOURCE_PATTERN, token.getId());
                clazz = Nurse.class;
                break;
            }
            case PATIENT: {
                resource = String.format(Utils.PATIENTS_BY_AUTHENTICABLE_ID_RESOURCE_PATTERN, token.getId());
                clazz = Patient.class;
                break;
            }
            default: throw new IllegalArgumentException("Unknown role.");
        }
        try {
            final RequestParams requestParams = builder.setResource(resource).setHttpMethod(HttpMethod.GET).buildWithAuthorization();
            final RequestAsyncTask requestAsyncTask = new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(new RetrieveUserPostProcessor(clazz)).buildWithDialog(getActivity());
            requestAsyncTask.execute();
        } catch (final MalformedURLException e) {
            throw new RuntimeException(e);
        }

    }


    private RequestParams createParams() throws MalformedURLException, NoSuchAlgorithmException {
        return new RequestParams.Builder()
                .setResource(Utils.TOKEN_RESOURCE)
                .setHttpMethod(HttpMethod.POST)
                .setJsonObject(
                        Pair.create("username", mUsername.getText().toString()),
                        Pair.create("password", toMD5(mPassword.getText().toString())))
                .build();
    }


    private String toMD5(String password) throws NoSuchAlgorithmException {
        String result = "";
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.update(password.getBytes(Charset.forName("UTF-8")),0,password.length());
        result = new BigInteger(1,messageDigest.digest()).toString(16);
        return result;
    }
}



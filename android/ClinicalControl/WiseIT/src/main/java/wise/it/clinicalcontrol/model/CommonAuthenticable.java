package wise.it.clinicalcontrol.model;

import java.util.Objects;
import java.util.Set;

import wise.it.clinicalcontrol.model.converter.AuthenticableIdConverter;
import wise.it.clinicalcontrol.model.converter.CustomConvertion;
import wise.it.clinicalcontrol.model.converter.RoleSetCustomConverter;

public class CommonAuthenticable implements Authenticable {

    @CustomConvertion(converter = AuthenticableIdConverter.class)
    private String authenticableId;
    private String username;
    private String password;
    private String name;
    private String email;
    private String phone;
    private String dateOfBirth;
    private Gender gender;
    private Object photo;
    private String address;

    @CustomConvertion(converter = RoleSetCustomConverter.class)
    private Set<Role> roles;

    @Override
    public final String generateKey() {
        return authenticableId;
    }

    public String getAuthenticableId() {
        return authenticableId;
    }

    public void setAuthenticableId(String authenticableId) {
        this.authenticableId = authenticableId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public Set<Authenticable.Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Authenticable.Role> roles) {
        this.roles = roles;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(final String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public Object getPhoto() {
        return photo;
    }

    public void setPhoto(Object photo) {
        this.photo = photo;
    }

    @Override
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("authenticableId: ").append(authenticableId).append(", ")
                .append("username: ").append(username).append(", ")
                .append("Password lenth: ").append(password == null ? password : password.length()).append(", ")
                .append("name: ").append(name).append(", ")
                .append("email: ").append(email).append(", ")
                .append("phone: ").append(phone).append(", ")
                .append("dateOfBirth: ").append(dateOfBirth).append(", ")
                .append("gender: ").append(gender).append(", ")
                .append("photo: ").append(photo).append(", ")
                .append("roles: ").append(roles).append(", ")
                .append("address: ").append(address).toString();
    }
}

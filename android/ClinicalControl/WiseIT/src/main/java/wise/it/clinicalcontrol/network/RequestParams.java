package wise.it.clinicalcontrol.network;

import android.support.v4.util.Pair;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import wise.it.clinicalcontrol.Token;
import wise.it.clinicalcontrol.util.Utils;

public class RequestParams {
    public static class Builder {
        private URL url;
        private HttpMethod method;
        private Set<Pair<String,String>> headerParams = new HashSet<>();
        private JSONObject jsonObject;

        public Builder setResource(final String resource) throws MalformedURLException {
            this.url = new URL(String.format("%s%s", Utils.BACKEND_SERVER_URL, resource));
            return this;
        }

        public Builder setHttpMethod(final HttpMethod method) {
            if (method == null) {
                throw new IllegalArgumentException("method should not be null.");
            }
            this.method = method;
            return this;
        }

        public Builder setHeaderParams(final Pair<String,String>... headerParams) {
            if (headerParams == null) {
                throw new IllegalArgumentException("headerParams should not be null.");
            }
            this.headerParams = new HashSet<>(Arrays.asList(headerParams));
            return this;
        }

        public Builder setJsonObject(final JSONObject jsonObject) {
            if (jsonObject == null) {
                throw new IllegalArgumentException("jsonObject should not be null.");
            }
            this.jsonObject = jsonObject;
            return this;
        }

        public Builder setJsonObject(final Pair<String,String>... pairs) {
            if (pairs == null) {
                throw new IllegalArgumentException("headerParams should not be null.");
            }
            try {
                final JSONObject jsonObject = new JSONObject();
                for (final Pair<String, String> pair : pairs) {
                    jsonObject.put(pair.first, pair.second);
                }
                this.jsonObject = jsonObject;
                return this;
            } catch (final JSONException e) {
                throw new RuntimeException(e);
            }
        }

        private static String convertToDataString(final JSONObject jsonObject) {
            if (jsonObject == null) {
                return null;
            }

            boolean first = true;
            final StringBuilder result = new StringBuilder();
            final Iterator<String> itr = jsonObject.keys();
            try {
                while (itr.hasNext()) {
                    final String key = itr.next();
                    final Object value = jsonObject.get(key);

                    if (first)
                        first = false;
                    else
                        result.append("&");

                    result.append(URLEncoder.encode(key, "UTF-8"));
                    result.append("=");
                    result.append(value instanceof String ? URLEncoder.encode(value.toString(), "UTF-8") : value);

                }
                return result.toString();
            } catch (final Exception e) {
                throw new RuntimeException(e);
            }
        }

        public RequestParams build() {
            if (url == null) {
                throw new IllegalStateException("URL must to be set.");
            }

            if (method == null) {
                throw new IllegalStateException("Method must to be set.");
            }

            return new RequestParams(url, method, headerParams, convertToDataString(jsonObject));
        }

        public RequestParams buildWithAuthorization() {
            this.headerParams.add(Pair.create(Utils.AUTHORIZATION_HEADER, String.format(Utils.AUTHORIZATION_VALUE_PATTERN, Token.getToken())));
            return build();
        }
    }

    private final URL url;
    private final HttpMethod method;
    private final Set<Pair<String,String>> headerParams;
    private final String dataString;

    private RequestParams(final URL url, final HttpMethod method, final Set<Pair<String, String>> headerParams, final String dataString) {
        this.url = url;
        this.method = method;
        this.headerParams = headerParams;
        this.dataString = dataString;
    }

    public URL getUrl() {
        return url;
    }

    public HttpMethod getMethod() {
        return method;
    }

    public Set<Pair<String, String>> getHeaderParams() {
        return headerParams;
    }

    public String getDataString() {
        return dataString;
    }
}
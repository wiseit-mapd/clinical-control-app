package wise.it.clinicalcontrol.network;

import android.content.Context;

import wise.it.clinicalcontrol.CustomProgressDialog;

public class DialogRequestProcessor implements RequestAsyncTask.PreProcessor, RequestAsyncTask.PostProcessor {

    private final CustomProgressDialog progressDialog;
    private final RequestAsyncTask.PreProcessor preProcessorDelegate;
    private final RequestAsyncTask.PostProcessor postProcessorDelegate;

    public DialogRequestProcessor(final Context context, final RequestAsyncTask.PreProcessor preProcessorDelegate, final RequestAsyncTask.PostProcessor postProcessorDelegate) {
        this.progressDialog = new CustomProgressDialog(context, 1);
        this.preProcessorDelegate = preProcessorDelegate;
        this.postProcessorDelegate = postProcessorDelegate;
    }


    @Override
    public void preProcess() {
        progressDialog.show();
        preProcessorDelegate.preProcess();
    }

    @Override
    public void postProcess(RequestAsyncTask.Response response) {
        postProcessorDelegate.postProcess(response);
        progressDialog.dismiss();
    }
}

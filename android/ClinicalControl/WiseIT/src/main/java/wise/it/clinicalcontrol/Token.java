//WiseIT
package wise.it.clinicalcontrol;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import io.jsonwebtoken.impl.Base64UrlCodec;
import wise.it.clinicalcontrol.model.Authenticable.Role;


/**
 * Created by rider on 30.10.2016.
 */

public class Token {

    private final static String sharedPref = "wise.it.clinicalcontrol.loginCredentials";
    private final static String tokenPref = "wise.it.clinicalcontrol.loginCredentials.token";

    public static Token sToken;
    private String mKey;
    private String mId;
    private Role mRole;


    public Token(String key) {
        this.mKey = key;

    }

    public static Token get(Context context) {
        if (sToken == null) {
            sToken = new Token(null);
            sToken.setToken(sToken.getSavedToken(context),context);
        }
        return sToken;
    }

    public static String getToken() {
        return sToken.mKey;
    }

    public static void setToken(String mToken, Context context) {
        sToken.mKey = mToken;
        if (mToken!=null) {
            sToken.saveToken(mToken, context);
            sToken.setId(sToken.getIdFromToken());
            sToken.setRole(sToken.getRoleFromToken());
        }
    }

    public String getId() {
        return sToken.mId;
    }

    public void setId(String id) {
        sToken.mId = id;
    }

    public Role getRole() {
        return sToken.mRole;
    }

    public void setRole(Role role) {
        sToken.mRole = role;
    }

    private String getSavedToken(Context context) {

        String jwt = null;
        long expire = 0;
        long date = 0;


        SharedPreferences sharedPreferences = context.getSharedPreferences(sharedPref, Context.MODE_PRIVATE);
        jwt = sharedPreferences.getString(tokenPref,null);
        if (jwt == null || jwt.equals("")) return null;

        String[] base64UrlEncodedSegments = jwt.split("\\.");
        String claims = Base64UrlCodec.BASE64.decodeToString(base64UrlEncodedSegments[1]);
        try {
            final JSONObject obj = new JSONObject(claims);
            String exp = obj.getString("exp");
            expire = Long.parseLong(exp);
            date = System.currentTimeMillis() /1000;
            System.out.println(exp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (expire>date) return jwt;
        else return null;
    }

    private String getIdFromToken() {
        String id = null;

        String[] base64UrlEncodedSegments = sToken.getToken().split("\\.");
        String claims = Base64UrlCodec.BASE64.decodeToString(base64UrlEncodedSegments[1]);
        try {
            final JSONObject obj = new JSONObject(claims);
            id = obj.getString("_id");
            System.out.println(id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return id;
    }

    private Role getRoleFromToken() {
        String role = null;

        String[] base64UrlEncodedSegments = sToken.getToken().split("\\.");
        String claims = Base64UrlCodec.BASE64.decodeToString(base64UrlEncodedSegments[1]);
        try {
            final JSONObject obj = new JSONObject(claims);
            final JSONArray roles = obj.getJSONArray("roles");
            role = (String) roles.get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Role.valueOf(role);
    }

    private void saveToken(String token, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(sharedPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(tokenPref,token);
        editor.commit();
    }
}


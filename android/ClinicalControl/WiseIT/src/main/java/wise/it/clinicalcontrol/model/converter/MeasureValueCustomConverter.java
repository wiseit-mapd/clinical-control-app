package wise.it.clinicalcontrol.model.converter;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import wise.it.clinicalcontrol.model.measure.HealthMeasure;

public class MeasureValueCustomConverter implements CustomConverter<Object> {

    @Override
    public Object convertToModel(String property, JSONObject jsonObject) throws JSONException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException{
        final HealthMeasure.Type type = Enum.valueOf(HealthMeasure.Type.class, jsonObject.getString("type"));
        return type.getValueClass().getConstructor(String.class).newInstance(jsonObject.getString(property));
    }

    @Override
    public void convertToJSON(final Field field, final Object instance, final Map<String,Object> propertiesMap) {
        final Field typeField = ReflectionUtils.findField(instance.getClass(), "type");
        ReflectionUtils.makeAccessible(typeField);
        final HealthMeasure.Type type = (HealthMeasure.Type) ReflectionUtils.getField(typeField, instance);
        ReflectionUtils.makeAccessible(field);
        final Object value =  type.getValueClass().cast(ReflectionUtils.getField(field, instance));
        propertiesMap.put(field.getName(), value);

    }


}

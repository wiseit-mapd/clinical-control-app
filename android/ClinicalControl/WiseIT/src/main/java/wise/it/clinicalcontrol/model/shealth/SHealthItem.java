package wise.it.clinicalcontrol.model.shealth;

import org.joda.time.DateTime;

public class SHealthItem {

    private final String id;
    private final DateTime dateTime;
    private final String type;
    private final Object value;

    public SHealthItem(final String id, final DateTime dateTime, final String type, final Object value) {
        this.id = id;
        this.dateTime = dateTime;
        this.type = type;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public DateTime getDateTime() {
        return dateTime;
    }

    public String getType() {
        return type;
    }

    public Object getValue() {
        return value;
    }
}

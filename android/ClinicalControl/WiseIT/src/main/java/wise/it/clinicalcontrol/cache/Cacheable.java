package wise.it.clinicalcontrol.cache;

public interface Cacheable {

    String generateKey();
}

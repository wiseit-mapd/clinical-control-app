package wise.it.clinicalcontrol;

import android.support.v4.app.Fragment;

/**
 * Created by rider on 12/16/2016.
 */

public class ListTestActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return ListTestFragment.newInstance();
    }
}

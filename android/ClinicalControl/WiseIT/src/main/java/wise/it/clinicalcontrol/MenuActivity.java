//WiseIT
package wise.it.clinicalcontrol;

import android.support.v4.app.Fragment;

import wise.it.clinicalcontrol.doctor.DoctorMenuFragment;
import wise.it.clinicalcontrol.patient.PatientMenuFragment;

public class MenuActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        switch(Token.sToken.getRole().toString()) {
            case "DOCTOR": return DoctorMenuFragment.newInstance();
            case "PATIENT": return PatientMenuFragment.newInstance();
            default: return null;
        }
    }
    /*protected Fragment createFragment() {
        return new ListPatientFragment();
    }*/
    /*protected Fragment createFragment() {
        return new ListTestFragment();
    }*/
}

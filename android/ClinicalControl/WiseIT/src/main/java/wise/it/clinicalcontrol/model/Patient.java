package wise.it.clinicalcontrol.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import wise.it.clinicalcontrol.model.converter.IgnoreOnConversion;

@IgnoreOnConversion("doc_ids")
public final class Patient extends AbstractIdentifiableAuthenticableWrapper {

    private String diagnosis;

    public Patient() {
    }

    public Patient(final String id,
                   final String authenticableId,
                   final String username,
                   final String password,
                   final Set<Authenticable.Role> roles,
                   final String name,
                   final String email,
                   final String phone,
                   final String dateOfBirth,
                   final Authenticable.Gender gender,
                   final Object photo,
                   final String diagnosis,
                   final String address) {
        super(id, authenticableId, username, password, roles, name, email, phone, dateOfBirth, gender, photo, address);
        this.diagnosis = diagnosis;
    }

    public Patient(final String name,
                   final String email,
                   final String phone,
                   final String dateOfBirth,
                   final String diagnosis,
                   final Authenticable.Gender gender,
                   final Object photo,
                   final String address) {
        authenticable.setName(name);
        authenticable.setEmail(email);
        authenticable.setPhone(phone);
        authenticable.setDateOfBirth(dateOfBirth);
        authenticable.setGender(gender);
        authenticable.setPhoto(photo);
        authenticable.setAddress(address);
        this.diagnosis = diagnosis;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
}

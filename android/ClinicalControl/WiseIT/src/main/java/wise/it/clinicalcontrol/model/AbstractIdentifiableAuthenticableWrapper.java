package wise.it.clinicalcontrol.model;

import java.util.Set;

import wise.it.clinicalcontrol.model.AbstractIdentifiable;
import wise.it.clinicalcontrol.model.Authenticable;
import wise.it.clinicalcontrol.model.CommonAuthenticable;
import wise.it.clinicalcontrol.model.converter.AuthenticableConverter;
import wise.it.clinicalcontrol.model.converter.CustomConvertion;

public abstract class AbstractIdentifiableAuthenticableWrapper extends AbstractIdentifiable implements AuthenticableWrapper {

    @CustomConvertion(converter = AuthenticableConverter.class)
    protected CommonAuthenticable authenticable = new CommonAuthenticable();

    public AbstractIdentifiableAuthenticableWrapper() {
    }

    public AbstractIdentifiableAuthenticableWrapper(final String id) {
        super(id);
    }

    public AbstractIdentifiableAuthenticableWrapper(
            final String id,
            final String authenticableId,
            final String username,
            final String password,
            final Set<Authenticable.Role> roles,
            final String name,
            final String email,
            final String phone,
            final String dateOfBirth,
            final Authenticable.Gender gender,
            final Object photo,
            final String address) {
        super(id);
        authenticable.setAuthenticableId(authenticableId);
        authenticable.setUsername(username);
        authenticable.setPassword(password);
        authenticable.setRoles(roles);
        authenticable.setName(name);
        authenticable.setEmail(email);
        authenticable.setPhone(phone);
        authenticable.setDateOfBirth(dateOfBirth);
        authenticable.setGender(gender);
        authenticable.setPhoto(photo);
        authenticable.setAddress(address);
    }

    @Override
    public Authenticable getAuthenticable() {
        return authenticable;
    }
}

package wise.it.clinicalcontrol.model;

public interface AuthenticableWrapper {

    Authenticable getAuthenticable();
}

package wise.it.clinicalcontrol;

import android.support.v4.app.Fragment;

/**
 * Created by rider on 12/15/2016.
 */

public class EditAppointmentActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return EditAppointmentFragment.newInstance();
    }
}

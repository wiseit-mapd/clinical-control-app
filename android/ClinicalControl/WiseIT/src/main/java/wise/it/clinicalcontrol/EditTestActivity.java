package wise.it.clinicalcontrol;

import android.support.v4.app.Fragment;

public class EditTestActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return EditTestFragment.newInstance();
    }
}

package wise.it.clinicalcontrol.doctor;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import wise.it.clinicalcontrol.EditTestActivity;
import wise.it.clinicalcontrol.EditTestFragment;
import wise.it.clinicalcontrol.ListTestFragment;
import wise.it.clinicalcontrol.R;
import wise.it.clinicalcontrol.model.Authenticable;
import wise.it.clinicalcontrol.model.Patient;

public class PatientCardAdapter extends RecyclerView.Adapter<PatientCardAdapter.PatientAdapterHolder> {

    public static class PatientAdapterHolder extends RecyclerView.ViewHolder {
        private final TextView nameTextView;
        private final TextView diagnosisTextView;

        public PatientAdapterHolder(final View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.text_patient_name);
            diagnosisTextView = (TextView) itemView.findViewById(R.id.text_patient_diagnosis);
        }
    }

    private final List<Patient> dataSet;

    public PatientCardAdapter(final List<Patient> dataset) {
        this.dataSet = dataset;
    }

    @Override
    public PatientAdapterHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.patient_card, parent, false);
        return new PatientAdapterHolder(view);
    }

    @Override
    public void onBindViewHolder(final PatientAdapterHolder holder, final int position) {
        final Patient patient = dataSet.get(position);
        holder.nameTextView.setText(patient.getAuthenticable().getName());
        holder.diagnosisTextView.setText(patient.getDiagnosis());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Context context = holder.itemView.getContext();
                final Intent intent = new Intent(context, DoctorPatientActivity.class);
                intent.putExtra(DoctorPatientFragment.PATIENT_PARAM_KEY, patient);
                intent.putExtra(ListTestFragment.PATIENT_ID_PARAM, patient.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}

package wise.it.clinicalcontrol;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import wise.it.clinicalcontrol.model.measure.HealthMeasure;
import wise.it.clinicalcontrol.model.shealth.SHealthItem;
import wise.it.clinicalcontrol.util.Utils;

public class SHealthCardAdapter extends RecyclerView.Adapter<SHealthCardAdapter.SHealthAdapterHolder> {
    private static final String measureValuePattern = "%s%s";

    public static class SHealthAdapterHolder extends RecyclerView.ViewHolder {
        private final TextView typeTextView;
        private final TextView valueTextView;

        public SHealthAdapterHolder(final View itemView) {
            super(itemView);
            typeTextView = (TextView) itemView.findViewById(R.id.text_test_type);
            valueTextView = (TextView) itemView.findViewById(R.id.text_test_value);
        }
    }

    private final List<SHealthItem> dataSet;
    private String packageName;

    public SHealthCardAdapter(final List<SHealthItem> dataset) {
        this.dataSet = dataset;
    }

    @Override
    public SHealthAdapterHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        this.packageName = parent.getContext().getPackageName();
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shealth_card, parent, false);
        return new SHealthAdapterHolder(view);
    }

    @Override
    public void onBindViewHolder(final SHealthAdapterHolder holder, final int position) {
        final SHealthItem item = dataSet.get(position);
        final Resources resources = holder.itemView.getResources();
        final String measureTypeBundle = Utils.getBundle(resources, HealthMeasure.Type.HEART_BEAT_RATE.name().toLowerCase() + "_text", packageName);
        holder.typeTextView.setText(measureTypeBundle);
        holder.valueTextView.setText(String.format(measureValuePattern, item.getValue().toString(), HealthMeasure.Type.HEART_BEAT_RATE.getValueUnitString(resources, packageName)));
        holder.valueTextView.setText(item.getValue().toString());
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}

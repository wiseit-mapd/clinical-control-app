package wise.it.clinicalcontrol.model.converter;

import android.support.v4.util.Pair;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;

@Target({TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ConvertCasting {

    String[] properties();
    Class<?>[] targetClasses();
}

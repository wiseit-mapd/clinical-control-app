package wise.it.clinicalcontrol;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import wise.it.clinicalcontrol.cache.Cache;
import wise.it.clinicalcontrol.model.Authenticable;
import wise.it.clinicalcontrol.model.Doctor;
import wise.it.clinicalcontrol.model.Patient;
import wise.it.clinicalcontrol.model.appointment.Appointment;
import wise.it.clinicalcontrol.model.appointment.AppointmentImpl;
import wise.it.clinicalcontrol.network.HttpMethod;
import wise.it.clinicalcontrol.network.RequestAsyncTask;
import wise.it.clinicalcontrol.network.RequestParams;
import wise.it.clinicalcontrol.util.Utils;

/**
 * Created by rider on 12/15/2016.
 */

public class EditAppointmentFragment extends Fragment {

    public static final String APPOINTMENT_PARAM_KEY = "appointment";
    public static final String PATIENT_ID_KEY = "patientId";
    public static final String DOCTOR_ID_KEY = "doctorId";

    private enum Mode {
        CREATE,
        EDIT;
    }

    private Mode mode;
    private String patientId;
    private String doctorId;
    private DateTime appointmentDateTime = null;


    private Collection<Doctor> doctors;
    private Map<String,Doctor> doctorByAuthenticableId;
    private Map<String, String> doctorNameId;


    private final RequestAsyncTask.PostProcessor createAppointmentPostProcessor = new RequestAsyncTask.PostProcessor() {
        @Override
        public void postProcess(final RequestAsyncTask.Response response) {
            if (response.getResult() != null) {
                getActivity().finish();
            } else {
                Toast.makeText(getContext(), getString(R.string.failed_create_appointment), Toast.LENGTH_LONG).show();
            }
        }
    };

    private final RequestAsyncTask.PostProcessor cancelAppointmentPostProcessor = new RequestAsyncTask.PostProcessor() {
        @Override
        public void postProcess(final RequestAsyncTask.Response response) {
            System.out.println(response.getResult());
            if (response.getResult() != null) {
                getView().findViewById(R.id.edit_appointment_doctor_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_appointment_date_time_button).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_appointment_reason_editable).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_appointment_request_message_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_appointment_accept_decline_button_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_appointment_start_button_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_appointment_start_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_appointment_request_cancel_button_container).setVisibility(View.GONE);

                try {
                    final Appointment appointment = Utils.convertToModel(new JSONObject(response.getResult()), AppointmentImpl.class);
                    Date date = appointment.getAppointmentDate().toDate();
                    ((TextView)getView().findViewById(R.id.edit_appointment_date_time_text)).setText(date.toString());
                    ((TextView)getView().findViewById(R.id.edit_appointment_reason_view)).setText(appointment.getReason());
                } catch (final JSONException e) {
                    throw new RuntimeException(e);
                }
            } else {
                Toast.makeText(getContext(), getString(R.string.failed_edit_appointment), Toast.LENGTH_LONG).show();
            }
        }
    };

    private final RequestAsyncTask.PostProcessor finishAppointmentPostProcessor = new RequestAsyncTask.PostProcessor() {
        @Override
        public void postProcess(final RequestAsyncTask.Response response) {
            System.out.println(response.getResult());
            if (response.getResult() != null) {
                getView().findViewById(R.id.edit_appointment_doctor_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_appointment_date_time_button).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_appointment_reason_editable).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_appointment_request_message_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_appointment_accept_decline_button_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_appointment_start_button_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_appointment_done).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_appointment_condition_editable_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_appointment_prescription_editable_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_appointment_request_cancel_button_container).setVisibility(View.GONE);

                try {
                    final Appointment appointment = Utils.convertToModel(new JSONObject(response.getResult()), AppointmentImpl.class);
                    Date date = appointment.getAppointmentDate().toDate();
                    ((TextView)getView().findViewById(R.id.edit_appointment_date_time_text)).setText(date.toString());
                    ((TextView)getView().findViewById(R.id.edit_appointment_reason_view)).setText(appointment.getReason());
                    ((TextView)getView().findViewById(R.id.edit_appointment_condition_view)).setText(appointment.getCondition());
                    ((TextView)getView().findViewById(R.id.edit_appointment_prescription_view)).setText(appointment.getPrescription());
                } catch (final JSONException e) {
                    throw new RuntimeException(e);
                }
            } else {
                Toast.makeText(getContext(), getString(R.string.failed_edit_appointment), Toast.LENGTH_LONG).show();
            }
        }
    };

    public static EditAppointmentFragment newInstance() {
        
        Bundle args = new Bundle();
        
        EditAppointmentFragment fragment = new EditAppointmentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_edit_appointment, container, false);

        doctors = Cache.getInstance().get(Doctor.class);
        doctorByAuthenticableId = new HashMap<>();
        for (final Doctor doctor : doctors) {
            doctorByAuthenticableId.put(doctor.getAuthenticable().getAuthenticableId(), doctor);
        }

        doctorNameId = new HashMap<>();
        for (Map.Entry<String,Doctor> doctor : doctorByAuthenticableId.entrySet()) {
            doctorNameId.put(doctor.getValue().getAuthenticable().getName(), doctor.getKey());
        }

        final Appointment appointment = (Appointment) getActivity().getIntent().getSerializableExtra(APPOINTMENT_PARAM_KEY);
        if (appointment == null) {
            patientId = getActivity().getIntent().getStringExtra(PATIENT_ID_KEY);
            if (patientId == null) {
                throw new IllegalStateException(String.format("%s must be provided.", PATIENT_ID_KEY));
            }
            mode = Mode.CREATE;
            configureCreateMode(v);
        } else {
            patientId = appointment.getPatientId();
            doctorId= appointment.getDoctorId();
            mode = Mode.EDIT;
            configureEditMode(v, appointment);
        }
        return v;
    }

    private void configureCreateMode(final View view) {
        switch (Token.sToken.getRole()) {
            case DOCTOR: {
                view.findViewById(R.id.edit_appointment_doctor_container).setVisibility(View.GONE);
                view.findViewById(R.id.edit_appointment_date_time_text).setVisibility(View.GONE);
                view.findViewById(R.id.edit_appointment_reason_read_only_container).setVisibility(View.GONE);
                view.findViewById(R.id.edit_appointment_request_message_container).setVisibility(View.GONE);
                view.findViewById(R.id.edit_appointment_accept_decline_button_container).setVisibility(View.GONE);
                view.findViewById(R.id.edit_appointment_start_button_container).setVisibility(View.GONE);
                view.findViewById(R.id.edit_appointment_start_container).setVisibility(View.GONE);
                ((Button)view.findViewById(R.id.edit_appointment_request_cancel_button)).setText(getResources().getString(R.string.edit_appointment_request_cancel_button_request_label));


                ((Button)view.findViewById(R.id.edit_appointment_date_time_button)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new SingleDateAndTimePickerDialog.Builder(getContext())
                                //.bottomSheet()
                                //.curved()
                                .title(getString(R.string.edit_appointment_date_time_label))
                                .listener(new SingleDateAndTimePickerDialog.Listener() {
                                    @Override
                                    public void onDateSelected(Date date) {
                                        appointmentDateTime = new DateTime(date.getTime());
                                        ((Button)view.findViewById(R.id.edit_appointment_date_time_button)).setText(date.toString());
                                    }
                                }).display();
                    }
                });

                ((Button)view.findViewById(R.id.edit_appointment_request_cancel_button)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String reason = ((EditText)view.findViewById(R.id.edit_appointment_reason_editable)).getText().toString();
                        final String doctorId = Token.sToken.getId();
                        Appointment appointment = new AppointmentImpl(patientId,doctorId,appointmentDateTime,reason, Appointment.State.PROCESSING_PATIENT);
                        final JSONObject jsonObject = Utils.convertToJSON(appointment);
                        try {
                            final RequestParams requestParams = new RequestParams.Builder().setResource(Utils.APPOINTMENTS_RESOURCE).setHttpMethod(HttpMethod.POST).setJsonObject(jsonObject).buildWithAuthorization();
                            final RequestAsyncTask requestAsyncTask = new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(createAppointmentPostProcessor).buildWithDialog(getContext());
                            requestAsyncTask.execute();
                        } catch (final MalformedURLException e) {
                            throw new RuntimeException(e);
                        }
                    }
                });
                break;
            }

            case PATIENT: {
                view.findViewById(R.id.edit_appointment_doctor_text).setVisibility(View.GONE);
                view.findViewById(R.id.edit_appointment_date_time_text).setVisibility(View.GONE);
                view.findViewById(R.id.edit_appointment_reason_read_only_container).setVisibility(View.GONE);
                view.findViewById(R.id.edit_appointment_request_message_container).setVisibility(View.GONE);
                view.findViewById(R.id.edit_appointment_accept_decline_button_container).setVisibility(View.GONE);
                view.findViewById(R.id.edit_appointment_start_button_container).setVisibility(View.GONE);
                view.findViewById(R.id.edit_appointment_start_container).setVisibility(View.GONE);
                ((Button)view.findViewById(R.id.edit_appointment_request_cancel_button)).setText(getResources().getString(R.string.edit_appointment_request_cancel_button_request_label));

                final Spinner spinner = ((Spinner)view.findViewById(R.id.edit_appointment_doctor_spinner));
                List<String> spinnerArray = new ArrayList<String>();
                for (Map.Entry<String, String> doctor: doctorNameId.entrySet()) {
                    spinnerArray.add(doctor.getKey());
                }
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, spinnerArray);
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                spinner.setAdapter(spinnerArrayAdapter);

                ((Button)view.findViewById(R.id.edit_appointment_date_time_button)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new SingleDateAndTimePickerDialog.Builder(getContext())
                                //.bottomSheet()
                                //.curved()
                                .title(getString(R.string.edit_appointment_date_time_label))
                                .listener(new SingleDateAndTimePickerDialog.Listener() {
                                    @Override
                                    public void onDateSelected(Date date) {
                                        appointmentDateTime = new DateTime(date.getTime());
                                        ((Button)view.findViewById(R.id.edit_appointment_date_time_button)).setText(date.toString());
                                    }
                                }).display();
                    }
                });

                ((Button)view.findViewById(R.id.edit_appointment_request_cancel_button)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String reason = ((EditText)view.findViewById(R.id.edit_appointment_reason_editable)).getText().toString();
                        final String doctorId = doctorNameId.get(spinner.getSelectedItem().toString());
                        Appointment appointment = new AppointmentImpl(patientId,doctorId,appointmentDateTime,reason, Appointment.State.PROCESSING_DOCTOR);
                        final JSONObject jsonObject = Utils.convertToJSON(appointment);
                        try {
                            final RequestParams requestParams = new RequestParams.Builder().setResource(Utils.APPOINTMENTS_RESOURCE).setHttpMethod(HttpMethod.POST).setJsonObject(jsonObject).buildWithAuthorization();
                            final RequestAsyncTask requestAsyncTask = new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(createAppointmentPostProcessor).buildWithDialog(getContext());
                            requestAsyncTask.execute();
                        } catch (final MalformedURLException e) {
                            throw new RuntimeException(e);
                        }
                    }
                });
                break;
            }
        }
    }

    private void configureEditMode(final View view, final Appointment appointment) {
        System.out.println(Token.sToken.getRole());
        switch (Token.sToken.getRole()) {
            case DOCTOR: {
                switch(appointment.getState()) {
                    case PROCESSING_PATIENT: {
                        view.findViewById(R.id.edit_appointment_doctor_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_date_time_button).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_reason_editable).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_request_message_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_accept_decline_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_start_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_start_container).setVisibility(View.GONE);
                        ((Button)view.findViewById(R.id.edit_appointment_request_cancel_button)).setText(getResources().getString(R.string.edit_appointment_request_cancel_button_cancel_label));

                        Date date = appointment.getAppointmentDate().toDate();
                        ((TextView)view.findViewById(R.id.edit_appointment_date_time_text)).setText(date.toString());
                        ((TextView)view.findViewById(R.id.edit_appointment_reason_view)).setText(appointment.getReason());

                        ((Button)view.findViewById(R.id.edit_appointment_request_cancel_button)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    appointment.setState(Appointment.State.CANCELLED);

                                    final JSONObject jsonObject = Utils.convertToJSON(appointment);
                                    System.out.println(jsonObject);
                                    System.out.println(appointment.getId());
                                    final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.APPOINTMENTS_BY_ID, appointment.getId())).setJsonObject(jsonObject).setHttpMethod(HttpMethod.PUT).buildWithAuthorization();
                                    new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(cancelAppointmentPostProcessor).buildWithDialog(getContext()).execute();
                                } catch (MalformedURLException e) {
                                    throw new RuntimeException(e);
                                }

                            }
                        });
                        break;
                    }

                    case CANCELLED: {
                        view.findViewById(R.id.edit_appointment_doctor_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_date_time_button).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_reason_editable).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_request_message_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_accept_decline_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_start_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_start_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_request_cancel_button_container).setVisibility(View.GONE);

                        Date date = appointment.getAppointmentDate().toDate();
                        ((TextView)view.findViewById(R.id.edit_appointment_date_time_text)).setText(date.toString());
                        ((TextView)view.findViewById(R.id.edit_appointment_reason_view)).setText(appointment.getReason());
                        break;
                    }

                    case APPROVED: {
                        view.findViewById(R.id.edit_appointment_doctor_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_date_time_button).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_reason_editable).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_request_message_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_accept_decline_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_start_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_condition_read_only_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_prescription_read_only_container).setVisibility(View.GONE);
                        ((Button)view.findViewById(R.id.edit_appointment_request_cancel_button)).setText(getResources().getString(R.string.edit_appointment_request_cancel_button_cancel_label));

                        Date date = appointment.getAppointmentDate().toDate();
                        ((TextView)view.findViewById(R.id.edit_appointment_date_time_text)).setText(date.toString());
                        ((TextView)view.findViewById(R.id.edit_appointment_reason_view)).setText(appointment.getReason());

                        ((Button)view.findViewById(R.id.edit_appointment_request_cancel_button)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    appointment.setState(Appointment.State.CANCELLED);

                                    final JSONObject jsonObject = Utils.convertToJSON(appointment);
                                    System.out.println(jsonObject);
                                    System.out.println(appointment.getId());
                                    final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.APPOINTMENTS_BY_ID, appointment.getId())).setJsonObject(jsonObject).setHttpMethod(HttpMethod.PUT).buildWithAuthorization();
                                    new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(cancelAppointmentPostProcessor).buildWithDialog(getContext()).execute();
                                } catch (MalformedURLException e) {
                                    throw new RuntimeException(e);
                                }

                            }
                        });

                        ((Button)view.findViewById(R.id.edit_appointment_done)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                try {
                                    appointment.setState(Appointment.State.DONE);
                                    appointment.setCondition(((EditText)view.findViewById(R.id.edit_appointment_condition_editable)).getText().toString());
                                    appointment.setPrescription(((EditText)view.findViewById(R.id.edit_appointment_prescription_editable)).getText().toString());

                                    final JSONObject jsonObject = Utils.convertToJSON(appointment);

                                    final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.APPOINTMENTS_BY_ID, appointment.getId())).setJsonObject(jsonObject).setHttpMethod(HttpMethod.PUT).buildWithAuthorization();
                                    new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(finishAppointmentPostProcessor).buildWithDialog(getContext()).execute();
                                } catch (MalformedURLException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        });

                        ((Button)view.findViewById(R.id.edit_appointment_start_button)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                view.findViewById(R.id.edit_appointment_start_container).setVisibility(View.VISIBLE);
                                view.findViewById(R.id.edit_appointment_start_button_container).setVisibility(View.GONE);
                            }
                        });

                        break;
                    }

                    case DONE: {
                        view.findViewById(R.id.edit_appointment_doctor_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_date_time_button).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_reason_editable).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_request_message_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_accept_decline_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_start_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_done).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_condition_editable_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_prescription_editable_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_request_cancel_button_container).setVisibility(View.GONE);

                        Date date = appointment.getAppointmentDate().toDate();
                        ((TextView)view.findViewById(R.id.edit_appointment_date_time_text)).setText(date.toString());
                        ((TextView)view.findViewById(R.id.edit_appointment_reason_view)).setText(appointment.getReason());
                        ((TextView)view.findViewById(R.id.edit_appointment_condition_view)).setText(appointment.getCondition());
                        ((TextView)view.findViewById(R.id.edit_appointment_prescription_view)).setText(appointment.getPrescription());

                        break;
                    }

                    case PROCESSING_DOCTOR: {
                        view.findViewById(R.id.edit_appointment_date_reason_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_request_cancel_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_start_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_start_container).setVisibility(View.GONE);
                        Date date = appointment.getAppointmentDate().toDate();
                        String message = String.format(getString(R.string.doctor_appointment_request_message), Cache.getInstance().get(Patient.class,patientId).getAuthenticable().getName(),date.toString(),appointment.getReason());
                        ((TextView)view.findViewById(R.id.edit_appointment_request_message_view)).setText(message);

                        ((Button)view.findViewById(R.id.edit_appointment_accept_button)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    appointment.setState(Appointment.State.APPROVED);

                                    final JSONObject jsonObject = Utils.convertToJSON(appointment);
                                    final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.APPOINTMENTS_BY_ID, appointment.getId())).setJsonObject(jsonObject).setHttpMethod(HttpMethod.PUT).buildWithAuthorization();
                                    new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(cancelAppointmentPostProcessor).buildWithDialog(getContext()).execute();
                                } catch (MalformedURLException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        });

                        ((Button)view.findViewById(R.id.edit_appointment_decline_button)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    appointment.setState(Appointment.State.CANCELLED);

                                    final JSONObject jsonObject = Utils.convertToJSON(appointment);
                                    final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.APPOINTMENTS_BY_ID, appointment.getId())).setJsonObject(jsonObject).setHttpMethod(HttpMethod.PUT).buildWithAuthorization();
                                    new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(cancelAppointmentPostProcessor).buildWithDialog(getContext()).execute();
                                } catch (MalformedURLException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        });
                        break;
                    }
                }
                break;
            }

            case PATIENT: {
                switch(appointment.getState()) {
                    case CANCELLED: {
                        view.findViewById(R.id.edit_appointment_doctor_spinner).setVisibility(View.GONE);
                        //view.findViewById(R.id.edit_appointment_doctor_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_date_time_button).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_reason_editable).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_request_message_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_accept_decline_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_start_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_start_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_request_cancel_button_container).setVisibility(View.GONE);

                        Date date = appointment.getAppointmentDate().toDate();
                        //System.out.println(doctorByAuthenticableId.get(appointment.getDoctorId()).getAuthenticable().getName());
                        ((TextView)view.findViewById(R.id.edit_appointment_doctor_text)).setText(doctorByAuthenticableId.get(appointment.getDoctorId()).getAuthenticable().getName());
                        ((TextView)view.findViewById(R.id.edit_appointment_date_time_text)).setText(date.toString());
                        ((TextView)view.findViewById(R.id.edit_appointment_reason_view)).setText(appointment.getReason());

                        break;
                    }

                    case APPROVED: {
                        view.findViewById(R.id.edit_appointment_doctor_spinner).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_date_time_button).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_reason_editable).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_request_message_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_accept_decline_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_start_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_start_container).setVisibility(View.GONE);
                        ((Button)view.findViewById(R.id.edit_appointment_request_cancel_button)).setText(getResources().getString(R.string.edit_appointment_request_cancel_button_cancel_label));

                        Date date = appointment.getAppointmentDate().toDate();
                        ((TextView)view.findViewById(R.id.edit_appointment_doctor_text)).setText(doctorByAuthenticableId.get(appointment.getDoctorId()).getAuthenticable().getName());
                        ((TextView)view.findViewById(R.id.edit_appointment_date_time_text)).setText(date.toString());
                        ((TextView)view.findViewById(R.id.edit_appointment_reason_view)).setText(appointment.getReason());

                        ((Button)view.findViewById(R.id.edit_appointment_request_cancel_button)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    appointment.setState(Appointment.State.CANCELLED);

                                    final JSONObject jsonObject = Utils.convertToJSON(appointment);
                                    System.out.println(jsonObject);
                                    System.out.println(appointment.getId());
                                    final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.APPOINTMENTS_BY_ID, appointment.getId())).setJsonObject(jsonObject).setHttpMethod(HttpMethod.PUT).buildWithAuthorization();
                                    new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(cancelAppointmentPostProcessor).buildWithDialog(getContext()).execute();
                                } catch (MalformedURLException e) {
                                    throw new RuntimeException(e);
                                }

                            }
                        });

                        break;
                    }

                    case DONE: {
                        view.findViewById(R.id.edit_appointment_doctor_spinner).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_date_time_button).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_reason_editable).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_request_message_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_accept_decline_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_start_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_done).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_condition_editable_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_prescription_editable_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_request_cancel_button_container).setVisibility(View.GONE);

                        Date date = appointment.getAppointmentDate().toDate();
                        ((TextView)view.findViewById(R.id.edit_appointment_doctor_text)).setText(doctorByAuthenticableId.get(appointment.getDoctorId()).getAuthenticable().getName());
                        ((TextView)view.findViewById(R.id.edit_appointment_date_time_text)).setText(date.toString());
                        ((TextView)view.findViewById(R.id.edit_appointment_reason_view)).setText(appointment.getReason());
                        ((TextView)view.findViewById(R.id.edit_appointment_condition_view)).setText(appointment.getCondition());
                        ((TextView)view.findViewById(R.id.edit_appointment_prescription_view)).setText(appointment.getPrescription());
                        break;
                    }

                    case PROCESSING_DOCTOR: {
                        view.findViewById(R.id.edit_appointment_doctor_spinner).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_date_time_button).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_reason_editable).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_request_message_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_accept_decline_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_start_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_start_container).setVisibility(View.GONE);
                        ((Button)view.findViewById(R.id.edit_appointment_request_cancel_button)).setText(getResources().getString(R.string.edit_appointment_request_cancel_button_cancel_label));

                        Date date = appointment.getAppointmentDate().toDate();
                        ((TextView)view.findViewById(R.id.edit_appointment_doctor_text)).setText(doctorByAuthenticableId.get(appointment.getDoctorId()).getAuthenticable().getName());
                        ((TextView)view.findViewById(R.id.edit_appointment_date_time_text)).setText(date.toString());
                        ((TextView)view.findViewById(R.id.edit_appointment_reason_view)).setText(appointment.getReason());

                        ((Button)view.findViewById(R.id.edit_appointment_request_cancel_button)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    appointment.setState(Appointment.State.CANCELLED);

                                    final JSONObject jsonObject = Utils.convertToJSON(appointment);
                                    System.out.println(jsonObject);
                                    System.out.println(appointment.getId());
                                    final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.APPOINTMENTS_BY_ID, appointment.getId())).setJsonObject(jsonObject).setHttpMethod(HttpMethod.PUT).buildWithAuthorization();
                                    new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(cancelAppointmentPostProcessor).buildWithDialog(getContext()).execute();
                                } catch (MalformedURLException e) {
                                    throw new RuntimeException(e);
                                }

                            }
                        });
                        break;
                    }

                    case PROCESSING_PATIENT: {
                        view.findViewById(R.id.edit_appointment_date_reason_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_request_cancel_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_start_button_container).setVisibility(View.GONE);
                        view.findViewById(R.id.edit_appointment_start_container).setVisibility(View.GONE);
                        Date date = appointment.getAppointmentDate().toDate();
                        String message = String.format(getString(R.string.patient_appointment_request_message), doctorByAuthenticableId.get(appointment.getDoctorId()).getAuthenticable().getName(),date.toString(),appointment.getReason());
                        ((TextView)view.findViewById(R.id.edit_appointment_request_message_view)).setText(message);

                        ((Button)view.findViewById(R.id.edit_appointment_accept_button)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    appointment.setState(Appointment.State.APPROVED);

                                    final JSONObject jsonObject = Utils.convertToJSON(appointment);
                                    final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.APPOINTMENTS_BY_ID, appointment.getId())).setJsonObject(jsonObject).setHttpMethod(HttpMethod.PUT).buildWithAuthorization();
                                    new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(cancelAppointmentPostProcessor).buildWithDialog(getContext()).execute();
                                } catch (MalformedURLException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        });

                        ((Button)view.findViewById(R.id.edit_appointment_decline_button)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    appointment.setState(Appointment.State.CANCELLED);

                                    final JSONObject jsonObject = Utils.convertToJSON(appointment);
                                    final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.APPOINTMENTS_BY_ID, appointment.getId())).setJsonObject(jsonObject).setHttpMethod(HttpMethod.PUT).buildWithAuthorization();
                                    new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(cancelAppointmentPostProcessor).buildWithDialog(getContext()).execute();
                                } catch (MalformedURLException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        });
                        break;
                    }
                }
                break;
            }
        }
    }


}

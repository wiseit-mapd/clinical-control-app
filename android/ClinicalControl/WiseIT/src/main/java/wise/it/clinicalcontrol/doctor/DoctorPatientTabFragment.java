package wise.it.clinicalcontrol.doctor;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import wise.it.clinicalcontrol.ListAppointmentFragment;
import wise.it.clinicalcontrol.ListTestFragment;
import wise.it.clinicalcontrol.R;

/**
 * Created by rider on 12/14/2016.
 */

public class DoctorPatientTabFragment extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Context mContext;
    private List<Fragment> mFragmentList;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /**
         *Inflate tab_layout and setup Views.
         */
        View x =  inflater.inflate(R.layout.patient_tab_layout,null);
        tabLayout = (TabLayout) x.findViewById(R.id.patient_sliding_tabs);
        viewPager = (ViewPager) x.findViewById(R.id.patient_viewpager);
        /**
         *Set an Apater for the View Pager
         */
        mFragmentList = new ArrayList<Fragment>();
        mFragmentList.add(new EditPatientFragment());
        mFragmentList.add(new ListAppointmentFragment());
        mFragmentList.add(new ListTestFragment());

        viewPager.setAdapter(new DoctorPatientPagerAdapter(getContext(),getActivity().getSupportFragmentManager(),mFragmentList));

        /**
         * Now , this is a workaround ,
         * The setupWithViewPager dose't works without the runnable .
         * Maybe a Support Library Bug .
         */

        tabLayout.setupWithViewPager(viewPager);

        return x;

    }
}

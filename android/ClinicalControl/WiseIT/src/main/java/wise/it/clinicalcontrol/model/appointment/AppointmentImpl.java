package wise.it.clinicalcontrol.model.appointment;

import org.joda.time.DateTime;

import wise.it.clinicalcontrol.model.AbstractIdentifiable;
import wise.it.clinicalcontrol.model.converter.CustomConvertion;

/**
 * Created by rider on 12/15/2016.
 */

public class AppointmentImpl extends AbstractIdentifiable implements Appointment {

    private String patientId;
    private String doctorId;
    private DateTime appointmentDate;
    private String reason;
    private String condition;
    private String prescription;
    private State state;


    public AppointmentImpl() {

    };

    public AppointmentImpl(final String patientId,
                            final String doctorId,
                            final DateTime appointmentDate,
                            final String reason,
                            final State state) {

        this.patientId = patientId;
        this.doctorId = doctorId;
        this.appointmentDate = appointmentDate;
        this.reason = reason;
        this.state = state;
        this.condition = null;
        this.prescription = null;

    }

    public AppointmentImpl(final String patientId,
                           final String doctorId,
                           final DateTime appointmentDate,
                           final String reason,
                           final String condition,
                           final String prescription,
                           final State state) {

        this.patientId = patientId;
        this.doctorId = doctorId;
        this.appointmentDate = appointmentDate;
        this.reason = reason;
        this.condition = condition;
        this.prescription = prescription;
        this.state = state;
    }

    @Override
    public String getPatientId() {
        return this.patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    @Override
    public String getDoctorId() {
        return this.doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    @Override
    public DateTime getAppointmentDate() {
        return this.appointmentDate;
    }

    public void setAppointmentDate(DateTime appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    @Override
    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String getCondition() {
        return this.condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    @Override
    public String getPrescription() {
        return this.prescription;
    }

    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }

    @Override
    public State getState() {
        return this.state;
    }

    public void setState(State state) {
        this.state = state;
    }
}

package wise.it.clinicalcontrol;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import wise.it.clinicalcontrol.cache.Cache;
import wise.it.clinicalcontrol.model.Authenticable;
import wise.it.clinicalcontrol.model.measure.HealthMeasure;
import wise.it.clinicalcontrol.model.measure.HealthMeasureImpl;
import wise.it.clinicalcontrol.network.HttpMethod;
import wise.it.clinicalcontrol.network.RequestAsyncTask;
import wise.it.clinicalcontrol.network.RequestParams;
import wise.it.clinicalcontrol.util.Utils;

public class EditTestFragment extends Fragment {
    private static class TypeAdapter {
        private final HealthMeasure.Type type;
        private final Context context;

        public TypeAdapter(final HealthMeasure.Type type, final Context context) {
            this.type = type;
            this.context = context;
        }

        public HealthMeasure.Type getType() {
            return type;
        }

        @Override
        public String toString() {
            return Utils.getBundle(context.getResources(), type.name().toLowerCase() + "_text", context.getPackageName());
        }
    }

    private enum Mode {
        CREATE,
        EDIT;
    }

    public static final String MEASURE_PARAM_KEY = "measure";
    public static final String PATIEND_ID_KEY = "patientId";
    private static final String measureValuePattern = "%s%s";

    private final RequestAsyncTask.PostProcessor createMeasurePostProcessor = new RequestAsyncTask.PostProcessor() {
        @Override
        public void postProcess(final RequestAsyncTask.Response response) {
            if (response.getResult() != null) {
                getActivity().finish();
            } else {
                Toast.makeText(getContext(), getString(R.string.failed_create_measure), Toast.LENGTH_LONG).show();
            }
        }
    };

    private final RequestAsyncTask.PostProcessor editMeasurePostProcessor = new RequestAsyncTask.PostProcessor() {
        @Override
        public void postProcess(final RequestAsyncTask.Response response) {
            if (response.getResult() != null) {
                getView().findViewById(R.id.edit_test_value_editable_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_test_value_read_only_container).setVisibility(View.VISIBLE);

                try {
                    final HealthMeasure measure = Utils.convertToModel(new JSONObject(response.getResult()), HealthMeasureImpl.class);

                    final TextView testValueView = ((TextView)getView().findViewById(R.id.edit_test_value_view));
                    testValueView.setText(String.format(measureValuePattern, measure.getValue().toString(), measure.getType().getValueUnitString(getResources(), getContext().getPackageName())));
                } catch (final JSONException e) {
                    throw new RuntimeException(e);
                }
            } else {
                Toast.makeText(getContext(), getString(R.string.failed_edit_measure), Toast.LENGTH_LONG).show();
            }
        }
    };

    private final RequestAsyncTask.PostProcessor deleteMeasurePostProcessor = new RequestAsyncTask.PostProcessor() {
        @Override
        public void postProcess(RequestAsyncTask.Response response) {
            if (response.getResult() != null) {
                getActivity().finish();
            } else {
                Toast.makeText(getContext(), getString(R.string.failed_delete_measure), Toast.LENGTH_LONG).show();
            }
        }
    };

    private NavigationView mNavigationView;
    private FragmentTransaction mFragmentTransaction;

    private String patientId;
    private Mode mode;

    public static EditTestFragment newInstance() {
        EditTestFragment fragment = new EditTestFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_edit_test, container, false);

        final HealthMeasure<?> measure = (HealthMeasure<?>) getActivity().getIntent().getSerializableExtra(MEASURE_PARAM_KEY);
        if (measure == null) {
            patientId = getActivity().getIntent().getStringExtra(PATIEND_ID_KEY);
            if (patientId == null) {
                throw new IllegalStateException(String.format("%s must to be provided.", PATIEND_ID_KEY));
            }
            mode = Mode.CREATE;
            configureCreateMode(v);
        } else {
            patientId = measure.getPatientId();
            mode = Mode.EDIT;
            configureEditMode(v, measure);
        }
        return v;
    }

    private void configureCreateMode(final View view) {
        view.findViewById(R.id.edit_test_type_text).setVisibility(View.GONE);
        view.findViewById(R.id.edit_test_delete).setVisibility(View.GONE);
        view.findViewById(R.id.edit_test_date_container).setVisibility(View.GONE);
        view.findViewById(R.id.edit_test_responsible_container).setVisibility(View.GONE);
        view.findViewById(R.id.edit_test_value_read_only_container).setVisibility(View.GONE);
        view.findViewById(R.id.edit_test_value_undo_button).setVisibility(View.GONE);
        view.findViewById(R.id.edit_test_value_apply_button).setVisibility(View.GONE);
        final Spinner spinner = ((Spinner)view.findViewById(R.id.edit_test_type_spinner));

        final HealthMeasure.Type[] types = HealthMeasure.Type.values();
        final List<TypeAdapter> adapters = new ArrayList<>(types.length);
        for (final HealthMeasure.Type type : types) {
            adapters.add(new TypeAdapter(type, getContext()));
        }
        spinner.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, adapters));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, final View currentView, final int position, final long id) {
                final TypeAdapter adapter = (TypeAdapter) spinner.getItemAtPosition(position);
                final TextView valueTextView = ((TextView)view.findViewById(R.id.edit_test_value_editable));
                valueTextView.setInputType(adapter.getType().getValueInputType());
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {

            }
        });

        view.findViewById(R.id.edit_test_create).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final HealthMeasure.Type type = ((TypeAdapter)spinner.getSelectedItem()).getType();
                final TextView valueTextView = ((TextView)view.findViewById(R.id.edit_test_value_editable));
                final Object value = type.getValueFromString(valueTextView.getText().toString());
                final String responsibleAuthenticableId = Token.sToken.getId();
                if (responsibleAuthenticableId == null) {
                    throw new IllegalStateException("Authentication required.");
                }
                final HealthMeasure<?> measure = new HealthMeasureImpl(null, patientId, responsibleAuthenticableId, null, new DateTime(), type, value);
                final JSONObject jsonObject = Utils.convertToJSON(measure);
                try {
                    final RequestParams requestParams = new RequestParams.Builder().setResource(Utils.MEASURES_RESOURCE).setHttpMethod(HttpMethod.POST).setJsonObject(jsonObject).buildWithAuthorization();
                    final RequestAsyncTask requestAsyncTask = new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(createMeasurePostProcessor).buildWithDialog(getContext());
                    requestAsyncTask.execute();
                } catch (final MalformedURLException e) {
                    throw new RuntimeException(e);
                }

            }
        });
    }

    private void configureEditMode(final View view, final HealthMeasure<?> measure) {
        view.findViewById(R.id.edit_test_type_spinner).setVisibility(View.GONE);
        view.findViewById(R.id.edit_test_create).setVisibility(View.GONE);
        view.findViewById(R.id.edit_test_value_editable_container).setVisibility(View.GONE);
        if (measure.getResponsibleAuthenticableId().equals(Token.sToken.getId())) {
            view.findViewById(R.id.edit_test_value_edit_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    view.findViewById(R.id.edit_test_value_read_only_container).setVisibility(View.GONE);
                    view.findViewById(R.id.edit_test_value_editable_container).setVisibility(View.VISIBLE);
                    final EditText testValueEditable = ((EditText)view.findViewById(R.id.edit_test_value_editable));
                    testValueEditable.setText(measure.getValue().toString());
                    testValueEditable.setInputType(measure.getType().getValueInputType());
                }
            });

            view.findViewById(R.id.edit_test_value_undo_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    InputMethodManager inputMethodManager =
                            (InputMethodManager) getActivity().getSystemService(
                                    Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                    getView().findViewById(R.id.edit_test_value_editable_container).setVisibility(View.GONE);
                    getView().findViewById(R.id.edit_test_value_read_only_container).setVisibility(View.VISIBLE);
                }
            });

            view.findViewById(R.id.edit_test_value_apply_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final HealthMeasure.Type type = measure.getType();
                    final TextView valueTextView = ((TextView)view.findViewById(R.id.edit_test_value_editable));
                    InputMethodManager inputMethodManager =
                            (InputMethodManager) getActivity().getSystemService(
                                    Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                    new AlertDialog.Builder(getContext())
                            .setMessage(R.string.message_save)
                            .setPositiveButton(android.R.string.yes,  new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        final Object value = type.getValueFromString(valueTextView.getText().toString());
                                        ((HealthMeasure<Object>)measure).setValue(value);

                                        final JSONObject jsonObject = Utils.convertToJSON(measure);
                                        final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.MEASURES_BY_ID_RESOURCE_PATTERN, measure.getId())).setJsonObject(jsonObject).setHttpMethod(HttpMethod.PUT).buildWithAuthorization();
                                        new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(editMeasurePostProcessor).buildWithDialog(getContext()).execute();
                                    } catch (MalformedURLException e) {
                                        throw new RuntimeException(e);
                                    }

                                }
                            })
                            .setNegativeButton(android.R.string.no,  new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            }).show();
                }
            });

            view.findViewById(R.id.edit_test_delete).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    new AlertDialog.Builder(getContext())
                            .setMessage(R.string.message_delete)
                            .setPositiveButton(android.R.string.yes,  new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.MEASURES_BY_ID_RESOURCE_PATTERN, measure.getId())).setHttpMethod(HttpMethod.DELETE).buildWithAuthorization();
                                        new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(deleteMeasurePostProcessor).buildWithDialog(getContext()).execute();
                                    } catch (MalformedURLException e) {
                                        throw new RuntimeException(e);
                                    }

                                }
                            })
                            .setNegativeButton(android.R.string.no,  new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            }).show();
                }
            });
        } else {
            view.findViewById(R.id.edit_test_value_edit_button).setVisibility(View.GONE);
            view.findViewById(R.id.edit_test_delete).setVisibility(View.GONE);
        }



        final String measureTypeBundle = Utils.getBundle(getResources(), measure.getType().name().toLowerCase() + "_text", getContext().getPackageName());
        ((TextView)view.findViewById(R.id.edit_test_type_text)).setText(measureTypeBundle);
        ((TextView)view.findViewById(R.id.edit_test_value_view)).setText(String.format(measureValuePattern, measure.getValue().toString(), measure.getType().getValueUnitString(getResources(), getContext().getPackageName())));
        ((TextView)view.findViewById(R.id.edit_test_date)).setText(measure.getMeasurementDate().toString(Utils.DATE_AND_TIME_FORMATTER));

        final Authenticable authenticable = Cache.getInstance().get(Authenticable.class,measure.getResponsibleAuthenticableId());
        ((TextView)view.findViewById(R.id.edit_test_responsible)).setText(authenticable.getName());
    }
}

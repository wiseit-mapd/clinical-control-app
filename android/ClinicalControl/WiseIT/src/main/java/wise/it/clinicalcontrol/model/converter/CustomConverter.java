package wise.it.clinicalcontrol.model.converter;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public interface CustomConverter<V> {

    V convertToModel(String property, JSONObject jsonObject) throws JSONException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException;

    void convertToJSON(Field field, Object instance, Map<String,Object> propertiesMap);
}

package wise.it.clinicalcontrol.model.measure;

import android.content.res.Resources;
import android.icu.util.Measure;
import android.text.InputType;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;

import wise.it.clinicalcontrol.model.Identifiable;
import wise.it.clinicalcontrol.util.Utils;

public interface HealthMeasure<V> extends Identifiable, Serializable {

    public enum Type {
        BLOOD_PRESSURE(Integer.class, InputType.TYPE_CLASS_NUMBER),
        RESPIRATORY_RATE(Integer.class, InputType.TYPE_CLASS_NUMBER),
        BLOOD_OXYGEN_LEVEL(Double.class, InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL),
        HEART_BEAT_RATE(Integer.class, InputType.TYPE_CLASS_NUMBER);

        private final Class<?> valueClass;
        private final int valueInputType;

        Type(final Class<?> valueClass, final int valueInputType) {
            this.valueClass = valueClass;
            this.valueInputType = valueInputType;
        }

        public Class<?> getValueClass() {
            return valueClass;
        }

        public int getValueInputType() {
            return valueInputType;
        }

        public Object getValueFromString(final String valueStr) {
            try {
                return valueClass.getConstructor(String.class).newInstance(valueStr);
            } catch (final NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        }

        public String getValueUnitString(final Resources resources, final String packageName) {
            return Utils.getBundle(resources, name().toLowerCase() + "_unit", packageName);
        }
    }

    /**
     * Id of the patient whom this measure is associated with.
     *
     * @return the id of the patient whom this measure is associated with
     */
    String getPatientId();

    /**
     * Id of the authenticable responsible for providing this measure.
     *
     * @return the id of the authenticable responsible for providing this measure
     */
    String getResponsibleAuthenticableId();

    /**
     * Id of the appointment in which this measure was taken, if was provided during an appointment.
     *
     * @return the id of the appointment in which this measure was taken, if was provided during an appointment. <code>null</code> in case of such measure was not provided during an appointment
     */
    String getAppointmentId();

    /**
     * The {@link DateTime} representing the moment when the measure was taken.
     *
     * @return the {@link DateTime} representing the moment when the measure was taken
     */
    DateTime getMeasurementDate();

    /**
     * The type of measure.
     *
     * @return the type of measure
     */
    Type getType();

    /**
     * Returns the value associated with this measure.
     * @return the value associated with this measure
     */
    V getValue();

    /**
     * Sets the value associated with this measure.
     * @param  value the value associated with this measure
     */
    void setValue(V value);

    /**
     * Returns the SHealth identifier associated with this measure if it exists. Otherwise, returns <code>null</code>
     * @return the SHealth identifier associated with this measure if it exists. Otherwise, returns <code>null</code>
     */
    String getSHealthId();

}

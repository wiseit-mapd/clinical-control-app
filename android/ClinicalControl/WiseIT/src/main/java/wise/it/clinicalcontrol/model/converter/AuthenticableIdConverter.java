package wise.it.clinicalcontrol.model.converter;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class AuthenticableIdConverter implements CustomConverter<String> {

    @Override
    public String convertToModel(final String property, final JSONObject jsonObject) throws JSONException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        return jsonObject.getString("authenticableId");
    }

    @Override
    public void convertToJSON(final Field field, final Object instance, final Map<String, Object> propertiesMap) {
        ReflectionUtils.makeAccessible(field);
        propertiesMap.put(field.getName(), ReflectionUtils.getField(field, instance));
    }
}

package wise.it.clinicalcontrol.model;

import java.io.Serializable;

import wise.it.clinicalcontrol.cache.Cacheable;

public interface Identifiable extends Cacheable, Serializable {

    String getId();
}

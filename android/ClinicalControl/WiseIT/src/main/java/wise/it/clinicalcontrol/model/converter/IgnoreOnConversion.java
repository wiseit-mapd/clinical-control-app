package wise.it.clinicalcontrol.model.converter;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;

/**
 * This annotation must to be used when a model entity does not want to convert all of the properties that can appear on related JSON object
 */
@Target({TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface IgnoreOnConversion {

    /**
     * The names of the properties to be ignored when converting JSON objects to this model class
     * @return
     */
    String[] value();
}

package wise.it.clinicalcontrol.doctor;

import android.support.v4.app.Fragment;

import wise.it.clinicalcontrol.SingleFragmentActivity;

/**
 * Created by rider on 12/14/2016.
 */

public class DoctorPatientActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return DoctorPatientFragment.newInstance();
    }
}

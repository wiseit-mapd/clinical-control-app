package wise.it.clinicalcontrol.network;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.util.Pair;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

public class RequestAsyncTask extends AsyncTask<Void, Void, RequestAsyncTask.Response> {

    public static class Builder {
        private RequestParams requestParams;
        private PreProcessor preProcessor;
        private PostProcessor postProcessor;

        public Builder setRequestParams(final RequestParams requestParams) {
            this.requestParams = requestParams;
            return this;
        }

        public Builder setPreProcessor(final PreProcessor preProcessor) {
            this.preProcessor = preProcessor;
            return this;
        }

        public Builder setPostProcessor(final PostProcessor postProcessor) {
            this.postProcessor = postProcessor;
            return this;
        }

        public RequestAsyncTask build() {
            if (requestParams == null) {
                throw new IllegalStateException("Request params must to be set.");
            }

            if (preProcessor == null) {
                preProcessor = PreProcessor.StandardPreProcessor.BY_PASS;
            }

            if (postProcessor == null) {
                postProcessor = PostProcessor.StandardPostProcessor.BY_PASS;
            }

            return new RequestAsyncTask(requestParams, preProcessor, postProcessor);
        }

        public RequestAsyncTask buildWithDialog(final Context context) {
            if (preProcessor == null) {
                preProcessor = PreProcessor.StandardPreProcessor.BY_PASS;
            }

            if (postProcessor == null) {
                postProcessor = PostProcessor.StandardPostProcessor.BY_PASS;
            }
            final DialogRequestProcessor dialogRequestProcessor = new DialogRequestProcessor(context, preProcessor, postProcessor);
            preProcessor = dialogRequestProcessor;
            postProcessor = dialogRequestProcessor;

            return build();
        }
    }

    public interface PreProcessor {
        void preProcess();

        public enum StandardPreProcessor implements PreProcessor {
            BY_PASS() {
                @Override
                public void preProcess() {
                }
            }
        }
    }

    public interface PostProcessor {
        void postProcess(final Response response);

        public enum StandardPostProcessor implements PostProcessor {
            BY_PASS() {
                @Override
                public void postProcess(final Response response) {
                }
            }
        }
    }

    public static class Response {
        private int code;
        private String message;
        private String result;

        public int getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String getResult() {
            return result;
        }
    }

    private final RequestParams requestParams;
    private final PreProcessor preProcessor;
    private final PostProcessor postProcessor;
    private Response response;

    private RequestAsyncTask(final RequestParams requestParams, final PreProcessor preProcessor, final PostProcessor postProcessor) {
        this.requestParams = requestParams;
        this.preProcessor = preProcessor;
        this.postProcessor = postProcessor;
    }

    @Override
    protected void onPreExecute() {
        preProcessor.preProcess();
    }

    @Override
    protected Response doInBackground(final Void... input) {
        try {
            final HttpURLConnection connection = (HttpURLConnection) requestParams.getUrl().openConnection();
            connection.setReadTimeout(15000);
            connection.setConnectTimeout(15000);
            connection.setRequestMethod(requestParams.getMethod().toString());
            connection.setDoInput(true);
            connection.setDoOutput(false);

            fillHeaderParams(connection);
            fillBodyParams(connection);


            final Response response = new Response();
            response.code = connection.getResponseCode();
            response.message = connection.getResponseMessage();

            if (response.code >= 200 && response.code < 300) {
                final BufferedReader in = new BufferedReader(
                        new InputStreamReader(
                                connection.getInputStream()));
                final StringBuffer sb = new StringBuffer("");
                String line = "";

                while ((line = in.readLine()) != null) {
                    sb.append(line);
                    break;
                }

                in.close();
                response.result = sb.toString();
            }
            return response;
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void fillHeaderParams(final HttpURLConnection connection) throws JSONException, UnsupportedEncodingException, IOException {
        final Set<Pair<String, String>> headerParams = requestParams.getHeaderParams();
        if (headerParams.size() > 0) {
            for (final Pair<String, String> pair : headerParams) {
                connection.setRequestProperty(pair.first, pair.second);
            }
        }
    }

    private void fillBodyParams(final HttpURLConnection connection) throws JSONException, UnsupportedEncodingException, IOException {
        final String dataString = requestParams.getDataString();
        if (dataString != null) {

            final OutputStream os = connection.getOutputStream();
            final BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(dataString);

            writer.flush();
            writer.close();
            os.close();
        }
    }

    @Override
    protected void onPostExecute(final Response response) {
        postProcessor.postProcess(response);
    }


}
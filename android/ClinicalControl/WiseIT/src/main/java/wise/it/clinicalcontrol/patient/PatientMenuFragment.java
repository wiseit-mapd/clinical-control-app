//WiseIT
package wise.it.clinicalcontrol.patient;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.Iterator;
import java.util.List;

import wise.it.clinicalcontrol.AppointmentCardAdapter;
import wise.it.clinicalcontrol.R;
import wise.it.clinicalcontrol.Token;
import wise.it.clinicalcontrol.cache.Cache;
import wise.it.clinicalcontrol.model.Authenticable;
import wise.it.clinicalcontrol.model.CommonAuthenticable;
import wise.it.clinicalcontrol.model.Doctor;
import wise.it.clinicalcontrol.model.appointment.Appointment;
import wise.it.clinicalcontrol.model.appointment.AppointmentImpl;
import wise.it.clinicalcontrol.network.HttpMethod;
import wise.it.clinicalcontrol.network.RequestAsyncTask;
import wise.it.clinicalcontrol.network.RequestParams;
import wise.it.clinicalcontrol.util.Utils;

/**
 * Created by rider on 30.10.2016.
 */

public class PatientMenuFragment extends Fragment {

    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private FragmentTransaction mFragmentTransaction;

    public static PatientMenuFragment newInstance() {
        PatientMenuFragment fragment = new PatientMenuFragment();
        return fragment;
    }

    public final RequestAsyncTask.PostProcessor retrieveDoctorsPostProcessor = new RequestAsyncTask.PostProcessor() {
        @Override
        public void postProcess(final RequestAsyncTask.Response response) {
            try {
                if (response.getResult() != null) {
                    System.out.println(response.getResult());
                    final JSONObject jsonObject = new JSONObject(response.getResult());
                    final JSONArray jsonDoctors = jsonObject.getJSONArray("doctors");
                    final JSONArray jsonAuthenticables = jsonObject.getJSONArray("authenticables");
                    //final JSONArray jsonAppointments = new JSONArray(response.getResult());

                    final List<Authenticable> authenticables = Utils.<Authenticable>convertToModel(jsonAuthenticables, CommonAuthenticable.class);
                    System.out.println(authenticables);

                    Cache.getInstance().put(Authenticable.class, authenticables);
                    final List<Doctor> doctors = Utils.<Doctor>convertToModel(jsonDoctors, Doctor.class);
                    Cache.getInstance().put(Doctor.class, doctors);
                }
            } catch (final JSONException e) {
                throw new RuntimeException(e);
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_patient_menu, container, false);
        roleInterfaceCreate(v, R.id.patient_drawer_layout, R.id.patient_navigation_view, new PatientTabFragment(), R.id.toolbar);


        return v;
    }

    private void roleInterfaceCreate(View v, int drawerId, int navViewId, Fragment classTabFragment, int toolbarId) {
        mDrawerLayout = (DrawerLayout) v.findViewById(drawerId);
        mNavigationView = (NavigationView) v.findViewById(navViewId);


        mFragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        mFragmentTransaction.replace(R.id.containerView, classTabFragment).commit();

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) v.findViewById(toolbarId);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(getActivity(),mDrawerLayout, toolbar,R.string.app_name,
                R.string.app_name);

        mDrawerLayout.addDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();
    }

    public void onResume() {
        super.onResume();
        try {
                final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.DOCTORS_RESOURCE)).setHttpMethod(HttpMethod.GET).buildWithAuthorization();
                final RequestAsyncTask requestAsyncTask = new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(retrieveDoctorsPostProcessor).buildWithDialog(getActivity());
                requestAsyncTask.execute();
        } catch (final MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }


}

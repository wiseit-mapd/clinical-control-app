//WiseIT
package wise.it.clinicalcontrol.doctor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.List;

import wise.it.clinicalcontrol.R;
import wise.it.clinicalcontrol.Token;
import wise.it.clinicalcontrol.cache.Cache;
import wise.it.clinicalcontrol.model.Authenticable;
import wise.it.clinicalcontrol.model.CommonAuthenticable;
import wise.it.clinicalcontrol.model.Patient;
import wise.it.clinicalcontrol.network.HttpMethod;
import wise.it.clinicalcontrol.network.RequestAsyncTask;
import wise.it.clinicalcontrol.network.RequestParams;
import wise.it.clinicalcontrol.util.Utils;

public class ListPatientFragment extends Fragment {

    public final RequestAsyncTask.PostProcessor retrievePatientsPostProcessor = new RequestAsyncTask.PostProcessor() {
        @Override
        public void postProcess(final RequestAsyncTask.Response response) {
            try {
                if (response.getResult() != null) {
                    final JSONObject jsonObject = new JSONObject(response.getResult());
                    final JSONArray patientsJson = jsonObject.getJSONArray("patients");
                    final JSONArray autheticablesJson = jsonObject.getJSONArray("authenticables");
                    final List<Authenticable> authenticables = Utils.<Authenticable>convertToModel(autheticablesJson, CommonAuthenticable.class);
                    Cache.getInstance().put(Authenticable.class, authenticables);

                    final List<Patient> patients = Utils.convertToModel(patientsJson, Patient.class);
                    Cache.getInstance().put(Patient.class, patients);

                    recyclerView.setAdapter(new PatientCardAdapter(patients));
                }
            } catch (final JSONException e) {
                throw new RuntimeException(e);
            }
        }
    };

    private NavigationView mNavigationView;
    private FragmentTransaction mFragmentTransaction;

    public static ListPatientFragment newInstance() {
        ListPatientFragment fragment = new ListPatientFragment();
        return fragment;
    }

    private RecyclerView recyclerView;
    private String patientId;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list_patient, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.list_patient_recycler_view);
        recyclerView.setHasFixedSize(true);

        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        ((FloatingActionButton)v.findViewById(R.id.list_patient_add)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Context context = getContext();
                final Intent intent = new Intent(context, EditPatientActivity.class);
                intent.putExtra(EditPatientFragment.DOCTOR_ID_KEY, Token.sToken.getId());
                context.startActivity(intent);
            }
        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            final RequestParams requestParams = new RequestParams.Builder().setResource(Utils.PATIENTS_RESOURCE).setHttpMethod(HttpMethod.GET).buildWithAuthorization();
            final RequestAsyncTask requestAsyncTask = new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(retrievePatientsPostProcessor).buildWithDialog(getActivity());
            requestAsyncTask.execute();
        } catch (final MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }


}

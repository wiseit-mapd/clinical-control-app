package wise.it.clinicalcontrol.model.measure;

import org.joda.time.DateTime;

import wise.it.clinicalcontrol.model.AbstractIdentifiable;
import wise.it.clinicalcontrol.model.converter.CustomConvertion;
import wise.it.clinicalcontrol.model.converter.MeasureValueCustomConverter;

public class HealthMeasureImpl<V> extends AbstractIdentifiable implements HealthMeasure<V> {

    private String sHealthId;
    private String patientId;
    private String responsibleAuthenticableId;
    private String appointmentId;
    private DateTime measurementDate;
    private Type type;

    @CustomConvertion(converter = MeasureValueCustomConverter.class)
    private V value;

    public HealthMeasureImpl() {
    }

    public HealthMeasureImpl(
            final String id,
            final String sHealthId,
            final String patientId,
            final String responsibleAuthenticableId,
            final String appointmentId,
            final DateTime measurementDate,
            final Type type,
            final V value) {
        super(id);
        this.sHealthId = sHealthId;
        this.patientId = patientId;
        this.responsibleAuthenticableId = responsibleAuthenticableId;
        this.appointmentId = appointmentId;
        this.measurementDate = measurementDate;
        this.type = type;
        this.value = value;
    }

    public HealthMeasureImpl(final String sHeathId,
                             final String patientId,
                             final String responsibleAuthenticableId,
                             final String appointmentId,
                             final DateTime measurementDate,
                             final Type type,
                             final V value) {
        this.sHealthId = sHeathId;
        this.patientId = patientId;
        this.responsibleAuthenticableId = responsibleAuthenticableId;
        this.appointmentId = appointmentId;
        this.measurementDate = measurementDate;
        this.type = type;
        this.value = value;
    }

    @Override
    public String getSHealthId() {
        return sHealthId;
    }

    @Override
    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    @Override
    public String getResponsibleAuthenticableId() {
        return responsibleAuthenticableId;
    }

    public void setResponsibleAuthenticableId(String responsibleAuthenticableId) {
        this.responsibleAuthenticableId = responsibleAuthenticableId;
    }

    @Override
    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    @Override
    public DateTime getMeasurementDate() {
        return measurementDate;
    }

    public void setMeasurementDate(DateTime measurementDate) {
        this.measurementDate = measurementDate;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }
}

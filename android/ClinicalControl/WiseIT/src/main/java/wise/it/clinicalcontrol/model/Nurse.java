package wise.it.clinicalcontrol.model;

import java.util.Set;

public final class Nurse extends AbstractIdentifiableAuthenticableWrapper {

    private String professionalIdentifier;

    public Nurse(final String id,
                 final String authenticableId,
                 final String username,
                 final String password,
                 final Set<Authenticable.Role> roles,
                 final String name,
                 final String email,
                 final String phone,
                 final String dateOfBirth,
                 final Authenticable.Gender gender,
                 final Object photo,
                 final String professionalIdentifier,
                 final String address) {
        super(id, authenticableId, username, password, roles, name, email, phone, dateOfBirth, gender, photo, address);
        this.professionalIdentifier = professionalIdentifier;
    }

    public Nurse(final String name,
                 final String email,
                 final String phone,
                 final String dateOfBirth,
                 final Authenticable.Gender gender,
                 final Object photo,
                 final String professionalIdentifier,
                 final String address) {
        authenticable.setName(name);
        authenticable.setEmail(email);
        authenticable.setPhone(phone);
        authenticable.setDateOfBirth(dateOfBirth);
        authenticable.setGender(gender);
        authenticable.setPhoto(photo);
        authenticable.setAddress(address);
        this.professionalIdentifier = professionalIdentifier;
    }

    public String getProfessionalIdentifier() {
        return professionalIdentifier;
    }

    public void setProfessionalIdentifier(final String professionalIdentifier) {
        this.professionalIdentifier = professionalIdentifier;
    }
}

package wise.it.clinicalcontrol;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import wise.it.clinicalcontrol.model.converter.ConvertCasting;
import wise.it.clinicalcontrol.model.measure.HealthMeasure;
import wise.it.clinicalcontrol.util.Utils;

public class TestCardAdapter extends RecyclerView.Adapter<TestCardAdapter.TestAdapterHolder> {
    private static final String measureValuePattern = "%s%s";

    public static class TestAdapterHolder extends RecyclerView.ViewHolder {
        private final TextView typeTextView;
        private final TextView valueTextView;

        public TestAdapterHolder(final View itemView) {
            super(itemView);
            typeTextView = (TextView) itemView.findViewById(R.id.text_test_type);
            valueTextView = (TextView) itemView.findViewById(R.id.text_test_value);
        }
    }

    private final List<? extends HealthMeasure> dataSet;
    private String packageName;

    public TestCardAdapter(final List<? extends HealthMeasure> dataset) {
        this.dataSet = dataset;
    }

    @Override
    public TestAdapterHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        this.packageName = parent.getContext().getPackageName();
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_card, parent, false);
        return new TestAdapterHolder(view);
    }

    @Override
    public void onBindViewHolder(final TestAdapterHolder holder, final int position) {
        final HealthMeasure<?> measure = dataSet.get(position);
        final Resources resources = holder.itemView.getResources();
        final String measureTypeBundle = Utils.getBundle(resources, measure.getType().name().toLowerCase() + "_text", packageName);
        holder.typeTextView.setText(measureTypeBundle);
        holder.valueTextView.setText(String.format(measureValuePattern, measure.getValue().toString(), measure.getType().getValueUnitString(resources, packageName)));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Context context = holder.itemView.getContext();
                final Intent intent = new Intent(context, EditTestActivity.class);
                intent.putExtra(EditTestFragment.MEASURE_PARAM_KEY, measure);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}

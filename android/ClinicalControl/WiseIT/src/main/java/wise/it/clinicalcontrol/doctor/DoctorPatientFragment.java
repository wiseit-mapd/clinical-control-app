package wise.it.clinicalcontrol.doctor;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import wise.it.clinicalcontrol.R;

/**
 * Created by rider on 12/14/2016.
 */

public class DoctorPatientFragment extends Fragment {

    public static final String PATIENT_PARAM_KEY = "patient";

    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private FragmentTransaction mFragmentTransaction;

    public static DoctorPatientFragment newInstance() {
        DoctorPatientFragment fragment = new DoctorPatientFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_doctor_menu, container, false);
        roleInterfaceCreate(v, R.id.doctor_drawer_layout, R.id.doctor_navigation_view, new DoctorPatientTabFragment(), R.id.toolbar);


        return v;
    }

    private void roleInterfaceCreate(View v, int drawerId, int navViewId, Fragment classTabFragment, int toolbarId) {
        mDrawerLayout = (DrawerLayout) v.findViewById(drawerId);
        mNavigationView = (NavigationView) v.findViewById(navViewId);


        mFragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        mFragmentTransaction.replace(R.id.containerView, classTabFragment).commit();

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) v.findViewById(toolbarId);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(getActivity(),mDrawerLayout, toolbar,R.string.app_name,
                R.string.app_name);

        mDrawerLayout.addDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();
    }
}

package wise.it.clinicalcontrol.doctor;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import wise.it.clinicalcontrol.DatePickerFragment;
import wise.it.clinicalcontrol.R;
import wise.it.clinicalcontrol.Token;
import wise.it.clinicalcontrol.cache.Cache;
import wise.it.clinicalcontrol.model.Authenticable;
import wise.it.clinicalcontrol.model.CommonAuthenticable;
import wise.it.clinicalcontrol.model.Patient;
import wise.it.clinicalcontrol.network.HttpMethod;
import wise.it.clinicalcontrol.network.RequestAsyncTask;
import wise.it.clinicalcontrol.network.RequestParams;
import wise.it.clinicalcontrol.util.Utils;

public class EditPatientFragment extends Fragment {

    private enum Mode {
        CREATE,
        EDIT;
    }

    public static final String PATIENT_PARAM_KEY = "patient";
    public static final String DOCTOR_ID_KEY = "doctorId";
    private static final String DIALOG_DATE = "DialogDate";
    private static final String measureValuePattern = "%s%s";
    private static final int REQUEST_DATE = 0;

    private final RequestAsyncTask.PostProcessor createPatientPostProcessor = new RequestAsyncTask.PostProcessor() {
        @Override
        public void postProcess(final RequestAsyncTask.Response response) {
            if (response.getResult() != null) {
                getActivity().finish();
            } else {
                Toast.makeText(getContext(), getString(R.string.failed_create_patient), Toast.LENGTH_LONG);
            }
        }
    };

    private final RequestAsyncTask.PostProcessor editPatientPostProcessor = new RequestAsyncTask.PostProcessor() {
        @Override
        public void postProcess(final RequestAsyncTask.Response response) {
            if (response.getResult() != null) {
                getView().findViewById(R.id.edit_patient_full_name_editable_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_patient_full_name_read_only_container).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.edit_patient_email_editable_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_patient_email_read_only_container).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.edit_patient_phone_editable_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_patient_phone_read_only_container).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.edit_patient_address_editable_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_patient_address_read_only_container).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.edit_patient_diagnosis_editable_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_patient_diagnosis_read_only_container).setVisibility(View.VISIBLE);

                try {
                    final JSONObject result = new JSONObject(response.getResult());
                    final Patient patient = Utils.convertToModel(result.getJSONObject("updatedPatient"), Patient.class);
                    final Authenticable authenticable = Utils.convertToModel(result.getJSONObject("updatedAuthenticable"), CommonAuthenticable.class);

                    final TextView nameValueView = ((TextView)getView().findViewById(R.id.edit_patient_full_name_view));
                    nameValueView.setText(authenticable.getName());

                    final TextView emailValueView = ((TextView)getView().findViewById(R.id.edit_patient_email_view));
                    emailValueView.setText(authenticable.getEmail());

                    final TextView phoneValueView = ((TextView)getView().findViewById(R.id.edit_patient_phone_view));
                    phoneValueView.setText(authenticable.getPhone());

                    final TextView addressValueView = ((TextView)getView().findViewById(R.id.edit_patient_address_view));
                    addressValueView.setText(authenticable.getAddress());

                    final TextView diagnosisValueView = ((TextView)getView().findViewById(R.id.edit_patient_diagnosis_view));
                    diagnosisValueView.setText(patient.getDiagnosis());

                } catch (final JSONException e) {
                    throw new RuntimeException(e);
                }
            } else {
                Toast.makeText(getContext(), getString(R.string.failed_edit_patient), Toast.LENGTH_LONG).show();
            }
        }
    };

    private final RequestAsyncTask.PostProcessor deletePatientPostProcessor = new RequestAsyncTask.PostProcessor() {
        @Override
        public void postProcess(RequestAsyncTask.Response response) {
            if (response.getResult() != null) {
                getActivity().finish();
            } else {
                Toast.makeText(getContext(), getString(R.string.failed_delete_patient), Toast.LENGTH_LONG);
            }
        }
    };

    private NavigationView mNavigationView;
    private FragmentTransaction mFragmentTransaction;

    private String doctorId;
    private Mode mode;
    Authenticable.Gender gender;
    private Patient patient;

    private Button mDateButton;

    public static EditPatientFragment newInstance() {
        EditPatientFragment fragment = new EditPatientFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_edit_patient, container, false);

        mDateButton = (Button)v.findViewById(R.id.edit_patient_dateofbirth);


        patient = (Patient) getActivity().getIntent().getSerializableExtra(PATIENT_PARAM_KEY);
        if (patient == null) {
            doctorId = getActivity().getIntent().getStringExtra(DOCTOR_ID_KEY);
            if (doctorId == null) {
                throw new IllegalStateException(String.format("%s must to be provided.", DOCTOR_ID_KEY));
            }
            mode = Mode.CREATE;
            configureCreateMode(v);
        } else {
            doctorId = patient.getId();
            mode = Mode.EDIT;
            configureEditMode(v, patient);
        }
        return v;
    }

    private void configureCreateMode(final View view) {
        view.findViewById(R.id.edit_patient_sex_text).setVisibility(View.GONE);
        view.findViewById(R.id.edit_patient_delete).setVisibility(View.GONE);

        view.findViewById(R.id.edit_patient_full_name_read_only_container).setVisibility(View.GONE);
        view.findViewById(R.id.edit_patient_full_name_apply_button).setVisibility(View.GONE);
        view.findViewById(R.id.edit_patient_full_name_undo_button).setVisibility(View.GONE);

        view.findViewById(R.id.edit_patient_email_read_only_container).setVisibility(View.GONE);
        view.findViewById(R.id.edit_patient_email_apply_button).setVisibility(View.GONE);
        view.findViewById(R.id.edit_patient_email_undo_button).setVisibility(View.GONE);

        view.findViewById(R.id.edit_patient_phone_read_only_container).setVisibility(View.GONE);
        view.findViewById(R.id.edit_patient_phone_apply_button).setVisibility(View.GONE);
        view.findViewById(R.id.edit_patient_phone_undo_button).setVisibility(View.GONE);

        view.findViewById(R.id.edit_patient_address_read_only_container).setVisibility(View.GONE);
        view.findViewById(R.id.edit_patient_address_apply_button).setVisibility(View.GONE);
        view.findViewById(R.id.edit_patient_address_undo_button).setVisibility(View.GONE);

        view.findViewById(R.id.edit_patient_diagnosis_read_only_container).setVisibility(View.GONE);
        view.findViewById(R.id.edit_patient_diagnosis_apply_button).setVisibility(View.GONE);
        view.findViewById(R.id.edit_patient_diagnosis_undo_button).setVisibility(View.GONE);


        final Spinner spinner = ((Spinner)view.findViewById(R.id.edit_patient_sex_spinner));
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                getContext(), R.array.genders, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        final String doctorId = Token.sToken.getId();

        view.findViewById(R.id.edit_patient_create).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                switch(spinner.getSelectedItemPosition()) {
                    case 0: {
                        gender = Authenticable.Gender.MALE;
                        break;
                    }
                    case 1: {
                        gender = Authenticable.Gender.FEMALE;
                        break;
                    }
                    default: {
                        gender = null;
                        break;
                    }
                }
                final String name = ((EditText)view.findViewById(R.id.edit_patient_full_name_editable)).getText().toString();
                final String address = ((EditText)view.findViewById(R.id.edit_patient_address_editable)).getText().toString();
                final String phone = ((EditText)view.findViewById(R.id.edit_patient_phone_editable)).getText().toString();
                final String email = ((EditText)view.findViewById(R.id.edit_patient_email_editable)).getText().toString();
                final String diagnosis = ((EditText)view.findViewById(R.id.edit_patient_diagnosis_editable)).getText().toString();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
                Date convertedDate = new Date();
                try {
                    convertedDate = dateFormat.parse(((Button)view.findViewById(R.id.edit_patient_dateofbirth)).getText().toString());
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dateFormat = new SimpleDateFormat("ddMMyyyy");
                final String dateOfBirth = dateFormat.format(convertedDate);

                if (doctorId == null) {
                    throw new IllegalStateException("Authentication required.");
                }
                final Authenticable.Gender sex = gender;
                final Patient patient = new Patient(name,email,phone,dateOfBirth,diagnosis,sex,null,address);
                final JSONObject jsonObject = Utils.convertToJSON(patient);
                System.out.println(jsonObject);
                try {
                    final RequestParams requestParams = new RequestParams.Builder().setResource(Utils.PATIENTS_RESOURCE).setHttpMethod(HttpMethod.POST).setJsonObject(jsonObject).buildWithAuthorization();
                    final RequestAsyncTask requestAsyncTask = new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(createPatientPostProcessor).buildWithDialog(getContext());
                    requestAsyncTask.execute();
                } catch (final MalformedURLException e) {
                    throw new RuntimeException(e);
                }
            }
        });


        mDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                DatePickerFragment dialog = DatePickerFragment.newInstance(null);
                dialog.setTargetFragment(EditPatientFragment.this, REQUEST_DATE);
                dialog.show(fm, DIALOG_DATE);
            }
        });
    }

    private void configureEditMode(final View view, final Patient patient) {
        view.findViewById(R.id.edit_patient_sex_spinner).setVisibility(View.GONE);
        view.findViewById(R.id.edit_patient_create).setVisibility(View.GONE);
        view.findViewById(R.id.edit_patient_full_name_editable_container).setVisibility(View.GONE);
        view.findViewById(R.id.edit_patient_email_editable_container).setVisibility(View.GONE);
        view.findViewById(R.id.edit_patient_phone_editable_container).setVisibility(View.GONE);
        view.findViewById(R.id.edit_patient_address_editable_container).setVisibility(View.GONE);
        view.findViewById(R.id.edit_patient_diagnosis_editable_container).setVisibility(View.GONE);

        view.findViewById(R.id.edit_patient_full_name_edit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.findViewById(R.id.edit_patient_full_name_read_only_container).setVisibility(View.GONE);
                view.findViewById(R.id.edit_patient_full_name_editable_container).setVisibility(View.VISIBLE);
                final EditText nameValueEditable = ((EditText)view.findViewById(R.id.edit_patient_full_name_editable));
                nameValueEditable.setText(patient.getAuthenticable().getName());
            }
        });

        view.findViewById(R.id.edit_patient_full_name_apply_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TextView nameTextView = ((TextView)view.findViewById(R.id.edit_patient_full_name_editable));
                patient.getAuthenticable().setName(nameTextView.getText().toString());
                InputMethodManager inputMethodManager =
                        (InputMethodManager) getActivity().getSystemService(
                                Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                new AlertDialog.Builder(getContext())
                        .setMessage(R.string.message_save)
                        .setPositiveButton(android.R.string.yes,  new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    final JSONObject jsonObject = Utils.convertToJSON(patient);
                                    final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.PATIENTS_BY_ID_RESOURCE_PATTERN, patient.getId())).setJsonObject(jsonObject).setHttpMethod(HttpMethod.PUT).buildWithAuthorization();
                                    new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(editPatientPostProcessor).buildWithDialog(getContext()).execute();
                                } catch (MalformedURLException e) {
                                    throw new RuntimeException(e);
                                }

                            }
                        })
                        .setNegativeButton(android.R.string.no,  new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        }).show();
            }
        });

        view.findViewById(R.id.edit_patient_full_name_undo_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputMethodManager =
                        (InputMethodManager) getActivity().getSystemService(
                                Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                getView().findViewById(R.id.edit_patient_full_name_editable_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_patient_full_name_read_only_container).setVisibility(View.VISIBLE);
            }
        });

        view.findViewById(R.id.edit_patient_email_edit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.findViewById(R.id.edit_patient_email_read_only_container).setVisibility(View.GONE);
                view.findViewById(R.id.edit_patient_email_editable_container).setVisibility(View.VISIBLE);
                final EditText emailValueEditable = ((EditText)view.findViewById(R.id.edit_patient_email_editable));
                emailValueEditable.setText(patient.getAuthenticable().getEmail());
            }
        });

        view.findViewById(R.id.edit_patient_email_apply_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TextView emailTextView = ((TextView)view.findViewById(R.id.edit_patient_email_editable));
                patient.getAuthenticable().setEmail(emailTextView.getText().toString());
                InputMethodManager inputMethodManager =
                        (InputMethodManager) getActivity().getSystemService(
                                Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                new AlertDialog.Builder(getContext())
                        .setMessage(R.string.message_save)
                        .setPositiveButton(android.R.string.yes,  new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    final JSONObject jsonObject = Utils.convertToJSON(patient);
                                    final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.PATIENTS_BY_ID_RESOURCE_PATTERN, patient.getId())).setJsonObject(jsonObject).setHttpMethod(HttpMethod.PUT).buildWithAuthorization();
                                    new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(editPatientPostProcessor).buildWithDialog(getContext()).execute();
                                } catch (MalformedURLException e) {
                                    throw new RuntimeException(e);
                                }

                            }
                        })
                        .setNegativeButton(android.R.string.no,  new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        }).show();
            }
        });

        view.findViewById(R.id.edit_patient_email_undo_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputMethodManager =
                        (InputMethodManager) getActivity().getSystemService(
                                Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                getView().findViewById(R.id.edit_patient_email_editable_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_patient_email_read_only_container).setVisibility(View.VISIBLE);
            }
        });

        view.findViewById(R.id.edit_patient_phone_edit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.findViewById(R.id.edit_patient_phone_read_only_container).setVisibility(View.GONE);
                view.findViewById(R.id.edit_patient_phone_editable_container).setVisibility(View.VISIBLE);
                final EditText phoneValueEditable = ((EditText)view.findViewById(R.id.edit_patient_phone_editable));
                phoneValueEditable.setText(patient.getAuthenticable().getPhone());
            }
        });

        view.findViewById(R.id.edit_patient_phone_apply_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TextView phoneTextView = ((TextView)view.findViewById(R.id.edit_patient_phone_editable));
                patient.getAuthenticable().setPhone(phoneTextView.getText().toString());
                InputMethodManager inputMethodManager =
                        (InputMethodManager) getActivity().getSystemService(
                                Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                new AlertDialog.Builder(getContext())
                        .setMessage(R.string.message_save)
                        .setPositiveButton(android.R.string.yes,  new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    final JSONObject jsonObject = Utils.convertToJSON(patient);
                                    final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.PATIENTS_BY_ID_RESOURCE_PATTERN, patient.getId())).setJsonObject(jsonObject).setHttpMethod(HttpMethod.PUT).buildWithAuthorization();
                                    new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(editPatientPostProcessor).buildWithDialog(getContext()).execute();
                                } catch (MalformedURLException e) {
                                    throw new RuntimeException(e);
                                }

                            }
                        })
                        .setNegativeButton(android.R.string.no,  new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        }).show();
            }
        });

        view.findViewById(R.id.edit_patient_phone_undo_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputMethodManager =
                        (InputMethodManager) getActivity().getSystemService(
                                Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                getView().findViewById(R.id.edit_patient_phone_editable_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_patient_phone_read_only_container).setVisibility(View.VISIBLE);
            }
        });

        view.findViewById(R.id.edit_patient_address_edit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.findViewById(R.id.edit_patient_address_read_only_container).setVisibility(View.GONE);
                view.findViewById(R.id.edit_patient_address_editable_container).setVisibility(View.VISIBLE);
                final EditText addressValueEditable = ((EditText)view.findViewById(R.id.edit_patient_address_editable));
                addressValueEditable.setText(patient.getAuthenticable().getAddress());
            }
        });

        view.findViewById(R.id.edit_patient_address_apply_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TextView addressTextView = ((TextView)view.findViewById(R.id.edit_patient_address_editable));
                patient.getAuthenticable().setAddress(addressTextView.getText().toString());
                InputMethodManager inputMethodManager =
                        (InputMethodManager) getActivity().getSystemService(
                                Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                new AlertDialog.Builder(getContext())
                        .setMessage(R.string.message_save)
                        .setPositiveButton(android.R.string.yes,  new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    final JSONObject jsonObject = Utils.convertToJSON(patient);
                                    final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.PATIENTS_BY_ID_RESOURCE_PATTERN, patient.getId())).setJsonObject(jsonObject).setHttpMethod(HttpMethod.PUT).buildWithAuthorization();
                                    new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(editPatientPostProcessor).buildWithDialog(getContext()).execute();
                                } catch (MalformedURLException e) {
                                    throw new RuntimeException(e);
                                }

                            }
                        })
                        .setNegativeButton(android.R.string.no,  new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        }).show();
            }
        });

        view.findViewById(R.id.edit_patient_address_undo_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputMethodManager =
                        (InputMethodManager) getActivity().getSystemService(
                                Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                getView().findViewById(R.id.edit_patient_address_editable_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_patient_address_read_only_container).setVisibility(View.VISIBLE);
            }
        });

        view.findViewById(R.id.edit_patient_diagnosis_edit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.findViewById(R.id.edit_patient_diagnosis_read_only_container).setVisibility(View.GONE);
                view.findViewById(R.id.edit_patient_diagnosis_editable_container).setVisibility(View.VISIBLE);
                final EditText diagnosisValueEditable = ((EditText)view.findViewById(R.id.edit_patient_diagnosis_editable));
                diagnosisValueEditable.setText(patient.getDiagnosis());
            }
        });

        view.findViewById(R.id.edit_patient_diagnosis_apply_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TextView diagnosisTextView = ((TextView)view.findViewById(R.id.edit_patient_diagnosis_editable));
                patient.setDiagnosis(diagnosisTextView.getText().toString());
                InputMethodManager inputMethodManager =
                        (InputMethodManager) getActivity().getSystemService(
                                Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                new AlertDialog.Builder(getContext())
                        .setMessage(R.string.message_save)
                        .setPositiveButton(android.R.string.yes,  new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    final JSONObject jsonObject = Utils.convertToJSON(patient);
                                    final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.PATIENTS_BY_ID_RESOURCE_PATTERN, patient.getId())).setJsonObject(jsonObject).setHttpMethod(HttpMethod.PUT).buildWithAuthorization();
                                    new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(editPatientPostProcessor).buildWithDialog(getContext()).execute();
                                } catch (MalformedURLException e) {
                                    throw new RuntimeException(e);
                                }

                            }
                        })
                        .setNegativeButton(android.R.string.no,  new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        }).show();
            }
        });

        view.findViewById(R.id.edit_patient_diagnosis_undo_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputMethodManager =
                        (InputMethodManager) getActivity().getSystemService(
                                Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                getView().findViewById(R.id.edit_patient_diagnosis_editable_container).setVisibility(View.GONE);
                getView().findViewById(R.id.edit_patient_diagnosis_read_only_container).setVisibility(View.VISIBLE);
            }
        });

        view.findViewById(R.id.edit_patient_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setMessage(R.string.message_delete)
                        .setPositiveButton(android.R.string.yes,  new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.PATIENTS_BY_ID_RESOURCE_PATTERN, patient.getId())).setHttpMethod(HttpMethod.DELETE).buildWithAuthorization();
                                    new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(deletePatientPostProcessor).buildWithDialog(getContext()).execute();
                                } catch (MalformedURLException e) {
                                    throw new RuntimeException(e);
                                }

                            }
                        })
                        .setNegativeButton(android.R.string.no,  new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        }).show();
            }
        });



        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        Date currentDateOfBirth = new Date();
        try {
            currentDateOfBirth = dateFormat.parse(patient.getAuthenticable().getDateOfBirth());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        dateFormat = new SimpleDateFormat("dd MMM yyyy");
        mDateButton.setText(dateFormat.format(currentDateOfBirth));

        mDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                DatePickerFragment dialog = DatePickerFragment.newInstance(null);
                dialog.setTargetFragment(EditPatientFragment.this, REQUEST_DATE);
                dialog.show(fm, DIALOG_DATE);
            }
        });

        ((TextView)view.findViewById(R.id.edit_patient_full_name_view)).setText(patient.getAuthenticable().getName());
        ((TextView)view.findViewById(R.id.edit_patient_sex_text)).setText(patient.getAuthenticable().getGender().toString());
        ((TextView)view.findViewById(R.id.edit_patient_email_view)).setText(patient.getAuthenticable().getEmail());
        ((TextView)view.findViewById(R.id.edit_patient_phone_view)).setText(patient.getAuthenticable().getPhone());
        ((TextView)view.findViewById(R.id.edit_patient_address_view)).setText(patient.getAuthenticable().getAddress());
        ((TextView)view.findViewById(R.id.edit_patient_diagnosis_view)).setText(patient.getDiagnosis());

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_DATE) {
            Date date = (Date) data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
            mDateButton.setText(dateFormat.format(date));

        }
    }
}

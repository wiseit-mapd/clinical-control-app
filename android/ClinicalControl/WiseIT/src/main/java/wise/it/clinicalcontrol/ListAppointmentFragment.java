package wise.it.clinicalcontrol;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.Iterator;
import java.util.List;

import wise.it.clinicalcontrol.R;
import wise.it.clinicalcontrol.authentication.AuthenticationHolder;
import wise.it.clinicalcontrol.cache.Cache;
import wise.it.clinicalcontrol.model.Authenticable;
import wise.it.clinicalcontrol.model.CommonAuthenticable;
import wise.it.clinicalcontrol.model.appointment.Appointment;
import wise.it.clinicalcontrol.model.appointment.AppointmentImpl;
import wise.it.clinicalcontrol.network.HttpMethod;
import wise.it.clinicalcontrol.network.RequestAsyncTask;
import wise.it.clinicalcontrol.network.RequestParams;
import wise.it.clinicalcontrol.util.Utils;

/**
 * Created by rider on 12/14/2016.
 */

public class ListAppointmentFragment extends Fragment {

    public static final String PATIENT_ID_PARAM = "patientId";

    public final RequestAsyncTask.PostProcessor retrieveAppointmentsPostProcessor = new RequestAsyncTask.PostProcessor() {
        @Override
        public void postProcess(final RequestAsyncTask.Response response) {
            try {
                if (response.getResult() != null) {
                    //final JSONObject jsonObject = new JSONObject(response.getResult());
                    //final JSONArray jsonMeasures = jsonObject.getJSONArray("measures");
                    //final JSONArray jsonResponsibles = jsonObject.getJSONArray("responsibles");
                    final JSONArray jsonAppointments = new JSONArray(response.getResult());

                    final List<? extends Appointment> appointments = Utils.convertToModel(jsonAppointments, AppointmentImpl.class);
                    Iterator<? extends Appointment> iterator = appointments.iterator();
                    if (Token.sToken.getRole() == Authenticable.Role.DOCTOR || Token.sToken.getRole() == Authenticable.Role.NURSE){
                        while (iterator.hasNext()) {
                            Appointment appointment = iterator.next();
                            System.out.println(appointment.getDoctorId());
                            System.out.println(Token.sToken.getId());
                            if (!appointment.getDoctorId().equals(Token.sToken.getId())) {
                                iterator.remove();
                            }

                        }
                    }
                    //final List<Authenticable> authenticables = Utils.<Authenticable>convertToModel(jsonResponsibles, CommonAuthenticable.class);
                    //Cache.getInstance().put(Authenticable.class, authenticables);
                    recyclerView.setAdapter(new AppointmentCardAdapter(appointments));
                }
            } catch (final JSONException e) {
                throw new RuntimeException(e);
            }
        }
    };

    private NavigationView mNavigationView;
    private FragmentTransaction mFragmentTransaction;

    public static ListAppointmentFragment newInstance() {
        ListAppointmentFragment fragment = new ListAppointmentFragment();
        return fragment;
    }

    private RecyclerView recyclerView;
    private String patientId;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list_appointment, container, false);

        if(Token.sToken.getRole() == Authenticable.Role.DOCTOR || Token.sToken.getRole() == Authenticable.Role.NURSE) {
            patientId = getActivity().getIntent().getExtras().getString(PATIENT_ID_PARAM);
            System.out.println(AuthenticationHolder.getUser().getId());
        }
        if (Token.sToken.getRole() == Authenticable.Role.PATIENT) {
            patientId = AuthenticationHolder.getUser().getId();
        }
        if (patientId == null) {
            throw  new IllegalStateException("Patient id or doctor id must to be provided.");
        }

        recyclerView = (RecyclerView) v.findViewById(R.id.list_appointment_recycler_view);
        recyclerView.setHasFixedSize(true);

        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        ((FloatingActionButton)v.findViewById(R.id.list_appointment_add)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Context context = getContext();
                final Intent intent = new Intent(context, EditAppointmentActivity.class);
                if (patientId != null) {
                    intent.putExtra(EditAppointmentFragment.PATIENT_ID_KEY, patientId);
                }
                context.startActivity(intent);
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (patientId != null) {
                final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.APPOINTMENTS_BY_PATIENT_ID_RESOURSE_PATTERN, patientId)).setHttpMethod(HttpMethod.GET).buildWithAuthorization();
                final RequestAsyncTask requestAsyncTask = new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(retrieveAppointmentsPostProcessor).buildWithDialog(getActivity());
                requestAsyncTask.execute();
            }
        } catch (final MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        System.out.println("ATTACH CONTEXT");
    }
}

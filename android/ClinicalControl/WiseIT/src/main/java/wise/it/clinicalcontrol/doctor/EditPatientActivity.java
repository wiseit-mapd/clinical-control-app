package wise.it.clinicalcontrol.doctor;

import android.support.v4.app.Fragment;

import wise.it.clinicalcontrol.SingleFragmentActivity;

/**
 * Created by rider on 11/25/2016.
 */

public class EditPatientActivity extends SingleFragmentActivity {
    @Override
    public Fragment createFragment() {
        return EditPatientFragment.newInstance();
    }
}

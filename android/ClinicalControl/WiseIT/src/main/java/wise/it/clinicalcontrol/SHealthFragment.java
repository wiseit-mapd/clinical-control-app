//WiseIT
package wise.it.clinicalcontrol;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.samsung.android.sdk.healthdata.HealthConnectionErrorResult;
import com.samsung.android.sdk.healthdata.HealthConstants;
import com.samsung.android.sdk.healthdata.HealthDataResolver;
import com.samsung.android.sdk.healthdata.HealthDataService;
import com.samsung.android.sdk.healthdata.HealthDataStore;
import com.samsung.android.sdk.healthdata.HealthPermissionManager;
import com.samsung.android.sdk.healthdata.HealthResultHolder;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import wise.it.clinicalcontrol.cache.Cache;
import wise.it.clinicalcontrol.model.Authenticable;
import wise.it.clinicalcontrol.model.CommonAuthenticable;
import wise.it.clinicalcontrol.model.measure.HealthMeasure;
import wise.it.clinicalcontrol.model.measure.HealthMeasureImpl;
import wise.it.clinicalcontrol.model.shealth.SHealthItem;
import wise.it.clinicalcontrol.network.HttpMethod;
import wise.it.clinicalcontrol.network.RequestAsyncTask;
import wise.it.clinicalcontrol.network.RequestParams;
import wise.it.clinicalcontrol.util.Utils;

public class SHealthFragment extends Fragment {

    public static final String APP_TAG = "ClinicalControl";
    private HealthDataStore mStore;
    private HealthConnectionErrorResult mConnError;
    private Set<HealthPermissionManager.PermissionKey> mKeySet;
    private FloatingActionButton synchButton;
    private Set<String> sHealthMeasures;

    public static final String PATIENT_ID_PARAM = "patientId";

    public final RequestAsyncTask.PostProcessor retrieveMeasuresPostProcessor = new RequestAsyncTask.PostProcessor() {
        @Override
        public void postProcess(final RequestAsyncTask.Response response) {
            try {
                if (response.getResult() != null) {
                    final JSONObject jsonObject = new JSONObject(response.getResult());
                    final JSONArray jsonMeasures = jsonObject.getJSONArray("measures");
                    final JSONArray jsonResponsibles = jsonObject.getJSONArray("responsibles");

                    final List<? extends HealthMeasure> measures = Utils.convertToModel(jsonMeasures, HealthMeasureImpl.class);
                    sHealthMeasures = new HashSet<>();
                    for (final HealthMeasure measure : measures) {
                        if (measure.getSHealthId() != null) {
                            sHealthMeasures.add(measure.getSHealthId());
                        }
                    }
                    final List<Authenticable> authenticables = Utils.<Authenticable>convertToModel(jsonResponsibles, CommonAuthenticable.class);
                    Cache.getInstance().put(Authenticable.class, authenticables);
                }
            } catch (final JSONException e) {
                throw new RuntimeException(e);
            }
        }
    };

    private final HealthDataStore.ConnectionListener mConnectionListener = new HealthDataStore.ConnectionListener() {
        @Override
        public void onConnected() {
            Log.d(APP_TAG, "Health data service is connected.");
            HealthPermissionManager pmsManager = new HealthPermissionManager(mStore);
            try {
                // Check whether the permissions that this application needs are acquired
                // Request the permission for reading step counts if it is not acquired
                // Get the current step count and display it if data permission is required
                // ...
                // Check whether the permissions that this application needs are acquired
                Map<HealthPermissionManager.PermissionKey, Boolean> resultMap = pmsManager.isPermissionAcquired(mKeySet);
                if (resultMap.containsValue(Boolean.FALSE)) {
                    // Request the permission for reading step counts if it is not acquired
                    pmsManager.requestPermissions(mKeySet, getActivity()).setResultListener(mPermissionListener);
                } else {
                    HealthDataResolver resolver = new HealthDataResolver(mStore, null);
                    HealthDataResolver.ReadRequest request = new HealthDataResolver.ReadRequest.Builder()
                            .setDataType(HealthConstants.HeartRate.HEALTH_DATA_TYPE)
                            .build();

                    try {
                        resolver.read(request).setResultListener(mRdResult);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("TEST", "Aggregating health data fails.");
                    }
                }
            } catch (Exception e) {
                Log.e(APP_TAG, e.getClass().getName() + " - " + e.getMessage());
                Log.e(APP_TAG, "Permission setting fails.");
            }
        }

        @Override
        public void onConnectionFailed(HealthConnectionErrorResult error) {
            Log.d(APP_TAG, "Health data service is not available.");
            showConnectionFailureDialog(error);
        }

        @Override
        public void onDisconnected() {
            Log.d(APP_TAG, "Health data service is disconnected.");
        }
    };

    private final HealthResultHolder.ResultListener<HealthDataResolver.ReadResult> mRdResult = new HealthResultHolder.ResultListener<HealthDataResolver.ReadResult>() {
        @Override
        public void onResult(HealthDataResolver.ReadResult result) {
            System.out.println("Result: " + result);
            Cursor c = result.getResultCursor();
            try {
                if (result != null) {
                    final List<SHealthItem> dataList = new ArrayList<>();

                    while (c.moveToNext()) {
                        final String id = c.getString(c.getColumnIndex(HealthConstants.Common.UUID));
                        final float data = c.getInt(c.getColumnIndex(HealthConstants.HeartRate.HEART_BEAT_COUNT));
                        final long milli = c.getLong(c.getColumnIndex(HealthConstants.Common.CREATE_TIME));
                        final DateTime date = new DateTime(milli);
                        dataList.add(new SHealthItem(id, date, HealthConstants.HeartRate.HEART_BEAT_COUNT, data));
                    }
                    c.close();

                    recyclerView.setAdapter(new SHealthCardAdapter(dataList));

                    synchButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            while (sHealthMeasures == null) {
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                            for (final SHealthItem item : dataList) {
                                if (!sHealthMeasures.contains(item.getId())) {
                                    final String responsibleAuthenticableId = Token.sToken.getId();
                                    final HealthMeasure<?> measure = new HealthMeasureImpl(item.getId(), patientId, responsibleAuthenticableId, null, item.getDateTime(), HealthMeasure.Type.HEART_BEAT_RATE, (int)(float)item.getValue());
                                    final JSONObject jsonObject = Utils.convertToJSON(measure);
                                    try {
                                        final RequestParams requestParams = new RequestParams.Builder().setResource(Utils.MEASURES_RESOURCE).setHttpMethod(HttpMethod.POST).setJsonObject(jsonObject).buildWithAuthorization();
                                        final RequestAsyncTask requestAsyncTask = new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(new RequestAsyncTask.PostProcessor() {
                                            @Override
                                            public void postProcess(RequestAsyncTask.Response response) {
                                                if (response.getResult() == null) {
                                                    System.out.println(response.getCode());
                                                    System.out.println(response.getMessage());
                                                    Toast.makeText(getContext(), R.string.shealth_synch_fail, Toast.LENGTH_LONG).show();
                                                } else {
                                                    sHealthMeasures.add(item.getId());
                                                }
                                            }
                                        }).buildWithDialog(getContext());
                                        requestAsyncTask.execute();
                                    } catch (final MalformedURLException e) {
                                        throw new RuntimeException(e);
                                    }
                                }
                            }
                        }
                    });
                    synchButton.setEnabled(true);
                }
            } finally {
                if (c != null) {
                    c.close();
                }
            }
        }

    };

    private final HealthResultHolder.ResultListener<HealthPermissionManager.PermissionResult> mPermissionListener = new HealthResultHolder.ResultListener<HealthPermissionManager.PermissionResult>() {
        @Override
        public void onResult(HealthPermissionManager.PermissionResult result) {
            Log.d(APP_TAG, "Permission callback is received.");
            Map<HealthPermissionManager.PermissionKey, Boolean> resultMap = result.getResultMap();
            if (resultMap.containsValue(Boolean.FALSE)) {
                // Requesting permission fails
            } else {
                // Get the current step count and display it
            }
        }
    };

    private void showConnectionFailureDialog(HealthConnectionErrorResult error) {
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        mConnError = error;
        alert.setMessage(R.string.shealth_synch_fail);
        alert.setPositiveButton(R.string.message_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        alert.show();
    }

    private NavigationView mNavigationView;
    private FragmentTransaction mFragmentTransaction;

    public static SHealthFragment newInstance() {
        SHealthFragment fragment = new SHealthFragment();
        return fragment;
    }

    private RecyclerView recyclerView;
    private String patientId;
    private String doctorId;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_shealth, container, false);

        patientId = getActivity().getIntent().getStringExtra(PATIENT_ID_PARAM);
        if (patientId == null) {
            throw  new IllegalStateException("Patient id must to be provided.");
        }

        recyclerView = (RecyclerView) v.findViewById(R.id.list_shealth_recicler_view);
        recyclerView.setHasFixedSize(true);

        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);



        mKeySet = new HashSet<HealthPermissionManager.PermissionKey>();
        mKeySet.add(new HealthPermissionManager.PermissionKey(HealthConstants.HeartRate.HEALTH_DATA_TYPE, HealthPermissionManager.PermissionType.READ));
        mKeySet.add(new HealthPermissionManager.PermissionKey(HealthConstants.HeartRate.HEALTH_DATA_TYPE, HealthPermissionManager.PermissionType.WRITE));
        HealthDataService healthDataService = new HealthDataService();
        try {
            healthDataService.initialize(getContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Create a HealthDataStore instance and set its listener
        mStore = new HealthDataStore(getContext(), mConnectionListener);
        // Request the connection to the health data store
        mStore.connectService();

        synchButton = ((FloatingActionButton)v.findViewById(R.id.synchronize_button));
        synchButton.setEnabled(false);

        try {
            final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.MEASURE_BY_PATIENT_ID_RESOURCE_PATTERN, patientId)).setHttpMethod(HttpMethod.GET).buildWithAuthorization();
            final RequestAsyncTask requestAsyncTask = new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(retrieveMeasuresPostProcessor).buildWithDialog(getActivity());
            requestAsyncTask.execute();
        } catch (final MalformedURLException e) {
            throw new RuntimeException(e);
        }
        return v;
    }

    @Override
    public void onDestroy() {

        mStore.disconnectService();
        super.onDestroy();
    }
}

package wise.it.clinicalcontrol.util;

import android.content.res.Resources;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import wise.it.clinicalcontrol.model.Authenticable;
import wise.it.clinicalcontrol.model.Identifiable;
import wise.it.clinicalcontrol.model.converter.AuthenticableConverter;
import wise.it.clinicalcontrol.model.converter.ConvertCasting;
import wise.it.clinicalcontrol.model.converter.CustomConvertion;
import wise.it.clinicalcontrol.model.converter.IgnoreOnConversion;

public final class Utils {



    //Server URL
    public static final String BACKEND_SERVER_URL = "https://clinical-control.herokuapp.com";

    //Authentication resources
    public static final String TOKEN_RESOURCE = "/token";
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String AUTHORIZATION_VALUE_PATTERN = "Bearer %s";

    //Entities resources
    public static final String DOCTORS_RESOURCE = "/doctors";
    public static final String DOCTORS_BY_ID_RESOURCE_PATTERN = "/doctors/%s";
    public static final String DOCTORS_BY_AUTHENTICABLE_ID_RESOURCE_PATTERN = "/doctors/authenticableId/%s";
    public static final String PATIENTS_RESOURCE = "/patients";
    public static final String PATIENTS_BY_ID_RESOURCE_PATTERN = "/patients/%s";
    public static final String PATIENTS_BY_AUTHENTICABLE_ID_RESOURCE_PATTERN = "/patients/authenticableId/%s";
    public static final String NURSES_RESOURCE = "/nurses";
    public static final String NURSES_BY_ID_RESOURCE_PATTERN = "/nurses/%s";
    public static final String NURSES_BY_AUTHENTICABLE_ID_RESOURCE_PATTERN = "/nurses/authenticableId/%s";
    public static final String MEASURES_RESOURCE = "/measures";
    public static final String MEASURES_BY_ID_RESOURCE_PATTERN = "/measures/%s";
    public static final String MEASURE_BY_PATIENT_ID_RESOURCE_PATTERN = "/measures/patient/%s";
    public static final String APPOINTMENTS_BY_PATIENT_ID_RESOURSE_PATTERN = "/appointments/patient/%s";
    public static final String APPOINTMENTS_BY_DOCTOR_ID_RESOURSE_PATTERN = "/appointments/doctor/%s";



    //Other resources
    public static final String APPOINTMENTS_RESOURCE = "/appointments";
    public static final String APPOINTMENTS_BY_ID = "/appointments/%s";

    public static final DateTimeFormatter DATE_OF_BIRTH_FORMATTER = DateTimeFormat.forPattern("yyyyMMdd");
    public static final DateTimeFormatter DATE_AND_TIME_FORMATTER = DateTimeFormat.forPattern("dd/MM/yy HH:mm");

    private Utils() {
        throw new AssertionError();
    }


    public static <M> M convertToModel(final JSONObject jsonObject, final Class<? extends M> modelClass) {
        try {
            final M result = modelClass.newInstance();
            final Iterator<String> keys = jsonObject.keys();

            final Set<String> propertiesToIgnore = new HashSet<>();
            final IgnoreOnConversion ignoreOnConversion = modelClass.getAnnotation(IgnoreOnConversion.class);
            if (ignoreOnConversion != null) {
                propertiesToIgnore.addAll(Arrays.asList(ignoreOnConversion.value()));
            }

            final Map<String,Class<?>> convertingCastMap = new HashMap<>();
            final ConvertCasting convertCasting = modelClass.getAnnotation(ConvertCasting.class);
            if (convertCasting != null) {
                final String[] properties = convertCasting.properties();
                final Class<?>[] targetClasses = convertCasting.targetClasses();

                if (properties.length != properties.length) {
                    throw new IllegalStateException("Different properties and target classes lenght at @" + ConvertCasting.class + " in " + modelClass);
                }
                for (int k = 0; k < properties.length; k++) {
                    convertingCastMap.put(properties[k], targetClasses[k]);
                }
            }
            while (keys.hasNext()) {
                final String key = keys.next();
                if (propertiesToIgnore.contains(key)) {
                    continue;
                }
                final String fieldName = key.equals("_id") ? Authenticable.class.isAssignableFrom(modelClass) ? "authenticableId" : "id" : (key.equals("authenticableId") && !Authenticable.class.isAssignableFrom(modelClass) ? "authenticable" : key);
                final Field field = ReflectionUtils.findField(modelClass, fieldName);
                if (field == null) {
                    continue;
                }
                final Class<?> expectedType = field.getType();
                final Object value;

                final CustomConvertion customConvertion = field.getAnnotation(CustomConvertion.class);
                if (customConvertion != null) {
                    value = customConvertion.converter().newInstance().convertToModel(key, jsonObject);
                } else {
                    if (jsonObject.isNull(key)) {
                        value = null;
                    } else {
                        final Class<?> castTarget = convertingCastMap.get(key);
                        if (castTarget != null) {
                            value = castTarget.cast(jsonObject.get(key));
                        } else if (Enum.class.isAssignableFrom(expectedType)) {
                            value = Enum.valueOf((Class<? extends Enum>)expectedType, jsonObject.getString(key));
                        } else if (expectedType.isAssignableFrom(String.class)) {
                            value = jsonObject.getString(key);
                        } else if (expectedType.isAssignableFrom(Double.class)) {
                            value = new Double(jsonObject.getString(key));;
                        } else if (expectedType.isAssignableFrom(Integer.class)) {
                            value = new Integer(jsonObject.getString(key));
                        } else if (expectedType.isAssignableFrom(Long.class)) {
                            value = new Long(jsonObject.getString(key));
                        } else if (expectedType.isAssignableFrom(DateTime.class)) {
                            value = new DateTime(new Long(jsonObject.getString(key)));
                        } else {
                            throw new RuntimeException("Unknown type: " + expectedType);
                        }
                    }
                }

                ReflectionUtils.makeAccessible(field);
                ReflectionUtils.setField(field, result, value);
            }
            return result;
        } catch (final IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (final JSONException e) {
            throw new RuntimeException(e);
        }

    }

    public static <M> List<M> convertToModel(final JSONArray jsonArray, final Class<? extends M> modelClass) {
        final List<M> result = new ArrayList<>(jsonArray.length());
        try {
            for (int k = 0; k < jsonArray.length(); k++) {
                final JSONObject jsonObject = jsonArray.getJSONObject(k);
                result.add(Utils.<M>convertToModel(jsonObject, modelClass));
            }
            return result;
        } catch (final JSONException e) {
            throw new RuntimeException(e);
        }

    }

    public static void populatePropertiesMap(final Map<String,Object> propertiesMap, final Class<?> clazz, final Object target) {
        final Field[] fields = clazz.getDeclaredFields();

        final Set<String> propertiesToIgnore = new HashSet<>();
        final IgnoreOnConversion ignoreOnConversion = clazz.getAnnotation(IgnoreOnConversion.class);
        if (ignoreOnConversion != null) {
            propertiesToIgnore.addAll(Arrays.asList(ignoreOnConversion.value()));
        }

        final Map<String,Class<?>> customConvertersMap = new HashMap<>();
        final ConvertCasting convertCasting = clazz.getAnnotation(ConvertCasting.class);
        if (convertCasting != null) {
            final String[] properties = convertCasting.properties();
            final Class<?>[] targetClasses = convertCasting.targetClasses();

            if (properties.length != properties.length) {
                throw new IllegalStateException("Different properties and target classes lenght at @" + ConvertCasting.class + " in " + clazz);
            }
            for (int k = 0; k < properties.length; k++) {
                customConvertersMap.put(properties[k], targetClasses[k]);
            }
        }

        try {
            for (final Field field : fields) {
                final String name = field.getName().equals("id") ? "_id" : field.getName();
                if (propertiesToIgnore.contains(name)) {
                    continue;
                }
                final CustomConvertion customConvertion = field.getAnnotation(CustomConvertion.class);
                if (customConvertion != null) {
                    customConvertion.converter().newInstance().convertToJSON(field, target, propertiesMap);
                    continue;
                }

                ReflectionUtils.makeAccessible(field);
                Object value = ReflectionUtils.getField(field, target);
                if (value instanceof Enum) {
                    value = ((Enum) value).name();
                } else if (value instanceof DateTime) {
                    value = ((DateTime) value).getMillis();
                }
                propertiesMap.put(name, value);
            }
        } catch (final IllegalAccessException | InstantiationException e) {
            throw new RuntimeException(e);
        }

    }

    public static JSONObject convertToJSON(final Identifiable identifiable) {
        Class<?> clazz = identifiable.getClass();
        final Map<String,Object> propertiesMap = new HashMap<>();
        boolean processNext = true;
        while (processNext) {
            populatePropertiesMap(propertiesMap, clazz, identifiable);
            clazz = clazz.getSuperclass();

            //Do not process class Object
            processNext = clazz.getSuperclass() != null;
        }
        return new JSONObject(propertiesMap);
    }

    public static JSONArray convertToJSON(final List<? extends Identifiable> identifiables) {
        final JSONArray result = new JSONArray();
        for (final Identifiable identifiable : identifiables) {
            final JSONObject jsonObject = convertToJSON(identifiable);
            result.put(jsonObject);
        }
        return result;
    }

    public static String getBundle(final Resources resources, final String key, final String packageName) {
        final int stringIdentifier = resources.getIdentifier(key, "string", packageName);
        return resources.getString(stringIdentifier);
    }



}

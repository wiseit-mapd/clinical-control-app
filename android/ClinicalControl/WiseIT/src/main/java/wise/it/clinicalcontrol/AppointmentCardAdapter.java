package wise.it.clinicalcontrol;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import wise.it.clinicalcontrol.cache.Cache;
import wise.it.clinicalcontrol.model.Authenticable;
import wise.it.clinicalcontrol.model.Doctor;
import wise.it.clinicalcontrol.model.User;
import wise.it.clinicalcontrol.model.appointment.Appointment;
import wise.it.clinicalcontrol.util.Utils;

/**
 * Created by rider on 12/15/2016.
 */

public class AppointmentCardAdapter extends RecyclerView.Adapter<AppointmentCardAdapter.AppointmentAdapterHolder> {

    public static class AppointmentAdapterHolder extends RecyclerView.ViewHolder {
        private final TextView appointmentDateTextView;
        private final TextView appointmentStateTextView;
        private final TextView appointmentDoctorTextView;

        public AppointmentAdapterHolder(final View itemView) {
            super(itemView);
            appointmentDateTextView = (TextView) itemView.findViewById(R.id.text_appointment_date);
            appointmentStateTextView = (TextView) itemView.findViewById(R.id.text_appointment_state);
            appointmentDoctorTextView = (TextView) itemView.findViewById(R.id.text_appointment_doctor);
        }
    }

    private final List<? extends Appointment> dataSet;
    private String packageName;

    public AppointmentCardAdapter(final List<? extends Appointment> dataset) {
        this.dataSet = dataset;
    }

    @Override
    public AppointmentAdapterHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        this.packageName = parent.getContext().getPackageName();
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.appointment_card, parent, false);
        return new AppointmentAdapterHolder(view);
    }

    @Override
    public void onBindViewHolder(final AppointmentAdapterHolder holder, final int position) {
        final Appointment appointment = dataSet.get(position);
        final Resources resources = holder.itemView.getResources();
        final String appointmentStateBundle = Utils.getBundle(resources, appointment.getState().name().toLowerCase() + "_text", packageName);
        holder.appointmentDateTextView.setText(appointment.getAppointmentDate().toString());
        holder.appointmentStateTextView.setText(appointmentStateBundle);
        if (Token.sToken.getRole() == Authenticable.Role.PATIENT) {
            holder.appointmentDoctorTextView.setText(Cache.getInstance().get(Authenticable.class,appointment.getDoctorId()).getName());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Context context = holder.itemView.getContext();
                final Intent intent = new Intent(context, EditAppointmentActivity.class);
                intent.putExtra(EditAppointmentFragment.APPOINTMENT_PARAM_KEY, appointment);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}

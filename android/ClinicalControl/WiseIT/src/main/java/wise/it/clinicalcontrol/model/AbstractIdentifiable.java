package wise.it.clinicalcontrol.model;

public abstract class AbstractIdentifiable implements Identifiable {

    private String id;

    public AbstractIdentifiable() {
    }

    public AbstractIdentifiable(final String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public final String generateKey() {
        return id;
    }
}

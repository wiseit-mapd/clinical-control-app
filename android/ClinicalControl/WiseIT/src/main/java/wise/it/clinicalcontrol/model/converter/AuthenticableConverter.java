package wise.it.clinicalcontrol.model.converter;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import wise.it.clinicalcontrol.cache.Cache;
import wise.it.clinicalcontrol.model.Authenticable;
import wise.it.clinicalcontrol.util.Utils;

public class AuthenticableConverter implements CustomConverter<Authenticable> {


    @Override
    public Authenticable convertToModel(final String property, final JSONObject jsonObject) throws JSONException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final String authenticableId = jsonObject.getString(property);
        final Authenticable authenticable = Cache.getInstance().get(Authenticable.class, authenticableId);
        if (authenticable == null) {
            throw new IllegalStateException("No authenticable found during conversion.");
        }
        return authenticable;
    }

    @Override
    public void convertToJSON(final Field field, final Object instance, final Map<String, Object> propertiesMap) {
        ReflectionUtils.makeAccessible(field);

        Utils.populatePropertiesMap(propertiesMap, field.getType(), ReflectionUtils.getField(field, instance));
    }
}

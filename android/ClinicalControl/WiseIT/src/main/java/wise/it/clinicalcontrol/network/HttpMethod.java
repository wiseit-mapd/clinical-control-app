package wise.it.clinicalcontrol.network;

public enum HttpMethod {
        GET,
        POST,
        PUT,
        DELETE,
        PATCH;
    }
//WiseIT
package wise.it.clinicalcontrol.patient;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import wise.it.clinicalcontrol.CustomCalendarView;
import wise.it.clinicalcontrol.R;

/**
 * Created by rider on 04.11.2016.
 */

public class PatientAppointmentsFragment extends Fragment {
    private FragmentTransaction mFragmentTransaction;

    Fragment calendar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_patient_appointments, container, false);
        mFragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        calendar = new CustomCalendarView();
        mFragmentTransaction.replace(R.id.calendar_view_container, calendar).commit();
        return v;
    }

}

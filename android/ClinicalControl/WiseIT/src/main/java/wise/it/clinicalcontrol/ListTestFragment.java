//WiseIT
package wise.it.clinicalcontrol;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.List;

import wise.it.clinicalcontrol.cache.Cache;
import wise.it.clinicalcontrol.model.Authenticable;
import wise.it.clinicalcontrol.model.CommonAuthenticable;
import wise.it.clinicalcontrol.model.measure.HealthMeasure;
import wise.it.clinicalcontrol.model.measure.HealthMeasureImpl;
import wise.it.clinicalcontrol.network.HttpMethod;
import wise.it.clinicalcontrol.network.RequestAsyncTask;
import wise.it.clinicalcontrol.network.RequestParams;
import wise.it.clinicalcontrol.util.Utils;

public class ListTestFragment extends Fragment {

    public static final String PATIENT_ID_PARAM = "patientId";

    public final RequestAsyncTask.PostProcessor retrieveMeasuresPostProcessor = new RequestAsyncTask.PostProcessor() {
        @Override
        public void postProcess(final RequestAsyncTask.Response response) {
            try {
                if (response.getResult() != null) {
                    final JSONObject jsonObject = new JSONObject(response.getResult());
                    final JSONArray jsonMeasures = jsonObject.getJSONArray("measures");
                    final JSONArray jsonResponsibles = jsonObject.getJSONArray("responsibles");

                    final List<? extends HealthMeasure> measures = Utils.convertToModel(jsonMeasures, HealthMeasureImpl.class);
                    final List<Authenticable> authenticables = Utils.<Authenticable>convertToModel(jsonResponsibles, CommonAuthenticable.class);
                    Cache.getInstance().put(Authenticable.class, authenticables);
                    recyclerView.setAdapter(new TestCardAdapter(measures));
                }
            } catch (final JSONException e) {
                throw new RuntimeException(e);
            }
        }
    };

    private NavigationView mNavigationView;
    private FragmentTransaction mFragmentTransaction;

    public static ListTestFragment newInstance() {
        ListTestFragment fragment = new ListTestFragment();
        return fragment;
    }

    private RecyclerView recyclerView;
    private String patientId;
    private String doctorId;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list_test, container, false);

        patientId = getActivity().getIntent().getExtras().getString(PATIENT_ID_PARAM);
        if (patientId == null) {
            throw  new IllegalStateException("Patient id must to be provided.");
        }

        recyclerView = (RecyclerView) v.findViewById(R.id.list_test_recicler_view);
        recyclerView.setHasFixedSize(true);

        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        ((FloatingActionButton)v.findViewById(R.id.list_test_add)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Context context = getContext();
                final Intent intent = new Intent(context, EditTestActivity.class);
                intent.putExtra(EditTestFragment.PATIEND_ID_KEY, patientId);
                context.startActivity(intent);
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            final RequestParams requestParams = new RequestParams.Builder().setResource(String.format(Utils.MEASURE_BY_PATIENT_ID_RESOURCE_PATTERN, patientId)).setHttpMethod(HttpMethod.GET).buildWithAuthorization();
            final RequestAsyncTask requestAsyncTask = new RequestAsyncTask.Builder().setRequestParams(requestParams).setPostProcessor(retrieveMeasuresPostProcessor).buildWithDialog(getActivity());
            requestAsyncTask.execute();
        } catch (final MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        System.out.println("ATTACH CONTEXT");
    }


}

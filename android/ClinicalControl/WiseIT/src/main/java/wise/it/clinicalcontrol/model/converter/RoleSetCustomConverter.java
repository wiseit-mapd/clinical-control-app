package wise.it.clinicalcontrol.model.converter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import wise.it.clinicalcontrol.model.Authenticable;
import wise.it.clinicalcontrol.model.measure.HealthMeasure;

public class RoleSetCustomConverter implements CustomConverter<Set<Authenticable.Role>> {

    @Override
    public Set<Authenticable.Role> convertToModel(final String property, final JSONObject jsonObject) throws JSONException {
        final Set<Authenticable.Role> result = new HashSet<>();
        final JSONArray array = jsonObject.getJSONArray(property);
        for (int k = 0; k < array.length(); k++) {
            result.add(Authenticable.Role.valueOf((String)array.get(k)));
        }
        return result;
    }

    @Override
    public void convertToJSON(final Field field, final Object instance, final Map<String,Object> propertiesMap) {
        ReflectionUtils.makeAccessible(field);
        final Set<Authenticable.Role> roles = (Set<Authenticable.Role>) ReflectionUtils.getField(field, instance);
        if(roles == null) { return;}
        final JSONArray array = new JSONArray();
        for (final Authenticable.Role role : roles) {
            array.put(role.name());
        }
        propertiesMap.put(field.getName(), array);
    }
}

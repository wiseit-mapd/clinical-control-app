//WiseIT
package wise.it.clinicalcontrol.doctor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import wise.it.clinicalcontrol.CustomCalendarView;
import wise.it.clinicalcontrol.EditTestActivity;
import wise.it.clinicalcontrol.EditTestFragment;
import wise.it.clinicalcontrol.R;
import wise.it.clinicalcontrol.Token;

/**
 * Created by rider on 04.11.2016.
 */

public class DoctorMyPatientsFragment extends Fragment {
    private FragmentTransaction mFragmentTransaction;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_doctor_patients_list, container, false);
        mFragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        mFragmentTransaction.replace(R.id.patients_list_view_container, new ListPatientFragment()).commit();



        return v;
    }
}

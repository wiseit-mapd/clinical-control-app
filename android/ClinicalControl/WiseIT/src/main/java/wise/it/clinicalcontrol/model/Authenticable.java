package wise.it.clinicalcontrol.model;

import java.io.Serializable;
import java.util.Set;

import wise.it.clinicalcontrol.cache.Cacheable;

public interface Authenticable extends Cacheable, Serializable {

    public enum Role {
        PATIENT,
        DOCTOR,
        NURSE;
    }

    public enum Gender {
        MALE,
        FEMALE;
    }

    String getAuthenticableId();
    String getUsername();
    String getPassword();
    Set<Role> getRoles();
    String getName();
    String getEmail();
    String getPhone();
    String getDateOfBirth();
    Gender getGender();
    Object getPhoto();
    String getAddress();
    void setUsername(String username);
    void setPassword(final String password);
    void setRoles(Set<Authenticable.Role> roles);
    void setName(final String name);
    void setEmail(final String email);
    void setPhone(final String phone);
    void setDateOfBirth(final String dateOfBirth);
    void setGender(Gender gender);
    void setPhoto(Object photo);
    void setAddress(String address);


}

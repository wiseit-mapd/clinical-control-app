//WiseIT
package wise.it.clinicalcontrol.patient;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import wise.it.clinicalcontrol.EditTestActivity;
import wise.it.clinicalcontrol.EditTestFragment;
import wise.it.clinicalcontrol.ListTestActivity;
import wise.it.clinicalcontrol.ListTestFragment;
import wise.it.clinicalcontrol.R;
import wise.it.clinicalcontrol.SHealthActivity;
import wise.it.clinicalcontrol.SHealthFragment;
import wise.it.clinicalcontrol.authentication.AuthenticationHolder;

/**
 * Created by rider on 04.11.2016.
 */

public class PatientMyHealthFragment extends Fragment {

    private TextView userName;
    private ImageButton syncSamsung;
    private CardView testCard;

    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_patient_my_health, container, false);

        userName = (TextView)v.findViewById(R.id.user_patient_name_label);
        userName.setText(AuthenticationHolder.getUser().asPatient().getAuthenticable().getName());

        syncSamsung = (ImageButton)v.findViewById(R.id.user_patient_samsung_sync_link);
        syncSamsung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Context context = getContext();
                final Intent intent = new Intent(context, SHealthActivity.class);
                intent.putExtra(SHealthFragment.PATIENT_ID_PARAM, AuthenticationHolder.getUser().getId());
                context.startActivity(intent);
            }
        });

        testCard = (CardView)v.findViewById(R.id.patient_test_clicable_card);
        testCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Context context = getContext();
                final Intent intent = new Intent(context, ListTestActivity.class);
                intent.putExtra(ListTestFragment.PATIENT_ID_PARAM, AuthenticationHolder.getUser().getId());
                context.startActivity(intent);
            }
        });



        return v;
    }
}

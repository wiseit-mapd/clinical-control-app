package wise.it.clinicalcontrol.cache;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Cache {

    private static Cache instance;
    private Map<Class<?>,Map<String,Cacheable>> cache = new HashMap<>();

    private Cache() {
    }

    public static Cache getInstance() {
        if (instance == null) {
            instance = new Cache();
        }
        return instance;
    }


    public <T extends Cacheable> T get(final Class<T> clazz, final String id) {
        final Map<String,T> cacheablesMap = getCacheableMap(clazz);
        return cacheablesMap.get(id);
    }

    public <T extends Cacheable> void put(final Class<T> clazz, final Collection<T> cacheables) {
        final Map<String,T> cacheablesMap = getCacheableMap(clazz);
        for (final T cacheable : cacheables) {
            cacheablesMap.put(cacheable.generateKey(), cacheable);
        }
    }

    private <T extends Cacheable> Map<String,T> getCacheableMap(final Class<T> clazz) {
        Map<String,Cacheable> cacheablesMap = cache.get(clazz);
        if (cacheablesMap == null) {
            cacheablesMap = new HashMap<String,Cacheable>();
            cache.put(clazz, cacheablesMap);
        }
        return (Map<String,T>)cacheablesMap;
    }

    public <T extends Cacheable> Collection<T> get(final Class<T> clazz) {
        final Map<String,T> cacheablesMap = getCacheableMap(clazz);
        return new HashSet<>(cacheablesMap.values());
    }
}

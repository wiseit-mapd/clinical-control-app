package wise.it.clinicalcontrol.doctor;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import wise.it.clinicalcontrol.R;

/**
 * Created by rider on 12/14/2016.
 */

public class DoctorPatientPagerAdapter extends FragmentPagerAdapter {
    private String tabTitles[];
    Context mContext;
    private List<Fragment> mFragmentList;

    public DoctorPatientPagerAdapter(Context context, FragmentManager fragmentManager, List<Fragment> listFragments) {
        super(fragmentManager);
        this.mFragmentList = listFragments;
        this.mContext = context;

        tabTitles = new String[] {context.getResources().getString(R.string.doctor_profile_tab_label),
                context.getResources().getString(R.string.patient_appointments_tab_label),
                context.getResources().getString(R.string.doctor_patient_test_tab_label)};
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}

package wise.it.clinicalcontrol.authentication;

import wise.it.clinicalcontrol.model.User;

/**
 * Created by rider on 11/17/2016.
 */

public final class AuthenticationHolder {

    private static User user;


    public static void setUser(final User user) {
//        if (AuthenticationHolder.user != null) {
//            throw new IllegalStateException("User has already been set.");
//        }
        AuthenticationHolder.user = user;
    }

    public static User getUser() {
        if (user == null) {
            throw new IllegalArgumentException("There is no user set.");
        }
        return user;
    }
}

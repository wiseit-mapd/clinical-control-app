package wise.it.clinicalcontrol.doctor;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import wise.it.clinicalcontrol.R;
import wise.it.clinicalcontrol.patient.PatientAppointmentsFragment;
import wise.it.clinicalcontrol.patient.PatientMyHealthFragment;
import wise.it.clinicalcontrol.patient.PatientPagerAdapter;

/**
 * Created by rider on 06.11.2016.
 */

public class DoctorTabFragment extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Context mContext;
    private List<Fragment> mFragmentList;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /**
         *Inflate tab_layout and setup Views.
         */
        View x =  inflater.inflate(R.layout.patient_tab_layout,null);
        tabLayout = (TabLayout) x.findViewById(R.id.patient_sliding_tabs);
        viewPager = (ViewPager) x.findViewById(R.id.patient_viewpager);
        /**
         *Set an Apater for the View Pager
         */
        mFragmentList = new ArrayList<Fragment>();
        mFragmentList.add(new DoctorProfileFragment());
        mFragmentList.add(new DoctorMyPatientsFragment());

        viewPager.setAdapter(new DoctorPagerAdapter(getContext(),getActivity().getSupportFragmentManager(),mFragmentList));

        /**
         * Now , this is a workaround ,
         * The setupWithViewPager dose't works without the runnable .
         * Maybe a Support Library Bug .
         */

        tabLayout.setupWithViewPager(viewPager);

        return x;

    }

}

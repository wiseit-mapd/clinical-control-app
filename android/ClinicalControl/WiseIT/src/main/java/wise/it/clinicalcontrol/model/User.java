package wise.it.clinicalcontrol.model;

/**
 * Created by rider on 11/15/2016.
 */

public class User {

    private final Identifiable identifiable;

    public User(final Identifiable identifiable) {
        this.identifiable = identifiable;
    }

    public String getId() {
        return identifiable.getId();
    }

    public Patient asPatient() {
        return (Patient) identifiable;
    }

    public Doctor asDoctor() {
        return (Doctor) identifiable;
    }

    public Nurse asNurse() {
        return (Nurse) identifiable;
    }
}

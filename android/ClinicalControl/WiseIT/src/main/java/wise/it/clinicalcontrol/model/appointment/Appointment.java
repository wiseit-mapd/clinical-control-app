package wise.it.clinicalcontrol.model.appointment;

import android.content.res.Resources;
import android.text.InputType;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;

import wise.it.clinicalcontrol.model.Identifiable;
import wise.it.clinicalcontrol.util.Utils;

/**
 * Created by rider on 12/15/2016.
 */

public interface Appointment extends Identifiable, Serializable {

    public enum State {
        PROCESSING_DOCTOR(String.class),
        PROCESSING_PATIENT(String.class),
        APPROVED(String.class),
        DONE(String.class),
        CANCELLED(String.class);

        private final Class<?> stateClass;


        State(Class<?> stateClass) {
            this.stateClass = stateClass;
        }

        public Object getStateFromString(final String stateStr) {
            try {
                return stateClass.getConstructor(String.class).newInstance(stateStr.toUpperCase());
            } catch (final NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        }

        public String getStateUnitString(final Resources resources, final String packageName) {
            return Utils.getBundle(resources, name().toLowerCase() + "_unit", packageName);
        }
    }

    String getPatientId();

    String getDoctorId();

    DateTime getAppointmentDate();

    String getReason();

    String getCondition();

    String getPrescription();

    State getState();

    void setPatientId(String patientId);

    void setDoctorId(String doctorId);

    void setAppointmentDate(DateTime appointmentDate);

    void setReason(String reason);

    void setCondition(String condition);

    void setPrescription(String prescription);

    void setState(State state);
}

package wise.it.clinicalcontrol;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import wise.it.clinicalcontrol.cache.Cache;
import wise.it.clinicalcontrol.model.Authenticable;
import wise.it.clinicalcontrol.model.CommonAuthenticable;
import wise.it.clinicalcontrol.model.Patient;
import wise.it.clinicalcontrol.model.measure.HealthMeasure;
import wise.it.clinicalcontrol.model.measure.HealthMeasureImpl;
import wise.it.clinicalcontrol.util.Utils;

public class FromJSONToModelConverterTest {

    @Test
    public void convertPatientJSONToModel() {
        final String authenticablesJson = "[{authenticableId:\"581cf60a89fe040300c9346c\",name:\"Willian Campos\",phone:\"1234567890\",email:\"willian.campos@gmail.com\",dateOfBirth:\"19870705\",address:\"123 Progress Avenue, Scarborough, ON, Canada\"}]";
        final String patientsJson = "[{_id:\"581cf60a89fe040300c9346d\",doc_ids:\"1\",diagnosis:\"temp\",authenticableId:\"581cf60a89fe040300c9346c\"}]";
        try {
            //Convert authenticables JSON to model
            final List<Authenticable> authenticables = Utils.<Authenticable>convertToModel(new JSONArray(authenticablesJson), CommonAuthenticable.class);
            //Cache converted authenticables
            Cache.getInstance().put(Authenticable.class, authenticables);

            //Convert patients JSON to model
            final List<Patient> patients = Utils.<Patient>convertToModel(new JSONArray(patientsJson), Patient.class);


            Assert.assertEquals(1, patients.size());

            final Patient patient = patients.get(0);
            Assert.assertEquals("581cf60a89fe040300c9346d", patient.getId());
            Assert.assertEquals("Willian Campos", patient.getAuthenticable().getName());
            Assert.assertEquals("1234567890", patient.getAuthenticable().getPhone());
            Assert.assertEquals("willian.campos@gmail.com", patient.getAuthenticable().getEmail());
            Assert.assertEquals("19870705", patient.getAuthenticable().getDateOfBirth());
            Assert.assertEquals("temp", patient.getDiagnosis());
            Assert.assertEquals("123 Progress Avenue, Scarborough, ON, Canada", patient.getAuthenticable().getAddress());
            Assert.assertEquals("581cf60a89fe040300c9346c", patient.getAuthenticable().getAuthenticableId());
        } catch (final JSONException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void convertTestsJSONToModel() {
        final String testsJson = "[{\"_id\": \"5835c4290d644b1500c6bd14\",\"patientId\": \"5834d8514bfa901500ab03af\",\"responsibleAuthenticableId\": \"5834d99c90a6d715003d05a9\",\"appointmentId\": null,\"measurementDate\": \"1479859382268\",\"type\": \"HEART_BEAT_RATE\",\"value\": \"100\"}]";
        try {
            //Convert test JSON to model
            final List<HealthMeasure> tests = Utils.<HealthMeasure>convertToModel(new JSONArray(testsJson), HealthMeasureImpl.class);

            Assert.assertEquals(1, tests.size());

            final HealthMeasure test = tests.get(0);
            Assert.assertEquals("5835c4290d644b1500c6bd14", test.getId());
            Assert.assertEquals("5834d8514bfa901500ab03af", test.getPatientId());
            Assert.assertEquals("5834d99c90a6d715003d05a9", test.getResponsibleAuthenticableId());
            Assert.assertEquals(null, test.getAppointmentId());
            Assert.assertEquals(new DateTime(1479859382268l), test.getMeasurementDate());
            Assert.assertEquals(HealthMeasure.Type.HEART_BEAT_RATE, test.getType());
            Assert.assertEquals(100, test.getValue());
        } catch (final JSONException e) {
            Assert.fail(e.getMessage());
        }
    }
}

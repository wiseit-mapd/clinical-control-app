package wise.it.clinicalcontrol;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;

import wise.it.clinicalcontrol.model.Authenticable;
import wise.it.clinicalcontrol.model.Patient;
import wise.it.clinicalcontrol.model.measure.HealthMeasure;
import wise.it.clinicalcontrol.model.measure.HealthMeasureImpl;
import wise.it.clinicalcontrol.util.Utils;

public class FromModelToJSONConverterTest {

    @Test
    public void convertPatientModelToJSON() {
        final Patient patient = new Patient(
                "581cf60a89fe040300c9346d",
                "581cf60a89fe040300c9346c",
                "willian.campos@gmail.com",
                "test",
                Collections.singleton(Authenticable.Role.PATIENT),
                "Willian Campos",
                "willian.campos@gmail.com",
                "1234567890",
                "19870705",
                Authenticable.Gender.MALE,
                null,
                "temp",
                "123 Progress Avenue, Scarborough, ON, Canada");

        //Invoke the conversion from model to JSON
        final JSONObject jsonObject = Utils.convertToJSON(patient);

        try {
            Assert.assertEquals("581cf60a89fe040300c9346d", jsonObject.getString("_id"));
            Assert.assertEquals("581cf60a89fe040300c9346c", jsonObject.getString("authenticableId"));
            Assert.assertEquals("Willian Campos", jsonObject.getString("name"));
            Assert.assertEquals("willian.campos@gmail.com", jsonObject.getString("email"));
            Assert.assertEquals("1234567890", jsonObject.getString("phone"));
            Assert.assertEquals("19870705", jsonObject.getString("dateOfBirth"));
            Assert.assertEquals("temp", jsonObject.getString("diagnosis"));
            Assert.assertEquals("123 Progress Avenue, Scarborough, ON, Canada", jsonObject.getString("address"));
        } catch (final JSONException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void convertMeasureModelToJSON() {
        final HealthMeasure test = new HealthMeasureImpl<Integer>(
                "5835c4290d644b1500c6bd14",
                "5834d8514bfa901500ab03af",
                "5834d99c90a6d715003d05a9",
                null,
                new DateTime(1479859382268l),
                HealthMeasure.Type.HEART_BEAT_RATE,
                100);


        //Invoke the conversion from model to JSON
        final JSONObject jsonObject = Utils.convertToJSON(test);

        try {
            Assert.assertEquals("5835c4290d644b1500c6bd14", jsonObject.getString("_id"));
            Assert.assertEquals("5834d8514bfa901500ab03af", jsonObject.getString("patientId"));
            Assert.assertEquals("5834d99c90a6d715003d05a9", jsonObject.getString("responsibleAuthenticableId"));
            Assert.assertEquals(true, jsonObject.isNull("appointmentId"));
            Assert.assertEquals(1479859382268l, jsonObject.getLong("measurementDate"));
            Assert.assertEquals("HEART_BEAT_RATE", jsonObject.getString("type"));
            Assert.assertEquals(100, jsonObject.getInt("value"));
        } catch (final JSONException e) {
            Assert.fail(e.getMessage());
        }
    }
}

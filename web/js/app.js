var serverUrl = 'https://clinical-control.herokuapp.com/';
var tokenResource = "token";
var previousActive;
function login() {
    var email = $("#email").val();
    var password = md5($("#password").val());
    var tokenRequestUrl = serverUrl + tokenResource;
    $.ajax({
        url: tokenRequestUrl,
        dataType: "json",
        contentType: 'application/json',
        data: JSON.stringify({ username: email, password: password }),
        type: 'POST'
    })
        .done(function (token) {
        console.log(token);
        setToken(token);
        goToMenu();
    })
        .fail(function () {
        alert('It was not possible to login');
    });
}
function onLoadMain() {
    if (isValidTokenPresent()) {
        goToMenu();
    }
    else {
        goToLogin();
    }
}
function onLoadLogin() {
    if (isValidTokenPresent()) {
        goToMenu();
    }
}
function onLoadMenu() {
    if (!isValidTokenPresent()) {
        goToLogin();
    }
}
function logout() {
    clearToken();
    goToLogin();
}
function goToLogin() {
    window.location.href = 'login.html';
}
function goToMenu() {
    window.location.href = 'menu.html';
}
function isValidTokenPresent() {
    if (getToken() == null) {
        return false;
    }
    console.log(getToken());
    return true;
}
function getToken() {
    return sessionStorage.getItem("token");
}
function setToken(token) {
    sessionStorage.setItem("token", token);
}
function clearToken() {
    sessionStorage.removeItem("token");
}
//# sourceMappingURL=app.js.map
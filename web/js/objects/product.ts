module objects {
    export class Patient {
        name: string;
        phone: string;
        dateOfBirth: string;
        doc_ids: number[];
        diagnosis: string;
        

        constructor(name:string, phone:string, dateOfBirth:string, doc_ids:number[], diagnosis:string ) {
            this.name = name;
            this.phone = phone;
            this.dateOfBirth = dateOfBirth;
            this.doc_ids = doc_ids;
            this.diagnosis = diagnosis;
        }

    }
}
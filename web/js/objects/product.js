var objects;
(function (objects) {
    var Patient = (function () {
        function Patient(name, phone, dateOfBirth, doc_ids, diagnosis) {
            this.name = name;
            this.phone = phone;
            this.dateOfBirth = dateOfBirth;
            this.doc_ids = doc_ids;
            this.diagnosis = diagnosis;
        }
        return Patient;
    }());
    objects.Patient = Patient;
})(objects || (objects = {}));
//# sourceMappingURL=product.js.map
module.exports.extractAuthenticableFromRequestParams = function(req, validation) {

    var md5 = require('js-md5')
    var md5_password = md5(req.params.password)
    var authenticable = {
        username: req.params.email,
        password: md5_password,
        name: req.params.name,
        phone: req.params.phone,
        email: req.params.email,
        dateOfBirth: req.params.dateOfBirth,
        gender: req.params.gender,
        address: req.params.address,
        photo: null
        //photo: req.params.photo
    }

    return authenticable 
}

module.exports.generatePassword = function() {
    var generator = require('generate-password')
    var password =  generator.generate({
                        length: 8,
                        numbers: true,
                        uppercase: false
                    })
    return password
}


module.exports.extractAuthenticableFromRequestAuthentication = function(req) {
    return {
        username: req.user.username,
        password: req.user.password
    }
}

module.exports.encodeImage = function(file) {
    var fs = require('fs');
    var image = fs.readFileSync(file);
    console.log("image encoding");
    return new Buffer(image).toString('base64');
}


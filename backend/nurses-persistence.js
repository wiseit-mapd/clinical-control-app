
module.exports.set = function (server, restify, authenticablePersistence) {

    var db = require('./db')
    var saveMongodb = require('save-mongodb')
    var utils = require('./utils')

    db.get().collection('nurses', function (error, collection) {
        var jwt = require('jsonwebtoken');
        // Get a persistence engine for the nurses  
        var nursesPersistence = require('save')('nurses', { engine: saveMongodb(collection) })

        // Get all nurses in the system
        server.get('/nurses', function (req, res, next) {
            //Log request received
            console.log('/nurses GET: request received')
            
            // Find every entity within the given collection
            nursesPersistence.find({}, function (error, nurses) {

                //Log sendig response
                console.log('/nurses GET: sending response')
                // Return all of the nurses in the system
                res.send(nurses)
            })
        })

        // Get a single nurse by its id
        server.get('/nurses/:id', function (req, res, next) {
            //Log request received
            console.log('/nurses/'+req.params.id+' GET: request received')

            // Find a single nurse by its id within save
            nursesPersistence.findOne({ _id: req.params.id }, function (error, nurse) {
                // If there are any errors, pass them to next in the correct format
                if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                //Log sending response
                console.log('/nurses/'+req.params.id+' GET: sending response')
                if (nurse) {
                    // Send the nurse if no issues
                    authenticablePersistence.findById(nurse.authenticableId, function(error, authenticable) {
                        authenticable.authenticableId = authenticable._id
                        delete authenticable._id
                        delete authenticable.password
                        delete authenticable.username
                        console.log(authenticable)
                        res.send({nurse: nurse, authenticable: authenticable})
                    })
                } else {
                    // Send 404 header if the nurse doesn't exist
                    res.send(404)
                }
            })
        })

        // Get a single nurse by its authenticableId
        server.get('/nurses/authenticableId/:id', function (req, res, next) {
            //Log request received
            console.log('/nurses/authenticableId/'+req.params.id+' GET: request received')

            // Find a single nurse by its authenticableId within save
            nursesPersistence.findOne({ authenticableId: req.params.id }, function (error, nurse) {
                // If there are any errors, pass them to next in the correct format
                if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                //Log sending response
                console.log('/nurses/authenticableId/'+req.params.id+' GET: sending response')
                if (nurse) {
                    // Send the nurse if no issues
                    authenticablePersistence.findById(nurse.authenticableId, function(error, authenticable) {
                        authenticable.authenticableId = authenticable._id
                        delete authenticable._id
                        delete authenticable.password
                        delete authenticable.username
                        console.log(authenticable)
                        res.send({nurse: nurse, authenticable: authenticable})
                    })
                } else {
                    // Send 404 header if the nurse doesn't exist
                    res.send(404)
                }
            })
        })

        // Create a new nurse
        server.post('/nurses', function (req, res, next) {
            //Log request received
            console.log('/nurses POST: request received')

            validationErrors = validateNurse(req)
            if (validationErrors.length > 0 ) {
                return next(new restify.InvalidArgumentError(JSON.stringify(validationErrors)))
            }

            req.params.password = utils.generatePassword()
            var user = utils.extractAuthenticableFromRequestParams(req)
            user.roles = ['NURSE']
            authenticablePersistence.create(user, function(error, authenticable) {

                if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                var newNurse = {
                    professionalIdentifier: req.params.professionalIdentifier,
                    doc_ids: req.params.doc_ids,
                    authenticableId: authenticable._id
            }

             // Create the nurses using the persistence engine
                nursesPersistence.create( newNurse, function (error, nurse) {
                    // If there are any errors, pass them to next in the correct format
                    if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                    //Log sending response
                    console.log('/nurses POST: sending response')

                    // Send the nurse if no issues
                    res.send(201, nurse)
                })
                
            })
            
        })

        // Update a nurse by its id
        server.put('/nurses/:id', function (req, res, next) {
            //Log request received
            console.log('/nurses/'+req.params.id+' PUT: request received')

            validationErrors = validateNurse(req)
            if (validationErrors.length > 0) {
                return next(new restify.InvalidArgumentError(JSON.stringify(validationErrors)))
            }

            //TODO: Validate nurse fields
            var updatedNurse = {
                _id: req.params.id,
                professionalIdentifier: req.params.professionalIdentifier,
                doc_ids: req.params.doc_ids
            }

            // Update the nurse with the persistence engine
            nursesPersistence.update(updatedNurse, true, function (error, nurse) {

            // If there are any errors, pass them to next in the correct format
            if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

            //Log sending response
            console.log('/nurses/:id PUT: sending response')

            // Send a 200 OK response
            res.send(updatedNurse)
            })
        })

        // Delete nurse with the given id
        server.del('/nurses/:id', function (req, res, next) {
            //Log request received
            console.log('/nurses/'+req.params.id+' DELETE: request received')

            // Delete the nurse with the persistence engine
            nursesPersistence.delete(req.params.id, function (error, nurse) {

            // If there are any errors, pass them to next in the correct format
            if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

            //Log sending response
            console.log('/nurses/'+req.params.id+' DELETE: sending response')

            // Send a 200 OK response
            res.send(nurse)
            })
        })

        // Delete all nurses
        server.del('/nurses', function (req, res, next) {
            //Log request received
            console.log('/nurses DELETE: request received')

            // Delete the nurses with the persistence engine
            nursesPersistence.deleteMany({}, function (error, nurses) {

            // If there are any errors, pass them to next in the correct format
            if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

            //Log sending response
            console.log('/nurses DELETE: sending response')

            // Send a 200 OK response
            res.send()
            })
        })

        console.log('/nurses method: GET')
        console.log('/nurses method: POST')
        console.log('/nurses/:id method: GET')
        console.log('/nurses/authenticableId/:id method: GET')
        console.log('/nurses/:id method: PUT')
        console.log('/nurses/:id method: DELETE')
    })
}

function validateNurse(req) {
    var errors = []
    //make sure professional identifier is defined
    if(req.params.professionalIdentifier === undefined) {
        errors.push('professional identifier must be provided')
    }
    //make sure doctors ids is defined
    if (req.params.doc_ids === undefined) {
        errors.push('doctors ids must be provided')
    }
    return errors
}

var scheduleAppointment = require('../schedule-appointment')
var DayOfWeek = require('js-joda').DayOfWeek

var patient = {
    id: 1,
    firstName: "John",
    lastName: "Locke"
}


var doctor = {
    id: 1,
    firstName: "Jack",
    lastName: "Sheppard",
    appointmentSettings: {
        appointmentDuration: 'PT1800S'
    },
    workingTimes: [
        {weekday: DayOfWeek.MONDAY, start: "08:00", end: "12:30"},
        {weekday: DayOfWeek.MONDAY, start: "15:00", end: "17:00"},
        {weekday: DayOfWeek.TUESDAY, start: "08:00", end: "12:00"}
    ]
}

var appointments = [
    {
        date: "2016-10-24T08:00:00.000",
        duration: 'PT1800S',
        doctorId: 1
    },
    {
        date: "2016-10-24T08:30:00.000",
        duration: 'PT1800S',
        doctorId: 1
    },
    {
        date: "2016-10-24T09:30:00.000",
        duration: 'PT1800S',
        doctorId: 1
    }
]
    

var appointmentInfo = {
    date: "2016-10-24T09:00:00.000",
    patientId: 1,
    doctorId: 1
}
var valid = scheduleAppointment.canSchedule(patient, doctor, appointmentInfo, appointments)
console.log(valid)

appointmentInfo = {
    date: "2016-10-24T16:45:00.000",
    patientId: 1,
    doctorId: 1
}
valid = scheduleAppointment.canSchedule(patient, doctor, appointmentInfo, appointments)
console.log(valid)
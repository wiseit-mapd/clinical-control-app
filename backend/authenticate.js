module.exports.set = function (server, restify, authenticablePersistence) {
    // used to create, sign, and verify tokens
    var restifyJwt = require('restify-jwt');
    var jwt = require('jsonwebtoken');
    var config = require('./config');

    server.use(restifyJwt({ secret: config.secret }).unless({path: ['/token']}));

    server.post('/token', function (req, res, next) {
        // Make sure username is defined
        if (req.params.username === undefined) {
            return next(new restify.InvalidArgumentError('username must be provided'))
        }
        // Make sure password is defined
        if (req.params.password === undefined) {
            return next(new restify.InvalidArgumentError('password must be provided'))
        }
        
        var user = {
            username: req.params.username,
            password: req.params.password
        }

        authenticablePersistence.find(user, function (error, authenticable) {
            if (error) return next(error)

            if (!authenticable) {
                return res.send(401, "Invalid credencials")
            } 
            // if user is found and password is right, create a token
            var token = jwt.sign(authenticable, config.secret, {
                expiresIn: 86400 // expires in 24 hours
            });

            if (token) {
                // Send the token if no issues
                res.send(token)
            } else {
                // Send 404 header if the token doesn't exist
                res.send(404)
            }
        })
    })
}
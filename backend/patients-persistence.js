
module.exports.set = function (server, restify, authenticablePersistence) {

    var db = require('./db')
    var utils = require('./utils')
    var saveMongodb = require('save-mongodb')
    var emailer = require('./emailer')

    db.get().collection('patients', function (error, collection) {
        var jwt = require('jsonwebtoken');
        // Get a persistence engine for the patients  
        var patientsPersistence = require('save')('patients', { engine: saveMongodb(collection) })

        module.exports.findById = function(patientId, callback) {
            // Find a single patient by its id within save
            patientsPersistence.findOne({ _id: patientId }, function (error, patient) {
                // If there are any errors, pass them to calback in the correct format
                if (error) {
                    callback(error, null)
                } else {
                    callback(null, patient)
                }
            })
        }

        // Get all patients in the system
        server.get('/patients', function (req, res, next) {
            //Log request received
            console.log('/patients GET: request received')

            // Find every entity within the given collection
            patientsPersistence.find({}, function (error, patients) {
                var authenticableIds = []
                    for (var k in patients) {
                        var patient = patients[k]
                        authenticableIds.push(patient.authenticableId)
                    }

                    authenticablePersistence.findByIds(authenticableIds, function(error, authenticables) {
                        // If there are any errors, pass them to next in the correct format
                        if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                        for (var k in authenticables) {
                            var authenticable = authenticables[k]
                            authenticable.authenticableId = authenticable._id
                            delete authenticable._id
                            delete authenticable.password
                        }

                        //Log sendig response
                        console.log('/patients GET: sending response')
                        // Send the patients and authenticables if no issues
                        res.send({patients: patients, authenticables: authenticables})
                    })
            })
        })

        // Get a single patient by its id
        server.get('/patients/:id', function (req, res, next) {
            //Log request received
            console.log('/patients/'+req.params.id+' GET: request received')

            // Find a single patient by its id within save
            patientsPersistence.findOne({ _id: req.params.id }, function (error, patient) {
                // If there are any errors, pass them to next in the correct format
                if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                //Log sending response
                console.log('/patients/'+req.params.id+' GET: sending response')
                if (patient) {
                    // Send the patient if no issues
                    //console.log(patient.photo)
                    //patient.photo = utils.decodeImage(patient.photo)
                    authenticableId = patient.authenticableId
                    console.log(authenticableId)
                    authenticablePersistence.findById(authenticableId, function(error, authenticable) {
                        authenticable.authenticableId = authenticable._id
                        delete authenticable._id
                        delete authenticable.password
                        delete authenticable.username
                        console.log(authenticable)
                        res.send({patient: patient, authenticable: authenticable})
                    })
                } else {
                    // Send 404 header if the patient doesn't exist
                    res.send(404)
                }
            })

        })

        // Get a single patient by its authenticableId
        server.get('/patients/authenticableId/:id', function (req, res, next) {
            //Log request received
            console.log('/patients/authenticableId/'+req.params.id+' GET: request received')

            // Find a single patient by its authenticableId within save
            patientsPersistence.findOne({ authenticableId: req.params.id }, function (error, patient) {
                // If there are any errors, pass them to next in the correct format
                if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                //Log sending response
                console.log('/patients/authenticableId/'+req.params.id+' GET: sending response')
                if (patient) {
                    // Send the patient if no issues
                    //console.log(patient.photo)
                    //patient.photo = utils.decodeImage(patient.photo)
                    authenticablePersistence.findById(patient.authenticableId, function(error, authenticable) {
                        authenticable.authenticableId = authenticable._id
                        delete authenticable._id
                        delete authenticable.password
                        delete authenticable.username
                        console.log(authenticable)
                        res.send({patient: patient, authenticable: authenticable})
                    })
                } else {
                    // Send 404 header if the patient doesn't exist
                    res.send(404)
                }
            })

        })

        // Create a new patient
        server.post('/patients', function (req, res, next) {
            //Log request received
            console.log('/patients POST: request received')
            
            validationErrors = validatePatient(req)
            if (validationErrors.length > 0 ) {
                return next(new restify.InvalidArgumentError(JSON.stringify(validationErrors)))
            }

            req = adaptRequestWithUser(req)
            emailer.sendEmailWithPassword(req)
            var user = utils.extractAuthenticableFromRequestParams(req)
            user.roles = ['PATIENT']
            //req.params.photo = utils.encodeImage(req.params.photo)
            authenticablePersistence.create(user, function(error, authenticable) {
                // If there are any errors, pass them to next in the correct format
                if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                var newPatient = {
                    doc_ids: req.params.doctors_ids,
                    diagnosis: req.params.diagnosis,
                    authenticableId: authenticable._id
                }

                // Create the patients using the persistence engine
                patientsPersistence.create( newPatient, function (error, patient) {
                // If there are any errors, pass them to next in the correct format
                if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                //Log sending response
                console.log('/patients POST: sending response')

                // Send the patient if no issues
                res.send(201, patient)
                })    
            })
        })

        // Update a patient by its id
        server.put('/patients/:id', function (req, res, next) {
            //Log request received
            console.log('/patients/'+req.params.id+' PUT: request received')

            validationErrors = validatePatient(req)
            if (validationErrors.length > 0 ) {
                return next(new restify.InvalidArgumentError(JSON.stringify(validationErrors)))
            }

            var updatedPatient = {
                _id: req.params.id,
                doc_ids: req.params.doctors_ids,
                diagnosis: req.params.diagnosis
            }

            var updatedAuthenticable = {
                _id :req.params.authenticableId,
                name: req.params.name,
                gender: req.params.gender,
                dateOfBirth: req.params.dateOfBirth,
                address: req.params.address,
                email: req.params.email,
                phone: req.params.phone,
            }

            // Update the patient with the persistence engine
            patientsPersistence.update(updatedPatient, function (error, patient) {
                // If there are any errors, pass them to next in the correct format
                if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                //Log sending response
                console.log('/patients/'+req.params.id+' PUT: sending response')
                authenticablePersistence.update(updatedAuthenticable, function(error, authenticable) {
                    if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))
                    updatedAuthenticable.authenticableId = updatedAuthenticable._id
                    delete updatedAuthenticable._id
                    res.send({updatedPatient : updatedPatient, updatedAuthenticable: updatedAuthenticable})
                })
                // Send a 200 OK response
            })
        })

        // Delete patient with the given id
        server.del('/patients/:id', function (req, res, next) {
            //Log request received
            console.log('/patients/'+req.params.id+' DELETE: request received')
            patientsPersistence.findOne({ _id: req.params.id }, function (error, patient) {
            // Delete the patient with the persistence engine
            authenticableId = patient.authenticableId
                patientsPersistence.delete(req.params.id, function (error, patient) {
                    // If there are any errors, pass them to next in the correct format
                    if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))
                    //Log sending response
                    authenticablePersistence.delete(authenticableId, function (error,authenticable) {
                        res.send(patient,authenticable)
                    })
                    console.log('/patients/'+req.params.id+' DELETE: sending response')
                    // Send a 200 OK response
                    
                })
            })
        })

        // Delete all patients
        server.del('/patients', function (req, res, next) {
            //Log request received
            console.log('/patients DELETE: request received')

            // Delete the patients with the persistence engine
            patientsPersistence.deleteMany({}, function (error, patients) {
                // If there are any errors, pass them to next in the correct format
                if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))
                //Log sending response
                console.log('/patients DELETE: sending response')
                // Send a 200 OK response
                res.send()
            })
        })

        console.log('/patients method: GET')
        console.log('/patients method: POST')
        console.log('/patients/:id method: GET')
        console.log('/patients/authenticableId/:id method: GET')
        console.log('/patients/:id method: PUT')
        console.log('/patients/:id method: DELETE')
    })
}

function validatePatient(req) {
    var errors = []
    // Make sure doctors ids is defined
//    if (req.params.doctors_ids === undefined) {
//        errors.push("doctors' ids must be provided")
//    }
    // Make sure diagnosis is defined
    if (req.params.diagnosis === undefined) {
        errors.push('diagnosis must be provided')
    }

    return errors
}

function adaptRequestWithUser(req) {
    req.params.password = req.params.dateOfBirth
    return req 
}

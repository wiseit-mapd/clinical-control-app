var SERVER_NAME = 'clinical-control-backend'
var PORT = (process.env.PORT || 5000);
var URI = process.env.mongolab_uri;

var restify = require('restify')
// Create the restify server
var server = restify.createServer({ name: SERVER_NAME})
// Create the database
var db = require('./db')

server
  // Allow the use of POST
  .use(restify.fullResponse())
  // Maps req.body to req.params so there is no switching between them
  .use(restify.bodyParser())
  .use(restify.CORS())

server.opts(/.*/, function (req,res,next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", req.header("Access-Control-Request-Method"));
  res.header("Access-Control-Allow-Headers", req.header("Access-Control-Request-Headers"));
  res.send(200);
  return next();
});

// Connect to Mongo on start
db.connect(function(err) {
  if (err) {
    console.log('Unable to connect to Mongo.')
    process.exit(1)
  } else {
    server.listen(PORT, function () {
      console.log('Server %s listening at %s', server.name, server.url)
    })

    //Configure resources
    console.log('Resources:')

    var authenticablePersistence = require('./authenticable-persistence')
    authenticablePersistence.set(server, restify)


    var authenticate = require('./authenticate')
    authenticate.set(server, restify, authenticablePersistence)

    var patientsPersistence = require('./patients-persistence')
    patientsPersistence.set(server, restify, authenticablePersistence)

    var doctorsPersistence = require('./doctors-persistence')
    doctorsPersistence.set(server, restify, authenticablePersistence)

    var nursesPersistence = require('./nurses-persistence')
    nursesPersistence.set(server, restify, authenticablePersistence)

    var measuresPersistence = require('./measures-persistence')
    measuresPersistence.set(server, restify, patientsPersistence, authenticablePersistence)

    var messagesPersistence = require('./messages-persistence')
    messagesPersistence.set(server, restify, authenticablePersistence)

    var appointmentsPersistence = require('./appointments-persistence')
    appointmentsPersistence.set(server,restify,patientsPersistence,doctorsPersistence,authenticablePersistence)
  }
})
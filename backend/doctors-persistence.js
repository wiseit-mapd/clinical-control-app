
module.exports.set = function (server, restify, authenticablePersistence) {
    var db = require('./db')
    var utils = require('./utils')
    var saveMongodb = require('save-mongodb')
    var generator = require('generate-password')

    db.get().collection('doctors', function (error, collection) {
        // Get a persistence engine for the doctors  
        var doctorsPersistence = require('save')('doctors', { engine: saveMongodb(collection) })

        module.exports.findById = function(doctorId, callback) {
            // Find a single patient by its id within save
            doctorsPersistence.findOne({ _id: doctorId }, function (error, doctor) {
                // If there are any errors, pass them to calback in the correct format
                if (error) {
                    callback(error, null)
                } else {
                    callback(null, doctor)
                }
            })
        }

        // Get all doctors in the system
        server.get('/doctors', function (req, res, next) {
            //Log request received
            console.log('/doctors GET: request received')
            
            // Find every entity within the given collection
            doctorsPersistence.find({}, function (error, doctors) {

                //Log sendig response
                var authenticableIds = []
                for (var i in doctors) {
                    var doctor = doctors[i]
                    authenticableIds.push(doctor.authenticableId)
                }

                authenticablePersistence.findByIds(authenticableIds, function (error, authenticables) {
                    if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))
                    for (var k in authenticables) {
                            var authenticable = authenticables[k]
                            authenticable.authenticableId = authenticable._id
                            delete authenticable._id
                            delete authenticable.password
                        }
                    res.send({doctors: doctors, authenticables: authenticables})
                })
                console.log('/doctors GET: sending response')
                // Return all of the doctors in the system
                //res.send(doctors)
            })
        })

        // Get a single doctors by its id
        server.get('/doctors/:id', function (req, res, next) {
            //Log request received
            console.log('/doctors/'+req.params.id+' GET: request received')

            // Find a single doctor by its id within save
            doctorsPersistence.findOne({ _id: req.params.id }, function (error, doctor) {
                // If there are any errors, pass them to next in the correct format
                if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                //Log sending response
                console.log('/doctors/'+req.params.id+' GET: sending response')
                if (doctor) {
                    // Send the doctor if no issues
                    authenticablePersistence.findById(doctor.authenticableId, function(error, authenticable) {
                        authenticable.authenticableId = authenticable._id
                        delete authenticable._id
                        delete authenticable.password
                        delete authenticable.username
                        console.log(authenticable)
                        res.send({doctor: doctor, authenticable: authenticable})
                    })
                } else {
                    // Send 404 header if the doctor doesn't exist
                    res.send(404)
                }
            })
        })

        // Get a single doctors by its authenticableId
        server.get('/doctors/authenticableId/:id', function (req, res, next) {
            //Log request received
            console.log('/doctors/authenticableId/'+req.params.id+' GET: request received')

            // Find a single doctor by its authenticableId within save
            doctorsPersistence.findOne({ authenticableId: req.params.id }, function (error, doctor) {
                // If there are any errors, pass them to next in the correct format
                if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                //Log sending response
                console.log('/doctors/authenticableId/'+req.params.id+' GET: sending response')
                if (doctor) {
                    // Send the doctor if no issues
                    authenticablePersistence.findById(doctor.authenticableId, function(error, authenticable) {
                        authenticable.authenticableId = authenticable._id
                        delete authenticable._id
                        delete authenticable.password
                        delete authenticable.username
                        console.log(authenticable)
                        res.send({doctor: doctor, authenticable: authenticable})
                    })
                } else {
                    // Send 404 header if the doctor doesn't exist
                    res.send(404)
                }
            })
        })

        // Create a new doctor
        server.post('/doctors', function (req, res, next) {
            //Log request received
            console.log('/doctors POST: request received')

            var validationErrors = validateDoctor(req)
            if (validationErrors.length > 0 ) {
                return next(new restify.InvalidArgumentError(JSON.stringify(validationErrors)))
            }

            req.params.password = utils.generatePassword()
            var user = utils.extractAuthenticableFromRequestParams(req)
            user.roles = ['DOCTOR']
            authenticablePersistence.create(user, function(error, authenticable) {
                // If there are any errors, pass them to next in the correct format
                if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                var newDoctor = {
                    professionalIdentifier: req.params.professionalIdentifier,
                    specializationId: req.params.specializationId,
                    authenticableId: authenticable._id
                }

                // Create the doctors using the persistence engine
                doctorsPersistence.create(newDoctor, function (error, doctor) {
                // If there are any errors, pass them to next in the correct format
                if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                //Log sending response
                console.log('/doctors POST: sending response')

                // Send the doctor if no issues
                res.send(201, doctor)
                })    
            })
        })

        // Update a doctor by its id
        server.put('/doctors/:id', function (req, res, next) {
            //Log request received
            console.log('/doctors/'+req.params.id+' PUT: request received')

            var validationErrors = validateDoctor(req)
            if (validationErrors.length > 0 ) {
                return next(new restify.InvalidArgumentError(JSON.stringify(validationErrors)))
            }

            var updatedDoctor = {
                _id: req.params.id,
                professionalIdentifier: req.params.professionalIdentifier,
                specializationId: req.params.specializationId
            }

            // Update the doctor with the persistence engine
            doctorsPersistence.update(updatedDoctor, true, function (error, doctor) {

            // If there are any errors, pass them to next in the correct format
            if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

            //Log sending response
            console.log('/doctors/'+req.params.id+' PUT: sending response')

            // Send a 200 OK response
            res.send(updatedDoctor)
            })
        })

        // Delete doctor with the given id
        server.del('/doctors/:id', function (req, res, next) {
            //Log request received
            console.log('/doctors/'+req.params.id+' DELETE: request received')

            // Delete the doctor with the persistence engine
            doctorsPersistence.delete(req.params.id, function (error, doctor) {

            // If there are any errors, pass them to next in the correct format
            if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

            //Log sending response
            console.log('/doctors/'+req.params.id+' DELETE: sending response')

            // Send a 200 OK response
            res.send(doctor)
            })
        })

        // Delete all doctors
        server.del('/doctors', function (req, res, next) {
            //Log request received
            console.log('/doctors DELETE: request received')

            // Delete the doctors with the persistence engine
            doctorsPersistence.deleteMany({}, function (error, doctors) {

            // If there are any errors, pass them to next in the correct format
            if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

            //Log sending response
            console.log('/doctors DELETE: sending response')

            // Send a 200 OK response
            res.send()
            })
        })

        console.log('/doctors method: GET')
        console.log('/doctors method: POST')
        console.log('/doctors/:id method: GET')
        console.log('/doctors/authenticableId/:id method: GET')
        console.log('/doctors/:id method: PUT')
        console.log('/doctors/:id method: DELETE')
    })
}

function validateDoctor(req) {
    var errors = []
    // Make sure professional identifier is defined
    if (req.params.professionalIdentifier === undefined) {
        errors.push('professional identifier must be provided')
    }
    // Make sure that specialization is defined
    if (req.params.specializationId === undefined) {
        errors.push('specialization id must be provided')
    }
    //TODO: Should ensure specializationId exists in specialization table 

    return errors
}


module.exports.set = function (server, restify, authenticablePersistence) {
    var db = require('./db')
    var saveMongodb = require('save-mongodb')

    db.get().collection('messages', function (error, collection) {
        // Get a persistence engine for the messages  
        var messagesPersistence = require('save')('messages', { engine: saveMongodb(collection) })

        // Get messages by their sender id
        server.get('/messages/sender/:id', function (req, res, next) {
            //Log request received
            console.log('/messages/sender/'+req.params.id+' GET: request received')

            // Find a message by their sender id within save
            messagesPersistence.find({ senderId: req.params.id }, function (error, messages) {
                // If there are any errors, pass them to next in the correct format
                if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                //Log sending response
                console.log('/messages/sender/'+req.params.id+' GET: sending response')
                if (messages) {
                    var authenticableIds = []
                    for (var k in messages) {
                        var message = messages[k]
                        authenticableIds.push(message.receiverId)
                    }

                    authenticablePersistence.findByIds(authenticableIds, function(error, authenticables) {
                        // If there are any errors, pass them to next in the correct format
                        if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                        for (var k in authenticables) {
                            var authenticable = authenticables[k]
                            authenticable.authenticableId = authenticable._id
                            delete authenticable._id
                            delete authenticable.password
                        }

                        // Send the messages and authenticables if no issues
                        res.send({messages: messages, receivers: authenticables})
                    })
                } else {
                    // Send empty array if there is no message
                    res.send([])
                }
            })
        })

        // Get messages by their receiver id
        server.get('/messages/receiver/:id', function (req, res, next) {
            //Log request received
            console.log('/messages/receiver/'+req.params.id+' GET: request received')

            // Find a message by their receiver id within save
            messagesPersistence.find({ receiverId: req.params.id }, function (error, messages) {
                // If there are any errors, pass them to next in the correct format
                if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                //Log sending response
                console.log('/messages/receiver/'+req.params.id+' GET: sending response')
                if (messages) {
                    var authenticableIds = []
                    for (var k in messages) {
                        var message = messages[k]
                        authenticableIds.push(message.senderId)
                    }

                    authenticablePersistence.findByIds(authenticableIds, function(error, authenticables) {
                        // If there are any errors, pass them to next in the correct format
                        if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                        var authenticablesToSend = [];
                        for (var k in authenticables) {
                            var authenticable = authenticables[k]
                            authenticable.authenticableId = authenticable._id
                            delete authenticable._id
                            delete authenticable.password
                        }
                        
                        // Send the messages and authenticables if no issues
                        res.send({messages: messages, receivers: authenticables})
                    })
                } else {
                    // Send empty array if there is no message
                    res.send([])
                }
            })
        })

        // Create a new message
        server.post('/messages', function (req, res, next) {
            //Log request received
            console.log('/messages POST: request received')

            // Make sure senderId is defined
            if (req.params.senderId === undefined ) {
            // If there are any errors, pass them to next in the correct format
            return next(new restify.InvalidArgumentError('senderId must be supplied'))
            }
            if (req.params.receiverId === undefined ) {
            // If there are any errors, pass them to next in the correct format
            return next(new restify.InvalidArgumentError('receiverId must be supplied'))
            }
            if (req.params.text === undefined ) {
            // If there are any errors, pass them to next in the correct format
            return next(new restify.InvalidArgumentError('text must be supplied'))
            }
            var newMessage = {
                senderId: req.params.senderId, 
                receiverId: req.params.receiverId,
                text: req.params.text,
                date: new Date()
            }

            // Create the message using the persistence engine
            messagesPersistence.create( newMessage, function (error, message) {
            // If there are any errors, pass them to next in the correct format
            if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

            //Log sending response
            console.log('/message POST: sending response')

            // Send the message if no issues
            res.send(201, message)
            })
        })

        console.log('/messages method: POST')
        console.log('/messages/sender/:id method: GET')
        console.log('/messages/receiver/:id method: GET')
    })
}
    
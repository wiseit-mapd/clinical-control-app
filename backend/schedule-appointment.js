module.exports.canSchedule = function (patient, doctor, appointmentInfo, currentAppointments) {
    var Joda = require('js-joda')
    var Period = Joda.Period
    var LocalTime = Joda.LocalTime
    var LocalDateTime = Joda.LocalDateTime
    var formatter = Joda.DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS")

    var appointmentDuration = Joda.Duration.parse(doctor.appointmentSettings.appointmentDuration)
    var requestedDateTime = LocalDateTime.parse(appointmentInfo.date, formatter)
    var requestedTime = requestedDateTime.toLocalTime()
    var requestedEndTime = appointmentDuration.addTo(requestedTime)
    var allWorkingTimes = doctor.workingTimes
    var weekDayWorkingTimes = new Set()

    var timeFormatter = Joda.DateTimeFormatter.ofPattern("HH:mm")

    for (k in currentAppointments) {
        var existingAppointment = currentAppointments[k];
        var existingAppointmentStart = LocalDateTime.parse(existingAppointment.date, formatter)
        if (existingAppointmentStart.toLocalDate().isEqual(requestedDateTime.toLocalDate())) {
            var existingAppointmentDuration = Joda.Duration.parse(existingAppointment.duration)
            var existingAppointmentEnd = existingAppointmentDuration.addTo(existingAppointmentStart)
            var requestedEndDateTime = appointmentDuration.addTo(requestedDateTime)
            if ((!requestedDateTime.isBefore(existingAppointmentStart) && requestedDateTime.isBefore(existingAppointmentEnd)) || 
                (requestedEndDateTime.isAfter(existingAppointmentStart) && requestedEndDateTime.isBefore(existingAppointmentEnd))) {
                return false
            } 
        }
    }


    for (k in allWorkingTimes) {
        var currentTimes = allWorkingTimes[k];
        if (currentTimes.weekday == requestedDateTime.dayOfWeek()) {
            if (!requestedTime.isBefore(LocalTime.parse(currentTimes.start, timeFormatter)) && !requestedEndTime.isAfter(LocalTime.parse(currentTimes.end, timeFormatter))) {
                return true
            } 
        }
    }
    return false
}
    
module.exports.set = function(server, restify, patientsPersistence, doctorsPersistence, authenticablePersistence) {

    var db = require('./db')
    var saveMongodb = require('save-mongodb')
    var utils = require('./utils')
    db.get().collection('appointments', function (error, collection) {

        var appointmentsPersistence = require('save')('measures', {engine: saveMongodb(collection)})

        server.get('/appointments/patient/:id', function (req, res, next) {

            console.log('/appointments/patient/'+req.params.id+' GET: request received')

            appointmentsPersistence.find({patientId: req.params.id}, function(error, appointments) {
                if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                console.log('/appointments/patient/'+req.params.id+' GET: sending response')
                res.send(appointments)
            })
        })

        server.get('appointments/doctor/:id', function (req,res,next) {

            console.log('/appointments/doctor/'+req.params.id+' GET: request received')
                authenticablePersistence.findById(req.params.id, function(error, authenticable) {
                    if (error) {
                       return next(new restify.InvalidArgumentError(JSON.stringify(error.erros)))
                    }

                    appointmentsPersistence.find({doctorId: authenticable._id}, function(error, appointments) {
                        if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                        console.log('/appointments/doctor/'+req.params.id+' GET: sending response')
                        res.send(appointments)
                    })
                })

        })

        server.post('/appointments', function (req,res, next) {
            
            console.log('/measures POST: request received')
            validationErrors = validateAppointment(req)

            if (validationErrors.length > 0 ) {
                return next(new restify.InvalidArgumentError(JSON.stringify(validationErrors)))
            }

             patientsPersistence.findById(req.params.patientId, function(error, patient) {
                if (error) {
                    return next(new restify.InvalidArgumentError(JSON.stringify(error.erros)))
                }
                
                authenticablePersistence.findById(req.params.doctorId, function(error, doctorAuthenticable) {
                    if (error) {
                       return next(new restify.InvalidArgumentError(JSON.stringify(error.erros)))
                    }

                    var errors = []
                    if (!patient) {
                        errors.push("Invalid patientId.")
                    }
                    if (!doctorAuthenticable) {
                        errors.push("Invalid doctorAuthenticableId.")
                    }
                    if (errors.length > 0) {
                        return next(new restify.InvalidArgumentError(JSON.stringify(errors)))
                    }

                    var newAppointment = {
                        patientId: req.params.patientId,
                        doctorId: req.params.doctorId,
                        appointmentDate: req.params.appointmentDate,
                        reason: req.params.reason,
                        state: req.params.state,
                        condition: req.params.condition,
                        prescription: req.params.prescription
                    }

                    appointmentsPersistence.create( newAppointment, function(error, appointment) {
                        
                        if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                        console.log('/appointments POST: sending response')

                        // Send the measure if no issues
                        res.send(201, appointment)
                    })
                })
                
             })
        })


                // Update a patient by its id
        server.put('/appointments/:id', function (req, res, next) {
            //Log request received
            console.log('/appointments/'+req.params.id+' PUT: request received')

            validationErrors = validateAppointment(req)
            if (validationErrors.length > 0 ) {
                return next(new restify.InvalidArgumentError(JSON.stringify(validationErrors)))
            }

            patientsPersistence.findById(req.params.patientId, function(error, patient) {
                if (error) {
                    return next(new restify.InvalidArgumentError(JSON.stringify(error.erros)))
                }

                 var updatedAppointment = {
                        _id: req.params.id,
                        patientId: req.params.patientId,
                        doctorId: req.params.doctorId,
                        appointmentDate: req.params.appointmentDate,
                        reason: req.params.reason,
                        state: req.params.state,
                        condition: req.params.condition,
                        prescription: req.params.prescription
                    }

                    // Update the measure with the persistence engine
                    appointmentsPersistence.update(updatedAppointment, function (error, appointment) {
                        // If there are any errors, pass them to next in the correct format
                        if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                        //Log sending response
                        console.log('/appointments/'+req.params.id+' PUT: sending response')

                        // Send a 200 OK response
                        res.send(updatedAppointment)
                    })
            })
        })

    })
}

function validateAppointment(req) {
    var errors = []
    
    //make sure patientId is defined
    if(req.params.patientId === undefined) {
        errors.push('patientId must be provided')
    }

    //make sure doctorId is defined
    if(req.params.doctorId === undefined) {
        errors.push('doctorId mush be provided')
    }

    //make sure appointmentDate is defined
    if(req.params.appointmentDate === undefined) {
        errors.push('appointmentDate must be provided')
    }

    if(req.params.reason === undefined) {
            errors.push('reason must be provided')
        }

    if (req.params.state === undefined) {
        errors.push('state must be provided')
    }
    

    if (req.params.state === "DONE") { 
    //make sure reason is defined

        //make sure condition is defined
        if (req.params.condition === undefined) {
            errors.push('condition must be provided')
        }

        //make sure prescription is defined
        if (req.params.prescription === undefined) {
            errors.push('prescription must be provided')
        }
    }

    return errors
}

var authenticablePersistence;

module.exports.set = function (server, restify) {
    var db = require('./db')
    var saveMongodb = require('save-mongodb')

    db.get().collection('authenticable', function (error, collection) {
        // Get a persistence engine for the authenticable
        authenticablePersistence = require('save')('authenticable', { engine: saveMongodb(collection) })


        module.exports.find = function (authenticable, callback) {
            // Find a single authenticable by its id within save
            authenticablePersistence.findOne(authenticable, function (error, authenticable) {
                // If there are any errors, pass them to calback in the correct format
                if (error) {
                    callback(error, null)
                } else {
                    callback(null, authenticable)
                }
            })
        }

        module.exports.findById = function (authenticableId, callback) {
            // Find a single authenticable by its id within save
            authenticablePersistence.findOne({ _id: authenticableId }, function (error, authenticable) {
                // If there are any errors, pass them to calback in the correct format
                if (error) {
                    callback(error, null)
                } else {
                    callback(null, authenticable)
                }
            })
        }

        module.exports.findByIds = function (authenticableIds, callback) {
            // Find multiple authenticables by their ids within save
            authenticablePersistence.find({ _id: { $in: authenticableIds } }, function (error, authenticables) {
                // If there are any errors, pass them to calback in the correct format
                if (error) {
                    callback(error, null)
                } else {
                    callback(null, authenticables)
                }
            })
        }

        module.exports.create = function (authenticable, callback) {
            // Make sure username and password are defined
            var errors = []
            if (authenticable.username === undefined) {
                errors.push("The username must be provided")
            } 
            if (authenticable.password === undefined) {
                errors.push("The password must be provided")
            }
            if (authenticable.roles === undefined || authenticable.roles.length == 0) {
                errors.push("The roles must be provided")
            }
            // Make sure name is defined
            if (authenticable.name === undefined) {
                errors.push('name must be provided')
            }
            // Make sure phone is defined
            if (authenticable.phone === undefined) {
                errors.push('phone must be provided')
            }
            // Make sure email is defined
            if (authenticable.email === undefined) {
                errors.push('email must be provided')
            }
            // Make sure date of birth is defined
            if (authenticable.dateOfBirth === undefined) {
                errors.push('date of birth must be provided')
            }
            // Make sure gender is defined
            if(authenticable.gender === undefined) {
                errors.push('gender must be provided')
            }
            // Make sure photo is defined
            //if(authenticable.photo === undefined) {
//                errors.push('photo must be provided')
//            }

            if(authenticable.address === undefined) {
                errors.push('address must be provided')
            }

            if (errors.length > 0) {
                return callback({errors: errors})
            }

            module.exports.find({username: authenticable.username}, function (error, persistedAuthenticable) {
                if (error) return callback(error)

                if (persistedAuthenticable) return callback({errors: 'Username already in use'})

                // Create the authenticable using the persistence engine
                authenticablePersistence.create(authenticable, function (error, persistedAuthenticable) {
                    // If there are any errors, pass them to next in the correct format
                    if (error) return callback(new restify.InvalidArgumentError(JSON.stringify(error.errors)))
                    // Send the authenticable if no issues
                    return callback(null, persistedAuthenticable)
                })
            })
        }

        module.exports.update = function(authenticable, callback) {
            var errors = [];
            if (authenticable.name === undefined) {
                errors.push('name must be provided')
            }
            // Make sure phone is defined
            if (authenticable.phone === undefined) {
                errors.push('phone must be provided')
            }
            // Make sure email is defined
            if (authenticable.email === undefined) {
                errors.push('email must be provided')
            }
            // Make sure date of birth is defined
            if (authenticable.dateOfBirth === undefined) {
                errors.push('date of birth must be provided')
            }
            // Make sure gender is defined
            if(authenticable.gender === undefined) {
                errors.push('gender must be provided')
            }
            // Make sure photo is defined
            //if(authenticable.photo === undefined) {
//                errors.push('photo must be provided')
//            }

            if(authenticable.address === undefined) {
                errors.push('address must be provided')
            }
            if (errors.length > 0) {
                return callback({errors: errors})
            }
            module.exports.find({_id: authenticable._id}, function (error, persistedAuthenticable) {
                if (error) return callback(error)

                // Create the authenticable using the persistence engine
                authenticablePersistence.update(authenticable, function (error, persistedAuthenticable) {
                    // If there are any errors, pass them to next in the correct format
                    if (error) return callback(new restify.InvalidArgumentError(JSON.stringify(error.errors)))
                    // Send the authenticable if no issues
                    return callback(null, persistedAuthenticable)
                })
            })
        }

        module.exports.delete = function(authenticable, callback) {
            module.exports.find({_id: authenticable._id}, function (error, persistedAuthenticable) {
                if (error) return callback(error)

                // Create the authenticable using the persistence engine
                authenticablePersistence.delete(authenticable, function (error, persistedAuthenticable) {
                    // If there are any errors, pass them to next in the correct format
                    if (error) return callback(new restify.InvalidArgumentError(JSON.stringify(error.errors)))
                    // Send the authenticable if no issues
                    return callback(null, persistedAuthenticable)
                })
            })
        }
        
    })
}
    
var config = require('./config');
var MongoClient = require('mongodb').MongoClient

var state = {
  connection: null,
}

exports.connect = function(done) {
    if (state.connection) return done()

    // Connect to the mongodb database configured.
    MongoClient.connect(config.database, function (err, connection) {
        if (err) return done(err)
        state.connection = connection
        done()
    })

}

exports.get = function() {
  return state.connection
}

exports.close = function(done) {
  if (state.connection) {
    state.connection.close(function(err, result) {
      state.connection = null
      state.mode = null
      done(err)
    })
  }
}
module.exports.sendEmailWithPassword = function(req, info) {
    var nodemailer = require('nodemailer');
    var config = require('./config');
    var CryptoJS = require('crypto-js');
    var fs = require('fs');
    var pug = require('pug');
    
    var key = config.password_key;
    var iv  = CryptoJS.enc.Hex.parse('be410fea41df7162a679875ec131cf2c');
    var pass_encrypted = config.password_encrypted;
    
    var decrypted = CryptoJS.AES.decrypt(
        pass_encrypted,
        key,
            {
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            }
        );
        decrypted = decrypted.toString(CryptoJS.enc.Utf8);
    
    // create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport('smtps://clinicalcontrol%40gmail.com:'+decrypted+'@smtp.gmail.com');

    var message = {
       to: req.params.email,
       name: req.params.name,
       password: req.params.password
    };

    //var template = fs.readFileSync('./email-templates/confirmation.pug', 'utf-8');

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: '"ClinicalControl" <clinicalcontrol@gmail.com>', // sender address
        to: message.to, // list of receivers
        subject: 'ClinicalControl Confirmation E-mail', // Subject line
        html: pug.renderFile('./email-templates/confirmation.pug', {name:message.name, username:message.to, password:message.password})// html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            return console.log(error);
        }
        console.log('Message sent: ' + info.response);
    });
}

module.exports.set = function (server, restify, patientsPersistence, authenticablePersistence) {

    var db = require('./db')
    var saveMongodb = require('save-mongodb')
    var utils = require('./utils')

    db.get().collection('measures', function (error, collection) {
        // Get a persistence engine for the measures  
        var measuresPersistence = require('save')('measures', { engine: saveMongodb(collection) })

        // Get the measures based on patientId
        server.get('/measures/patient/:id', function (req, res, next) {
            //Log request received
            console.log('/measures/patient/'+req.params.id+' GET: request received')

            // Find athe measures based on patientId within save
            measuresPersistence.find({ patientId: req.params.id }, function (error, measures) {
                // If there are any errors, pass them to next in the correct format
                if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                //Log sending response
                console.log('/measures/patient/'+req.params.id+' GET: sending response')
                if (measures) {
                    var authenticableIds = []
                    for (var k in measures) {
                        var measure = measures[k]
                        authenticableIds.push(measure.responsibleAuthenticableId)
                    }

                    authenticablePersistence.findByIds(authenticableIds, function(error, authenticables) {
                        // If there are any errors, pass them to next in the correct format
                        if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                        for (var k in authenticables) {
                            var authenticable = authenticables[k]
                            authenticable.authenticableId = authenticable._id
                            delete authenticable._id
                            delete authenticable.password
                        }

                        // Send the measures and authenticables if no issues
                        res.send({measures: measures, responsibles: authenticables})
                    })
                } else {
                    // Send empty array if there is no measures
                    res.send([])
                }
            })
        })

        // Create a new measure
        server.post('/measures', function (req, res, next) {
            //Log request received
            console.log('/measures POST: request received')
            validationErrors = validateMeasure(req)

            if (validationErrors.length > 0 ) {
                return next(new restify.InvalidArgumentError(JSON.stringify(validationErrors)))
            }

            patientsPersistence.findById(req.params.patientId, function(error, patient) {
                if (error) {
                    return next(new restify.InvalidArgumentError(JSON.stringify(error.erros)))
                }

                authenticablePersistence.findById(req.params.responsibleAuthenticableId, function(error, responsibleAuthenticable) {
                    if (error) {
                       return next(new restify.InvalidArgumentError(JSON.stringify(error.erros)))
                    }

                    var errors = []
                    if (!patient) {
                        errors.push("Invalid patientId.")
                    }
                    if (!responsibleAuthenticable) {
                        errors.push("Invalid responsibleAuthenticableId.")
                    }
                    if (errors.length > 0) {
                        return next(new restify.InvalidArgumentError(JSON.stringify(errors)))
                    }

                    var newMeasure = {
                        patientId: req.params.patientId,
                        responsibleAuthenticableId: req.params.responsibleAuthenticableId,
                        appointmentId: req.params.appointmentId,
                        measurementDate: req.params.measurementDate,
                        type: req.params.type,
                        value: req.params.value,
                        sHealthId: req.params.sHealthId
                    }

                    // Create the measure using the persistence engine
                    measuresPersistence.create( newMeasure, function (error, measure) {
                        // If there are any errors, pass them to next in the correct format
                        if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                        //Log sending response
                        console.log('/measures POST: sending response')

                        // Send the measure if no issues
                        res.send(201, measure)
                    })
                })
            })
        })

        // Update a patient by its id
        server.put('/measures/:id', function (req, res, next) {
            //Log request received
            console.log('/measures/'+req.params.id+' PUT: request received')

            validationErrors = validateMeasure(req)
            if (validationErrors.length > 0 ) {
                return next(new restify.InvalidArgumentError(JSON.stringify(validationErrors)))
            }

            patientsPersistence.findById(req.params.patientId, function(error, patient) {
                if (error) {
                    return next(new restify.InvalidArgumentError(JSON.stringify(error.erros)))
                }

                authenticablePersistence.findById(req.params.responsibleAuthenticableId, function(error, responsibleAuthenticable) {
                    if (error) {
                       return next(new restify.InvalidArgumentError(JSON.stringify(error.erros)))
                    }

                    var errors = []
                    if (!patient) {
                        errors.push("Invalid patientId.")
                    }
                    if (!responsibleAuthenticable) {
                        errors.push("Invalid responsibleAuthenticableId.")
                    }
                    if (errors.length > 0) {
                        return next(new restify.InvalidArgumentError(JSON.stringify(errors)))
                    }

                    var updatedMeasure = {
                        _id: req.params.id,
                        patientId: req.params.patientId,
                        responsibleAuthenticableId: req.params.responsibleAuthenticableId,
                        appointmentId: req.params.appointmentId,
                        measurementDate: req.params.measurementDate,
                        type: req.params.type,
                        value: req.params.value,
                        sHealthId: req.params.sHealthId
                    }

                    // Update the measure with the persistence engine
                    measuresPersistence.update(updatedMeasure, function (error, measure) {
                        // If there are any errors, pass them to next in the correct format
                        if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

                        //Log sending response
                        console.log('/measures/'+req.params.id+' PUT: sending response')

                        // Send a 200 OK response
                        res.send(updatedMeasure)
                    })
                })
            })
        })

        // Delete measure with the given id
        server.del('/measures/:id', function (req, res, next) {
            //Log request received
            console.log('/measures/'+req.params.id+' DELETE: request received')

            // Delete the measure with the persistence engine
            measuresPersistence.delete(req.params.id, function (error, measure) {

            // If there are any errors, pass them to next in the correct format
            if (error) return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))

            //Log sending response
            console.log('/meadure/'+req.params.id+' DELETE: sending response')

            // Send a 200 OK response
            res.send(measure)
            })
        })

        console.log('/measures/patientId/:id method: GET')
        console.log('/measures method: POST')
        console.log('/measures/:id method: DELETE')
    })
}

function validateMeasure(req, patientsPersistence) {
    var errors = []
    
    //make sure patientId is defined
    if(req.params.patientId === undefined) {
        errors.push('patientId must be provided')
    }

    //make sure responsibleAuthenticableId is defined
    if(req.params.responsibleAuthenticableId === undefined) {
        errors.push('responsibleAuthenticableId mush be provided')
    }

    //make sure measurementDate is defined
    if(req.params.measurementDate === undefined) {
        errors.push('measurementDate must be provided')
    }

    //make sure type is defined
    if(req.params.type === undefined) {
        errors.push('type must be provided')
    }

    //make sure value is defined
    if (req.params.value === undefined) {
        errors.push('value must be provided')
    }

    return errors
}
